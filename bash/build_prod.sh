if [ ! -d "../../produce" ]; then
    echo "create release path"
  mkdir ../../produce
fi
if [ ! -d "../../produce/_examples" ]; then
    echo "create _examples path"
  mkdir ../../produce/_examples
fi
if [ ! -d "../../produce/_export" ]; then
    echo "create _export path"
  mkdir ../../produce/_export
fi
if [ ! -d "../../produce/logs" ]; then
    echo "create logs path"
  mkdir ../../produce/logs
fi
if [[ ! -f "../../produce/package.json" ]]; then
	echo "package.json 不存在"
    cp ../package.json ../../produce
    ../../produce/npm install
else
	echo "package.json is exist"
fi
npm run build
rm ../../produce/config.js
rm ../../produce/config.service.js

if [ ! -d "../../produce/node_modules" ]; then
    echo "create node_modules path"
    cd ../../produce
    npm install
    cp ../4usdt-restfulapi-server/lib/sendemail/lib/index.js ./node_modules/sendemail/lib/index.js
    cp ../4usdt-restfulapi-server/.env.prod ./.env
    cp ../4usdt-restfulapi-server/publicKey.pub ./publicKey.pub
fi

read num

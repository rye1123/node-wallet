/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('wlt_convert_info', {
    id: {
      type: DataTypes.INTEGER(32),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    order_no: {
      type: DataTypes.STRING(40),
      allowNull: false,
      defaultValue: ''
    },
    symbol: {
      type: DataTypes.STRING(20),
      allowNull: false,
      defaultValue: ''
    },
    exchange_rate: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000000000000000'
    },
    amount: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000000000000000'
    },
    sign: {
      type: DataTypes.STRING(260),
      allowNull: false,
      defaultValue: ''
    },
    status: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    created_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'wlt_convert_info'
  });
};

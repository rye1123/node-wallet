/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('wlt_withdraw_merchants_callback_record', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    order_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    call_url: {
      type: DataTypes.STRING(200),
      allowNull: false,
      defaultValue: ''
    },
    status_code: {
      type: DataTypes.INTEGER(3),
      allowNull: false,
      defaultValue: '200'
    },
    res_data: {
      type: DataTypes.STRING(200),
      allowNull: false,
      defaultValue: ''
    },
    created_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    enable: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    is_delete: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    status: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    sign: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ''
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ''
    }
  }, {
    tableName: 'wlt_withdraw_merchants_callback_record'
  });
};

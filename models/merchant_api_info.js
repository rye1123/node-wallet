/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('merchant_api_info', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    api_key: {
      type: DataTypes.STRING(60),
      allowNull: false
    },
    api_secret: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ''
    },
    created_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    is_delete: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    enable: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    status: {
      type: DataTypes.INTEGER(255),
      allowNull: false
    },
    bind_ip: {
      type: DataTypes.STRING(1000),
      allowNull: false,
      defaultValue: '0.0.0.0'
    },
    readonly: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    wallet_deposit: {
      type: DataTypes.INTEGER(255),
      allowNull: false,
      defaultValue: '0'
    },
    wallet_withdraw: {
      type: DataTypes.INTEGER(255),
      allowNull: false,
      defaultValue: '0'
    },
    sign: {
      type: DataTypes.STRING(500),
      allowNull: false,
      defaultValue: ''
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ''
    },
    last_use_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'merchant_api_info'
  });
};

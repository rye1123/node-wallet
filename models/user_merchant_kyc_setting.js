/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_merchant_kyc_setting', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    id_check: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    id_check_with_phone: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    vedio_id_check: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    vedio_id_check_min_amount: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '10000.00'
    },
    vedio_id_check_max_amount: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '50000.00'
    },
    vedio_id_check_count_cycle: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '0'
    },
    vedio_id_check_retry_count: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '3'
    },
    is_delete: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    sign: {
      type: DataTypes.STRING(256),
      allowNull: false,
      defaultValue: ''
    },
    created_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updated_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'user_merchant_kyc_setting'
  });
};

/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('wlt_withdraw_merchants_approval_record', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    auditor_user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    order_no: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '1'
    },
    result: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    created_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    sign: {
      type: DataTypes.STRING(260),
      allowNull: false
    },
    remark: {
      type: DataTypes.STRING(260),
      allowNull: false,
      defaultValue: ''
    },
    is_delete: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'wlt_withdraw_merchants_approval_record'
  });
};

/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('admin_base', {
    adminId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    adminName: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    account: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    password: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    status: {
      type: DataTypes.INTEGER(2),
      allowNull: true
    },
    isAdmin: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    avatar: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    roleId: {
      type: DataTypes.INTEGER(32),
      allowNull: true
    },
    isDelete: {
      type: DataTypes.INTEGER(2),
      allowNull: true
    },
    createdTime: {
      type: DataTypes.INTEGER(16),
      allowNull: false,
      defaultValue: '1542869595'
    },
    updatedTime: {
      type: DataTypes.INTEGER(16),
      allowNull: false,
      defaultValue: '1542869595'
    }
  }, {
    tableName: 'admin_base'
  });
};

/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('funds_list', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    parent_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    userid: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    orderNo: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    value: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    created_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    symbol: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    source: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    use: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    sign: {
      type: DataTypes.STRING(256),
      allowNull: false
    },
    createUserId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ''
    },
    log_amount: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000000000000000'
    }
  }, {
    tableName: 'funds_list'
  });
};

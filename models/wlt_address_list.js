/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('wlt_address_list', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    userid: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    address: {
      type: DataTypes.STRING(80),
      allowNull: false
    },
    created_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    symbol: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    display_name: {
      type: DataTypes.STRING(20),
      allowNull: false,
      defaultValue: 'USDT'
    },
    enable: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    sign: {
      type: DataTypes.STRING(260),
      allowNull: false
    },
    is_delete: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'wlt_address_list'
  });
};

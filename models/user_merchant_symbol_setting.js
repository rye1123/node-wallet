/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_merchant_symbol_setting', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    merchantId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    symbol_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    merchantProfit: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000'
    },
    merchantBuyProfit: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000'
    },
    merchantSellProfit: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000'
    },
    merchantAppendFee: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00000000'
    },
    merchantBuyAppendFee: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00000000'
    },
    merchantSellAppendFee: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00000000'
    },
    merchantPlatformBuyProfit: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0050'
    },
    merchantPlatformSellProfit: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0050'
    },
    merchant_max_cancel_order_day: {
      type: DataTypes.INTEGER(5),
      allowNull: false,
      defaultValue: '3'
    },
    merchant_max_order_concurrent: {
      type: DataTypes.INTEGER(5),
      allowNull: false,
      defaultValue: '3'
    },
    app_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '1'
    }
  }, {
    tableName: 'user_merchant_symbol_setting'
  });
};

/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('wlt_withdraw_record', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    userid: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    address: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    value: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    tx: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    created_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    symbol: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    enable: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    sign: {
      type: DataTypes.STRING(260),
      allowNull: false
    },
    is_delete: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    status: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ''
    },
    logBalance: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000000000000000'
    },
    no_id: {
      type: DataTypes.STRING(34),
      allowNull: false,
      defaultValue: ''
    },
    block_check: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    is_internal: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'wlt_withdraw_record'
  });
};

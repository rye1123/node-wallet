import path from 'path';
import UUID from 'uuid';

export const System = {
    API_server_type: 'http://',
    API_server_host: '127.0.0.1',
    API_server_port: '3001',
    HTTP_server_type: 'http://',
    HTTP_server_host: '127.0.0.1',
    HTTP_server_port: '65534',
    System_country: 'zh-cn',
    System_plugin_path: path.join(__dirname, './plugins'),
    Session_Config: {
		
        key: UUID.v1().replace(/-/g, ''),
		
        maxAge: 1000 * 60 * 5,
		
        overwrite: true,
		
        httpOnly: true,
		
        signed: true,
		
        rolling: true,
		
        renew: false,
    }
};

export const ENV = {
    isDev: true
};

export const DB = {
    relationalConfs: [{
            DB_type: 'mysql',
            dbName: 'webDB',
            host: '47.110.139.56',
            port: 3306,
            username: 'root',
            password: 'Webit!!2018',
            database: 'test_dealer_user_info',
            //prefix: 'api_',
            dialectOptions: {
                charset: 'utf8mb4',
                //collate: 'utf8mb4_unicode_520_ci',
                supportBigNumbers: true,
                bigNumberStrings: true,
                //requestTimeout: 60 * 1000
            },
            pool: {
                max: 50,
                min: 0,
                idle: 10000
            }
        }
    ],
    mongoConf: {
        host: 'mongodb://localhost',
        port: 27017,
        username: '',
        password: '',
        database: 'tx',
        prefix: 'api_'
    }
};

export const RedisConfig = {
    port: 6379,
    host: '47.110.139.56',
    family: 4,
    password: 'Q1w2E3r4T5y6',
    db: 0
};

export const SendEmail = {
    service: 'smtp.afdsbcd.com12',
    username: 'postmaster%40abcd.com',
    password: 'password',
    sender_address: '"XX平台 👥" <postmfsdaster@abfdscd.cofdsm>'
};

export const constant = {
    IP_REG_EXP: /^((25[0-5]|2[0-4]\d|((1\d{2})|(1-9)?\d))\.){3}((25[0-5]|2[0-4]\d|((1\d{2})|(1-9)?\d)))$/ //ip正则
}
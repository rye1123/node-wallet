import Sequelize from 'sequelize';
import { DB } from '../config';

let mysqls = {};
DB.relationalConfs.forEach(item => {
    if (!mysqls[`${item.dbName}Util`]) {
        mysqls[`${item.dbName}Util`] = new Sequelize(item.database, item.username, item.password, {
            host: item.host,
            port: item.port,
            dialect: item.DB_type,
            pool: item.pool,
            timezone: '+08:00',
            dialectOptions: {
                dateStrings: true,
                typeCast: true
            }
            // operatorsAliases: false
        });
        mysqls[`${item.dbName}Util`].authenticate().then(() => {
            console.log(`数据库地址 ${item.host}:${item.port} 连接成功!`);
            console.log(`${item.dbName} 连接成功!`);
        }).catch(err => {
            throw new Error(`${item.dbName} 连接出错${err}`);
        });
    }
});


export default Object.assign(Sequelize, mysqls);

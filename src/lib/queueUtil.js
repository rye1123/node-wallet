const Queue = require('bee-queue');
import { RedisConfig } from '../config';

//发起端使用
export const queueConfig = {
    prefix: 'order',
    redis: {
        host: RedisConfig.host,
        port: RedisConfig.port,
        db: 0,
        options: {},
        password: RedisConfig.password
    },
    isWorker: false,
    getEvents: true,
    sendEvents: true,
    storeJobs: true,
    ensureScripts: true,
    activateDelayedJobs: false,
    removeOnSuccess: true,
    removeOnFailure: true,
    redisScanCount: 100
}

//处理端使用
export const queueWorkerConfig = {
    prefix: 'order',
    stallInterval: 5000,
    nearTermWindow: 1200000,
    delayedDebounce: 1000,
    redis: {
        host: RedisConfig.host,
        port: RedisConfig.port,
        db: 0,
        options: {},
        password: RedisConfig.password,
        retry_strategy(options) {
            return Math.min(options.attempt * 100, 3000);
        },
    },
    isWorker: true,
    getEvents: true,
    sendEvents: true,
    storeJobs: true,
    ensureScripts: true,
    activateDelayedJobs: false,
    removeOnSuccess: true,
    removeOnFailure: true,
    redisScanCount: 100
}

// export const lock_orderQueue = new Queue('lock_order', queueConfig);
// export const complete_orderQueue = new Queue('order_complete', queueOrderCompleteConfig);


export function getFullNum(num) {
    //处理非数字
    if (isNaN(num)) { return num };

    //处理不需要转换的数字
    var str = '' + num;
    if (!/e/i.test(str)) { return num; };

    //先获取到精确的小数位
    var fixed = ('' + num).match(/\d+$/)[0];

    //拿到保留指定的小数
    return new Number(num).toFixed(fixed);
}

export function add(num1, num2) {
    const num1Digits = (num1.toString().split('.')[1] || '').length;
    const num2Digits = (num2.toString().split('.')[1] || '').length;
    const baseNum = Math.pow(10, Math.max(num1Digits, num2Digits));
    return (num1 * baseNum + num2 * baseNum) / baseNum;
}

// //jn定义的加法
// export function add(num1, num2) {
//     var r1, r2, m, n;
//     try {
//         r1 = num1.toString().split(".")[1].length;
//     } catch (e) {
//         r1 = 0;
//     }
//     try {
//         r2 = num2.toString().split(".")[1].length;
//     } catch (e) {
//         r2 = 0;
//     }
//     m = Math.pow(10, Math.max(r1, r2));
//     n = r1 >= r2 ? r1 : r2;
//     return ((num1 * m + num2 * m) / m).toFixed(n);
// }
//jn定义的减法
export function sub(num1, num2) {
    var r1, r2, m, n;
    try {
        r1 = num1.toString().split(".")[1].length;
    } catch (e) {
        r1 = 0;
    }
    try {
        r2 = num2.toString().split(".")[1].length;
    } catch (e) {
        r2 = 0;
    }
    n = r1 >= r2 ? r1 : r2;
    m = Math.pow(10, Math.max(r1, r2));
    return ((num1 * m - num2 * m) / m).toFixed(n);
}
//jn定义的乘法
export function mul(num1, num2) {
    var m = 0;
    try {
        m += num1.toString().split(".")[1].length;
    } catch (e) {
    }
    try {
        m += num2.toString().split(".")[1].length;
    } catch (e) {
    }
    return (
        Number(num1.toString().replace(".", "")) *
        Number(num2.toString().replace(".", "")) /
        Math.pow(10, m)
    );
}
//jn定义的除法
export function div(arg1, arg2) {
    var t1 = 0,
        t2 = 0,
        r1,
        r2;
    try {
        t1 = arg1.toString().split(".")[1].length;
    } catch (e) {
    }
    try {
        t2 = arg2.toString().split(".")[1].length;
    } catch (e) {
    }
    r1 = Number(arg1.toString().replace(".", ""));
    r2 = Number(arg2.toString().replace(".", ""));
    return r1 / r2 * Math.pow(10, t2 - t1);
}
//处理小数点位数
export function fool(n, s) {
    let v = 0;
    switch (s) {
        case 1:
            v = 10;
            break;
        case 2:
            v = 100;
            break;
        case 3:
            v = 1000;
            break;
        case 4:
            v = 10000;
            break;
        case 5:
            v = 100000;
            break;
        case 6:
            v = 1000000;
            break;
        case 7:
            v = 10000000;
            break;
        case 8:
            v = 100000000;
            break;
        case 9:
            v = 1000000000;
            break;
        default:
            v = 10000;
    }
    return getFullNum(div(Math.floor(mul(n, v)), v));
}

export function formatNumber(num, precision, separator) {
    var parts;
    // 判断是否为数字
    if (!isNaN(parseFloat(num)) && isFinite(num)) {
        // 把类似 .5, 5. 之类的数据转化成0.5, 5, 为数据精度处理做准, 至于为什么
        // 不在判断中直接写 if (!isNaN(num = parseFloat(num)) && isFinite(num))
        // 是因为parseFloat有一个奇怪的精度问题, 比如 parseFloat(12312312.1234567119)
        // 的值变成了 12312312.123456713
        num = Number(num);
        // 处理小数点位数
        num = (typeof precision !== 'undefined' ? num.toFixed(precision) : num).toString();
        // 分离数字的小数部分和整数部分
        parts = num.split('.');
        // 整数部分加[separator]分隔, 借用一个著名的正则表达式
        parts[0] = parts[0].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + (separator || ','));

        return parts.join('.');
    }
    return 0;
}

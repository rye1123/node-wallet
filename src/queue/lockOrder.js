import sequelize from '../lib/sequelize_queue';
import RedisDao from '../lib/RedisDao';
import result from '../tools/Result';
import Sign from '../tools/Sign';
const { webDBUtil, Sequelize } = sequelize;

import AmountService from '../services/Account/AmountService';
const amountService = new AmountService()
const lock_order_listDAO = webDBUtil.import('../models/Amount/lock_order_list');
const convertInfoDAO = webDBUtil.import('../models/Wallet/wlt_convert_info');

import { queueWorkerConfig } from '../lib/queueUtil'
var Queue = require('bee-queue');

var queue = new Queue('lock', queueWorkerConfig);


queue.on('ready', async function () {
    await queue.process(async function (job, done) {
        console.log(`开始订单金额冻结事务 - ${job.id}`)
        try {
            job.reportProgress(10);

            console.log('processing job ' + job.id);
            let msg;

            let session = job.data.session;
            let data = job.data.data;
            let symbol = data.symbol;
            let source = data.source;
            let value = data.value;
            let order_no = data.order_no;
            let convertInfo = data.convert;

            if (convertInfo && convertInfo.symbol) {

                let convert_info_exist = await convertInfoDAO.findOne({
                    where: {
                        order_no: data.order_no,
                        user_id: session.id
                    }
                })

                if (!convert_info_exist) {

                    convertInfo.order_no = data.order_no
                    convertInfo.symbol = convertInfo.symbol
                    convertInfo.user_id = session.id
                    convertInfo.exchange_rate = convertInfo.exchange_rate
                    convertInfo.amount = convertInfo.amount
                    convertInfo.sign = await Sign.getRSASign(JSON.stringify(convertInfo), 'funds_convert_info')
                    await convertInfoDAO.create(convertInfo)

                }
                else {
                    msg = result.failed('转换记录已存在，请勿重复提交订单', 405687);
                    RedisDao.set(`order:lock_order_result_${job.id}`, JSON.stringify(msg), 6000)
                    return done(null, msg);
                }
            }

            let queryData = {
                where: {
                    orderNo: order_no,
                    is_delete: 0,
                    userid: session.id
                },
                raw: true,
                lock: Sequelize.Transaction.LOCK.UPDATE
            };

            const orderFreezeInfo = await lock_order_listDAO.findOne(queryData);
            job.reportProgress(50);

            if (orderFreezeInfo) {
                msg = result.failed('该订单资金已冻结，请勿重复操作', 405601);
            } else {
                let amountInfo = await amountService.amount(session, { symbol })

                if (amountInfo.data.available >= value) {
                    await lock_order_listDAO.create({
                        userid: session.id,
                        orderNo: order_no,
                        value,
                        symbol,
                        source,
                        enable: 1,
                        sign: ''
                    })
                    msg = result.success("订单资金冻结成功");
                } else {
                    msg = result.failed('余额已不足，请充值后再次下单，可用余额：' + amountInfo.data.available, 405602);
                }
            }
            job.reportProgress(90);


            console.log(data)
            console.log(msg)
            RedisDao.set(`order:lock_order_result_${job.id}`, JSON.stringify(msg), 6000)
            return done(null, msg);
        } catch (error) {
            console.log(error)
            RedisDao.set(`order:lock_order_result_${job.id}`, JSON.stringify(error), 6000)
            return done(null, error);
        }
    });

    console.log('processing lock Order jobs...');
});

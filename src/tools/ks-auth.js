// import Crypto from '../lib/sha1hmac'
// import crypto from "crypto";
let crypto = require('crypto');

class KSHelper {

    genSign = async function (apiKey, apiSecret, expired) {
        return new Promise(function (resolve, reject) {
            let random = `8${Math.random().toString().slice(-9)}`
            let current_time = Date.parse(new Date()) / 1000;
            let expire_time = current_time + expired;

            let raw = `a=${apiKey}&b=${expire_time}&c=${current_time}&d=${random}`
            raw = "a=ICVvC_xUs6177WEtyUNwIH8J6NfGu50t&b=1530762218&c=1530762118&d=0799687066"
            console.log(`row-${raw}`)


            let sign_tmp = crypto.createHmac('sha1', apiSecret).update(raw).digest()//.toString('base64');

            // let sign_tmp = Crypto.sha1_hmac(raw, apiSecret)

            console.log(Buffer.from(sign_tmp).toString('base64'))
            console.log(Buffer.from(sign_tmp).toString('hex'))
            console.log(Buffer.from(sign_tmp).toString('ascii'))

            console.log(Buffer.from(sign_tmp))

            resolve(`${sign_tmp}${raw}`)
        });
    }
}


module.exports = new KSHelper();

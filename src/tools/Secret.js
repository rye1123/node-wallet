const CryptoJS = require('crypto-js');  //引用AES源码js

const key = CryptoJS.enc.Utf8.parse("96345df998a8a153");  //十六位十六进制数作为密钥
const iv = CryptoJS.enc.Utf8.parse('96345df998a8a153');   //十六位十六进制数作为密钥偏移量

class Secret {
    //加密方法
    encryptByDES = function (message) {
        /**判断传参类型**/
        if (message === "" || message === null || message === undefined) {
            return "";
        }
        if (typeof (message) != "string") {
            message = message.toString();
        }
        var keyHex = CryptoJS.enc.Utf8.parse(key || fn.key);
        var encrypted = CryptoJS.DES.encrypt(message, keyHex, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        return encrypted.toString();
    }

    //解密方法
    decryptByDES = function (ciphertext) {
        if (ciphertext === "" || ciphertext === null || ciphertext === undefined) {
            return "";
        }
        if (typeof (ciphertext) != "string") {
            ciphertext = ciphertext.toString();
        }
        var keyHex = CryptoJS.enc.Utf8.parse(key || fn.key);
        var decrypted = CryptoJS.DES.decrypt({
            ciphertext: CryptoJS.enc.Base64.parse(ciphertext)
        }, keyHex, {
                mode: CryptoJS.mode.ECB,
                padding: CryptoJS.pad.Pkcs7
            });
        return decrypted.toString(CryptoJS.enc.Utf8);
    }
}

module.exports = new Secret();
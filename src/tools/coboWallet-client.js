const crypto = require('crypto');
const sha256 = require('sha256');
const bip66 = require('bip66');
const fetch = require('node-fetch');
const ec = new require('elliptic').ec('secp256k1')
const env = require('env2')('./.env');

const ZERO = Buffer.alloc(1, 0)

const host = process.env.COBO_WALLET_API_URL

const api_key = process.env.COBO_WALLET_API_KEY;
const api_secret = process.env.COBO_WALLET_API_SECRET;

const sig_type = 'ecdsa'

function toDER(x) {
    let i = 0
    while (x[i] === 0)++i
    if (i === x.length) return ZERO
    x = x.slice(i)
    if (x[0] & 0x80) return Buffer.concat([ZERO, x], 1 + x.length)
    return x
}


const sign_ecc = (message, api_secret) => {
    let privateKey = Buffer.from(api_secret, 'hex')
    let result = ec.sign(Buffer.from(sha256.x2(message), 'hex'), privateKey)
    var r = new Buffer(result.r.toString(16, 64), 'hex')
    var s = new Buffer(result.s.toString(16, 64), 'hex')
    r = toDER(r);
    s = toDER(s);
    return bip66.encode(r, s).toString('hex');
};

const sign_hmac = (message, api_secret) => {
    console.log(message)
    var x = crypto.createHmac('sha256', api_secret)
        .update(message)
        .digest('hex');
    console.log(x);
    return x
}

const coboFetch = (method, path, params, api_key, api_secret, host = 'https://api.sandbox.cobo.com') => {
    let nonce = String(new Date().getTime());
    let sort_params = Object.keys(params).sort().map((k) => {
        return k + '=' + encodeURIComponent(params[k]).replace(/%20/g, '+');
    }).join('&');
    let content = [method, path, nonce, sort_params].join('|');

    console.log(`content`)
    console.log(content)

    var signature = '';
    if (sig_type == 'ecdsa') {
        signature = sign_ecc(content, api_secret)
    } else if (sig_type == 'hmac') {
        signature = sign_hmac(content, api_secret)
    } else {
        throw "unexpected sig_type " + sig_type;
    }

    console.log(`signature`)
    console.log(signature)

    let headers = {
        'Biz-Api-Key': api_key,
        'Biz-Api-Nonce': nonce,
        'Biz-Api-Signature': signature
    };

    console.log(`headers`)
    console.log(headers)

    if (method == 'GET') {
        return fetch(host + path + '?' + sort_params, {
            'method': method,
            'headers': headers,
        });
    } else if (method == 'POST') {
        headers['Content-Type'] = "application/x-www-form-urlencoded";

        console.log(`host + path`)
        console.log(host + path)
        return fetch(host + path, {
            'method': method,
            'headers': headers,
            'body': sort_params
        });
    } else {
        throw "unexpected method " + method;
    }
}

export async function cobo_org_info() {
    return new Promise((resolve, reject) => {
        console.log(`调用查询cobo账户详情接口`)
        coboFetch('GET', '/v1/custody/org_info/', {},
            api_key, api_secret, host
        ).then(res => {
            // console.log(res)
            res.json().then((data) => {
                // console.log(JSON.stringify(data, null, 4));
                resolve(data)
            })
        }).catch(err => {
            console.log(err)
            reject(err)
        });
    })
}

export async function cobo_getNewAddres(coin = 'BTC_USDT') {
    return new Promise((resolve, reject) => {
        coboFetch('POST', '/v1/custody/new_address/', {
            "coin": coin
        },
            api_key, api_secret, host
        ).then(res => {
            console.log(res)
            res.json().then((data) => {
                console.log(JSON.stringify(data, null, 4));
                resolve(data)
            })
        }).catch(err => {
            console.log(err)
            reject(err)
        });
    })
}

export async function cobo_transaction_history(min_id = 0, side = 'withdraw') {
    return new Promise((resolve, reject) => {
        console.log(`调用查询cobo账户提现详情`)
        coboFetch('GET', '/v1/custody/transaction_history/', { min_id, side },
            api_key, api_secret, host
        ).then(res => {
            // console.log(res)
            res.json().then((data) => {
                // console.log(JSON.stringify(data, null, 4));
                resolve(data)
            })
        }).catch(err => {
            console.log(err)
            reject(err)
        });
    })
}

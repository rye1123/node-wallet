const crypto = require('crypto');
const fs = require('fs');
var path = require('path');

function signer(algorithm, key, data) {
    var sign = crypto.createSign(algorithm);
    sign.update(data);
    var sig = sign.sign(key, 'hex');
    return sig;
}

class Sign {

    /**
     * 获取rsa签名
     */
    getRSASign = async function (words, type) {
        return new Promise((resolve, reject) => {
            fs.readFile(process.cwd() + '/src/file/private_key.pem', 'utf-8', function (err, prikey) {
                if (err) {
                    reject(err)
                } else {
                    var algorithm = 'RSA-SHA1';
                    var privatePem = prikey;
                    var key = privatePem.toString();
                    var sig = signer(algorithm, key, words); //数字签名
                    resolve(sig)
                }
            });
        })
    }

    verify = function (sig, data) {
        var algorithm = 'RSA-SHA1';
        var publicPem = fs.readFileSync(process.cwd() + '/src/file/rsa_public_key.pub');
        var pubkey = publicPem.toString();
        var verify = crypto.createVerify(algorithm);
        verify.update(data);
        return verify.verify(pubkey, sig, 'hex')
    }
}

module.exports = new Sign();

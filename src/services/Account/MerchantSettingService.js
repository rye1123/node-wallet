import result from '../../tools/Result';
import Utils from '../../tools/Utils';
import Sign from '../../tools/Sign';
import sequelize from '../../lib/sequelize';
/* eslint-disable */
const { webDBUtil, Sequelize } = sequelize;
const userMerchantSettingModel = webDBUtil.import('../../models/Users/user_merchant_setting');
const userMerchantStrategyModel = webDBUtil.import('../../models/Users/user_merchant_strategy_withdraw');
/* eslint-endble */

/**
 * 业务示例代码
 */
module.exports = class MerchantSettingService {


    async withdraw_strategy_list(session) {
        try {

            let strategeList = await userMerchantStrategyModel.findAll({
                raw: true,
                where: {
                    isDelete: 0, user_id: session.parent_id || session.id
                }
            })

            return result.success('获取成功', strategeList);
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    async withdraw_strategy_add(session, data) {
        try {

            let existsCount = await userMerchantStrategyModel.count({
                where: {
                    isDelete: 0, user_id: session.id, symbol: data.symbol
                },
                col: 'id'
            })

            if (data.limit_day > 1000000 || data.limit_hour > 200000 || data.limit_per > 100000 || data.limit_per < 20) {
                return result.failed("策略限额错误", 4160138);
            }

            console.log(` existsCount - ${existsCount}`)
            if (existsCount < 1) {
                let strategyInfo = {
                    user_id: session.id,
                    limit_per: data.limit_per,
                    limit_hour: data.limit_hour,
                    limit_day: data.limit_day,
                    symbol: data.symbol,
                    isDelete: 0,
                    enable: 1
                }

                strategyInfo.sign = await Sign.getRSASign(JSON.stringify(strategyInfo), 'withdraw_strategy')
                strategyInfo.status = 1
                await userMerchantStrategyModel.create(strategyInfo)

                return result.success('策略信息创建完成');
            }
            else {
                return result.failed("每个币种仅可设置一个提币策略", 416018);
            }
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    async withdraw_strategy_edit(session, data) {
        try {

            let existsCount = await userMerchantStrategyModel.count({
                where: {
                    isDelete: 0, user_id: session.id, symbol: data.symbol
                },
                col: 'id'
            })

            if (data.limit_day > 1000000 || data.limit_hour > 200000 || data.limit_per > 100000 || data.limit_per < 20) {
                return result.failed("策略限额错误", 4160138);
            }

            console.log(` existsCount - ${existsCount}`)
            if (existsCount < 1) {
                let strategyInfo = {
                    user_id: session.id,
                    limit_per: data.limit_per,
                    limit_hour: data.limit_hour,
                    limit_day: data.limit_day,
                    symbol: data.symbol,
                    isDelete: 0,
                    enable: 1
                }

                strategyInfo.sign = await Sign.getRSASign(JSON.stringify(strategyInfo), 'withdraw_strategy')
                strategyInfo.status = 1
                await userMerchantStrategyModel.create(strategyInfo)

                return result.success('策略信息创建完成');
            }
            else {

                userMerchantStrategyModel.update({
                    password: Utils.getMd5(newpassword)
                }, {
                        where: { id: session.id, password: Utils.getMd5(oldpassword) },
                        fields: ['password']
                    });

                return result.success('策略信息修改完成');
            }
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    async withdraw_strategy_delete(session) {

    }

    async withdraw_strategy_disable(session) {

    }

    /**
     * 获取
     * @param {*} param0
     */
    async withdraw_callback_status(session) {
        try {
            let merchantSetting = await userMerchantSettingModel.findOne({
                where: {
                    user_id: session.parent_id || session.id
                }
            })

            if (merchantSetting) {
                return merchantSetting
            }
            return {};
        } catch (error) {
            console.log(error);
            return {};
        }
    }

    async update_withdraw_callback_url(session, { callback_url }) {
        try {
            let merchantSetting = await userMerchantSettingModel.findOne({
                where: {
                    user_id: session.id
                }
            })

            console.log(merchantSetting)

            if (!callback_url) {
                return false
            }

            if (merchantSetting) {
                await userMerchantSettingModel.update({
                    withdraw_callback_url: callback_url
                }, {
                        where: { user_id: session.id },
                        fields: ['withdraw_callback_url']
                    })
            }
            else {
                await userMerchantSettingModel.create({
                    user_id: session.id,
                    withdraw_callback_url: callback_url
                })
            }
            return true
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    /**
     * 添加
     * @param {*} user
     */
    async addDemo({ param }) {
        try {
            if (!param) return result.paramsLack();
            return result.success();
        } catch (error) {
            console.log(error)
            return result.failed();
        }
    }

    /**
     * 删除
     * @param {*} user
     */
    async delDemo(id) {
        try {
            if (!id) return result.paramsLack();
            return result.success();
        } catch (error) {
            console.log(error);
            return result.failed(`注销失败`);
        }
    }

    /**
     * 修改
     * @param {*} user
     */
    async updateDemo({ id, param }) {
        try {
            if (!id || !param) return result.paramsLack();
            return result.success();
        } catch (error) {
            console.log(error);
            return result.failed(`修改失败`);
        }
    }
};

import result from '../../tools/Result';
import sequelize from '../../lib/sequelize';
import middleware from '../../middleware';
import RedisDao from '../../lib/RedisDao';
import Sign from '../../tools/Sign';
var moment = require('moment');
import { MicroService, ThirdPartyService } from '../../config.service';
import { add, sub, mul } from "../../lib/operation"
import { queueConfig } from "../../lib/queueUtil"
import CommonService from '../Common/CommonService';
import { max } from 'moment';
// import { RedisConfig } from '../config';
const Queue = require('bee-queue');
var _ = require('lodash');

var commonService = new CommonService()

const { webDBUtil, Sequelize } = sequelize;
const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

const funds_listDAO = webDBUtil.import('../../models/Amount/funds_list');
const lock_margin_listDAO = webDBUtil.import('../../models/Amount/lock_margin_list');
const lock_order_listDAO = webDBUtil.import('../../models/Amount/lock_order_list');
const convertInfoDAO = webDBUtil.import('../../models/Wallet/wlt_convert_info');
const user_merchant_symbol_settingDAO = webDBUtil.import('../../models/Users/user_merchant_symbol_setting');
const wlt_symbol_infoDAO = webDBUtil.import('../../models/Wallet/wlt_symbol_info');

const merchantDAO = webDBUtil.import('../../models/Users/user_info');
const merchantKycDAO = webDBUtil.import('../../models/Users/user_merchant_kyc_setting');
const ks_biz_tokenDAO = webDBUtil.import('../../models/Users/user_kyc_ks_biz_token');
const kycInfoListDAO = webDBUtil.import('../../models/Users/user_kyc_info_list');
const symbolInfoDAO = webDBUtil.import('../../models/Wallet/wlt_symbol_info');
const ksVedioChkRecordDAO = webDBUtil.import('../../models/Users/user_kyc_ks_vedio_chk_record');
import request from '../../tools/request';


const API_ORDER_DETAIL_URL = `${MicroService.API_order_type}${MicroService.API_order_host}:${MicroService.API_order_port}/api/v1/_html/order/detail`

/**
 * 用户额度服务 AmountService
 * 本页面处理业务逻辑 接收参数与返回处理结果
 */
module.exports = class AmountService {

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async amount(session, { symbol }) {
        try {
            if (!symbol) {
                symbol = "USDT"
            }
            let amountInfo = await this.amountBase(session.parent_id || session.id, symbol)
            return result.success('获取成功', amountInfo);
        } catch (error) {
            return result.failed(error);
        }
    }

    async getAmountList(ids, symbol = "USDT") {
        ids = ids.filter(item => {
            return !!item
        })

        let amountCacheList = await RedisDao.mget(ids.map(item => { return `4usdt:account:balanceInfo:${item}-${symbol}` }))

        for (var i = 0; i < ids.length; i++) {
            if (ids[i]) {
                if (!amountCacheList[i]) {
                    amountCacheList[i] = await this.amountBase(ids[i], symbol)
                }
                else {
                    amountCacheList[i] = JSON.parse(amountCacheList[i])
                }

                delete amountCacheList[i].withdrawFee
                delete amountCacheList[i].withdrawMin
            }
        }

        return amountCacheList
    }

    async amountBaseList(id, app_id = "4USDT") {
        try {
            let symbolList = await RedisDao.get(`4usdt:wallet:symbol_info`)
            if (!symbolList) {
                symbolList = await symbolInfoDAO.findAll({
                    where: { enable: 1, is_delete: 0, app_id },
                    attributes: ["id", "name", "symbol", "display_name", "withdrawFee", "withdrawMin", "address_role"]
                })
                symbolList = JSON.stringify(symbolList)
                await RedisDao.set(`4usdt:wallet:symbol_info`, symbolList, 10)
            }
            symbolList = JSON.parse(symbolList)

            console.log(`symbolList`)
            console.log(symbolList)


            let feeList = []
            for (var i = 0; i < symbolList.length; i++) {
                let feeInfo = await RedisDao.get(`4usdt:merchant_fee_info:${id}-${symbolList[i].display_name}`)
                if (!feeInfo) {
                    feeInfo = await user_merchant_symbol_settingDAO.findOne({
                        where: {
                            symbol_id: symbolList[i].id,
                            merchantId: id
                        }
                    })
                    feeInfo = JSON.stringify(feeInfo)
                    RedisDao.set(`4usdt:merchant_fee_info:${id}-${symbolList[i].display_name}`, feeInfo, 10)
                }
                feeInfo = JSON.parse(feeInfo)
                feeList.push(feeInfo || {})
            }

            console.log(`feeList`)
            console.log(feeList)

            let amountList = []
            for (var i = 0; i < symbolList.length; i++) {
                let balance = await RedisDao.get(`4usdt:balance_info:${id}-${symbolList[i].display_name}`)
                if (!balance) {
                    balance = await this.amountBaseData(id, symbolList[i].display_name)
                    balance = JSON.stringify(balance)
                    RedisDao.set(`4usdt:balance_info:${id}-${symbolList[i].display_name}`, balance, 10)
                }
                balance = JSON.parse(balance)
                amountList.push(balance)
            }
            console.log(`amountList`)
            console.log(amountList)

            return _.merge(symbolList, amountList, feeList)
        }
        catch (err) {
            console.log(err)
        }
    }

    async amountBaseData(id, symbol = "USDT", endTime) {
        if (symbol == "BTC_USDT" || symbol == "ETH_USDT") {
            symbol = "USDT"
        }

        let queryScript = 'select if(isnull(sum_amount),0,sum_amount) as sum_amount,' +
            'if(isnull(freeze_margin_value),0,freeze_margin_value) as freeze_margin_value,' +
            'if(isnull(freeze_order_value),0,freeze_order_value) as freeze_order_value ' +
            'from (select sum(`value`) as sum_amount from funds_list where `symbol` = ? and `use` = 0 and userid = ?) as t1 ' +
            ' LEFT JOIN (select  sum(`value`) as freeze_margin_value from lock_margin_list where `symbol` = ? and `enable` = 1 and is_delete = 0 and userid = ?) as t2 on 1=1 ' +
            ' LEFT JOIN (select  sum(`value`) as freeze_order_value from lock_order_list where `symbol` = ? and `enable` = 1 and is_delete = 0 and userid = ?) as t3 on 1=1 '

        let paramsArr = [symbol, id, symbol, id, symbol, id]
        if (endTime) {
            queryScript =
                'select if(isnull(sum_amount),0,sum_amount) as sum_amount,' +
                'if(isnull(freeze_margin_value),0,freeze_margin_value) as freeze_margin_value,' +
                'if(isnull(freeze_order_value),0,freeze_order_value) as freeze_order_value ' +
                'from (select sum(`value`) as sum_amount from funds_list where `symbol` = ? and `use` = 0 and userid = ? and created_time < ?) as t1 ' +
                ' LEFT JOIN (select  sum(`value`) as freeze_margin_value from lock_margin_list where `symbol` = ? and `enable` = 1 and is_delete = 0 and userid = ? and created_time < ?) as t2 on 1=1 ' +
                ' LEFT JOIN (select  sum(`value`) as freeze_order_value from lock_order_list where `symbol` = ? and `enable` = 1 and is_delete = 0 and userid = ? and created_time < ?) as t3 on 1=1 '
            paramsArr = [symbol, id, endTime, symbol, id, endTime, symbol, id, endTime]
        }

        let amountInfo = await webDBUtil.query(queryScript, { raw: true, replacements: paramsArr, type: sequelize.QueryTypes.SELECT })
        return amountInfo[0]
    }

    async amountBase(id, symbol = "USDT", endTime) {
        let amountInfo = await this.amountBaseData(id, symbol, endTime)
        amountInfo._uid = id

        amountInfo.available = amountInfo.sum_amount - amountInfo.freeze_margin_value - amountInfo.freeze_order_value

        await RedisDao.set(`4usdt:account:balanceInfo:${id}-${symbol}`, JSON.stringify(amountInfo), 60 * 6)

        amountInfo.withdrawFee = 5
        amountInfo.withdrawMin = 100
        amountInfo.withdrawFeeETH = 3
        return amountInfo
    }

    /**
     * 获取商户额度信息
     * @param {*} ctx
     */
    async merchantAmount(id, symbol = "USDT") {

        try {
            let queryData = {
                where: {
                    id: id
                },
                raw: true,
                attributes: ["id", "merchantId", ["name", "merchantName"], "merchantStatus", "merchantProfit", "merchantAppendFee"]
            };

            let queryScript = 'select if(isnull(sum_amount),0,sum_amount) as sum_amount,' +
                'if(isnull(freeze_margin_value),0,freeze_margin_value) as freeze_margin_value,' +
                'if(isnull(freeze_order_value),0,freeze_order_value) as freeze_order_value ' +
                'from (select sum(`value`) as sum_amount from funds_list where `symbol` = ? and `use` = 0 and userid = ?) as t1 ' +
                ' LEFT JOIN (select  sum(`value`) as freeze_margin_value from lock_margin_list where `symbol` = ? and `enable` = 1 and is_delete = 0 and userid = ?) as t2 on 1=1 ' +
                ' LEFT JOIN (select  sum(`value`) as freeze_order_value from lock_order_list where `symbol` = ? and `enable` = 1 and is_delete = 0 and userid = ?) as t3 on 1=1 '

            let where = { raw: true, replacements: [symbol, id, symbol, id, symbol, id], type: sequelize.QueryTypes.SELECT }

            console.log(JSON.stringify(where))

            let amountInfo = await webDBUtil.query(queryScript, where)

            console.log(amountInfo)

            amountInfo = amountInfo[0]

            amountInfo.available = amountInfo.sum_amount - amountInfo.freeze_margin_value - amountInfo.freeze_order_value
            return result.success('获取成功', amountInfo);
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }

    /**
     * 获取商户利率信息
     * @param {*} ctx
     */
    async merchantProfit(id, { symbol = 'BTC_USDT', app_id = '4USDT' }) {
        try {
            var cacheKey = `4usdt:merchant_fee_info:${id}-${symbol.replace('BTC_', '')}`
            let queryData = {
                where: {
                    id: id
                },
                raw: true,
                attributes: ["id", "name", "merchantId", "merchantStatus", ["name", "merchantName"], "merchant_max_cancel_order_day", "merchant_max_order_concurrent"]
            };

            let userInfo = await merchantDAO.findOne(queryData)
            let merchantSymbolSetting = {}
            if (await RedisDao.exists(cacheKey)) {
                merchantSymbolSetting = JSON.parse(await RedisDao.get(cacheKey))
            }
            else {
                const symbolInfo = await wlt_symbol_infoDAO.findOne({
                    where: {
                        app_id: app_id,
                        symbol: symbol
                    }
                })
                merchantSymbolSetting = await user_merchant_symbol_settingDAO.findOne({
                    where: {
                        merchantId: id,
                        symbol_id: symbolInfo.id
                    }
                })
                if (!merchantSymbolSetting) {
                    merchantSymbolSetting = await user_merchant_symbol_settingDAO.create({
                        symbol_id: symbolInfo.id,
                        merchantId: id || id
                    })
                }
                await RedisDao.set(cacheKey, JSON.stringify(merchantSymbolSetting), 100)
            }
            userInfo.merchantBuyProfit = merchantSymbolSetting.merchantBuyProfit
            userInfo.merchantSellProfit = merchantSymbolSetting.merchantSellProfit
            userInfo.merchantAppendFee = merchantSymbolSetting.merchantAppendFee
            userInfo.merchantPlatformBuyProfit = merchantSymbolSetting.merchantPlatformBuyProfit
            userInfo.merchantPlatformSellProfit = merchantSymbolSetting.merchantPlatformSellProfit

            userInfo.kycConfig = await this.merchant_kyc_info(id)

            // userInfo.merchant_max_cancel_order_day = merchantSymbolSetting.merchant_max_cancel_order_day
            // userInfo.merchant_max_order_concurrent = merchantSymbolSetting.merchant_max_order_concurrent
            return result.success(null, userInfo);
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }

    async orderDetail({ orderNo, token, biztoken }) {

        let resData = {};
        console.log(orderNo)
        orderNo = orderNo.match(/\d+/)[0]

        let { response, body } = await request.post(
            API_ORDER_DETAIL_URL, { orderNo: orderNo, token: token }
        )

        let orderInfo = body.data

        if (biztoken) {
            console.log(`请求中包含biztoken，通过ks获取验证结果是否正确`)
            let ks_result = await commonService.ks_get_result({ biz_token: biztoken })
            await ksVedioChkRecordDAO.create({
                biz_token: biztoken,
                msg: ks_result
            })

            ks_result = JSON.parse(ks_result)
            console.log(`ks_result ${typeof ks_result}`)
            //结果正确，直接返回订单详情
            if (!ks_result.error && ks_result.biz_no == orderNo && ks_result.result_code == 1000) {
                delete orderInfo.kycName
                delete orderInfo.kycIdNo

                await kycInfoListDAO.update({
                    enable: 1
                }, {
                    where: {
                        user_id: orderInfo.merchantId,
                        order_no: orderNo,
                        kyc_name: kycName,
                        kyc_id_no: kycIdNo,
                        is_delete: 0
                    },
                    enable: ['password']
                });
                resData = result.success(body.msg, orderInfo)
                return resData
            }
        }

        if (body.code == 0) {

            let { kycName, kycIdNo } = body.data

            delete body.data.kycName
            delete body.data.kycIdNo

            //未付款订单
            if (body.data.status == "PAY_NO") {
                //必须是用户买单
                if (body.data.side == "BUY") {
                    // 获取商户kyc设置
                    let kycSetting = await this.merchant_kyc_info(body.data.merchantId)
                    console.log(kycSetting)

                    let enable = parseFloat(kycSetting.vedio_id_check_min_amount) <= parseFloat(body.data.realAmount)

                    let kycInfoExists = await kycInfoListDAO.count({
                        where: {
                            user_id: orderInfo.merchantId,
                            order_no: orderNo,
                            kyc_name: kycName,
                            kyc_id_no: kycIdNo,
                            is_delete: 0
                        }
                    })

                    if (kycInfoExists == 0) {
                        await kycInfoListDAO.create({
                            user_id: orderInfo.merchantId,
                            order_no: orderNo,
                            abs_amount: orderInfo.realAmount,
                            kyc_name: kycName,
                            kyc_id_no: kycIdNo,
                            phone_no: orderInfo.mobile,
                            source: "BITMALL",
                            enable: enable
                        })
                    }

                    //商户是否需要认证
                    if (kycSetting.vedio_id_check) {

                        console.log('执行认证内容')
                        let curVedioChkRecord = await ks_biz_tokenDAO.findOne({
                            where: {
                                request_id: orderNo,
                                user_id: body.data.merchantId,
                                isDelete: 0,
                            },
                            'order': [
                                ['id', 'DESC'],
                            ]
                        })

                        if (curVedioChkRecord && curVedioChkRecord.status == 1000) {
                            resData = result.success(body.msg, orderInfo)
                        }
                        else {
                            let mustVerify = parseFloat(kycSetting.vedio_id_check_max_amount) <= parseFloat(orderInfo.realAmount)

                            if (!mustVerify) {
                                //当前订单的验证次数
                                let chkCount = await ks_biz_tokenDAO.count({
                                    where: {
                                        request_id: orderNo,
                                        user_id: body.data.merchantId,
                                        isDelete: 0,
                                    }
                                })

                                console.log(`当前订单验证次数：${chkCount}/${kycSetting.vedio_id_check_retry_count}`)

                                if (chkCount > (kycSetting.vedio_id_check_retry_count - 1)) {
                                    return result.success(body.msg, {
                                        face_vali_url: `error`
                                    })
                                }
                                //没有记录，重新生成签名以及验证请求，并让前端跳转至视频验证
                                else {
                                    let verifyCount = await kycInfoListDAO.count({
                                        where: {
                                            user_id: orderInfo.merchantId,
                                            kyc_name: kycName,
                                            kyc_id_no: kycIdNo,
                                            is_delete: 0,
                                            enable: 1
                                        }
                                    })
                                    console.log(`总验证订单次数[${verifyCount}],策略验证周期[${kycSetting.vedio_id_check_count_cycle}]`)

                                    //vedio_id_check_count_cycle 是0的话，只第一次做人脸验证
                                    if (kycSetting.vedio_id_check_count_cycle == 0) {
                                        //第一次超过最低限额的订单
                                        if (verifyCount == 1) {
                                            console.log('第一次做人脸验证')
                                            mustVerify = true
                                        }
                                        else {
                                            console.log(`非第一次不做人脸验证，当前第${verifyCount}`)
                                            mustVerify = false
                                        }
                                    }
                                    else {
                                        //每vedio_id_check_count_cycle次，需要验证一次人脸识别
                                        if ((verifyCount % kycSetting.vedio_id_check_count_cycle) == 0) {
                                            mustVerify = true
                                        }
                                        else {
                                            mustVerify = false
                                        }
                                    }
                                }
                            }

                            console.log(`mustVerify-${mustVerify}`)

                            if (mustVerify) {
                                console.log(`进行ks签名，获取biz_token`)
                                //return url修改的话，页面也要对应修改获取参数的方式
                                let signData = {
                                    orderNo: orderNo,
                                    idcard_name: kycName,
                                    idcard_number: kycIdNo,
                                    return_url: `${ThirdPartyService.API_Ks_Return_url}?token=${token}&orderNo=${orderNo}`
                                }
                                console.log(`创建新的faceid签名`)

                                console.log(signData)
                                let bizToken = await commonService.ks_get_biz_token(signData)
                                console.log(bizToken)

                                await ks_biz_tokenDAO.create({
                                    user_id: body.data.merchantId,
                                    request_id: bizToken.biz_no,
                                    biz_token: bizToken.biz_token,
                                    time_used: bizToken.time_used,
                                    status: 0
                                })
                                resData = result.success(body.msg, {
                                    face_vali_url: `${ThirdPartyService.API_Ks_FaceId_url}${bizToken.biz_token}`
                                })
                            }
                            else {
                                resData = result.success(body.msg, body.data)
                            }
                        }
                    }
                    else {
                        resData = result.success(body.msg, body.data)
                    }
                }
                else {
                    resData = result.success(body.msg, body.data)
                }

            }
            else {
                resData = result.success(body.msg, body.data)
            }
        }
        else {
            resData = result.failed('订单状态异常', body.code)
        }
        console.log(resData)
        return resData
    }

    async setKsResult(bizToken, code, msg) {

        await ks_biz_tokenDAO.update({
            status: code,
            remark: msg
        }, {
            where: { biz_token: bizToken, isDelete: 0 },
            fields: ['status', 'remark']
        })

    }


    async merchant_kyc_info(merchantId) {
        var cacheKey = `4usdt:merchant_kyc_info:${merchantId}`
        let kycInfo = {}
        if (await RedisDao.exists(cacheKey)) {
            kycInfo = JSON.parse(await RedisDao.get(cacheKey))
        }
        else {
            let filter = {
                where: {
                    user_id: merchantId
                },
                attributes:
                    [
                        'id_check',
                        'id_check_with_phone',
                        'vedio_id_check',
                        'vedio_id_check_min_amount',
                        'vedio_id_check_max_amount',
                        'vedio_id_check_count_cycle',
                        'vedio_id_check_retry_count'
                    ]
            }
            kycInfo = await merchantKycDAO.findOne(filter)

            if (!kycInfo) {
                await merchantKycDAO.create({
                    user_id: merchantId
                })
                kycInfo = await merchantKycDAO.findOne(filter)
            }

            await RedisDao.set(cacheKey, JSON.stringify(kycInfo), 100)

        }
        return kycInfo
    }

    /**
     * 获取用户充值地址
     * @param {*} ctx
     */
    async address(session, query) {
        ctx.body = await ggService.address({ jwt: ctx.request.header.authorization }, ctx.request.query);
    }

    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async history(session, { page = 1, per_page = 10, order = 0, type = 0, symbol, start, end, side, source, userid = 0 }) {
        try {
            let max_per_page_size = 100
            let sortby = 'id'
            let queryData = {
                where: { userid: session.parent_id || session.id, value: { [Sequelize.Op.ne]: 0 }, use: 0 },
                raw: true,
                order: [
                    [sortby, (order == 0 ? 'DESC' : 'ASC')]
                ],
                attributes: ["id", "userid", "orderNo", "value", "created_time", "symbol", "source", "log_amount"]
            };

            if (queryData.where.userid == 0 || queryData.where.userid == undefined) {
                queryData.where.userid = { [Sequelize.Op.ne]: 1 }
            }
            if (userid) {
                queryData.where.userid = userid
            }


            if (source && source.toUpperCase() != 'ALL') {
                queryData.where.source = source
            }

            if (source && source.indexOf(',') > 0) {
                max_per_page_size = 10000
                let sourceArr = [];
                source.split(',').map(itm => {
                    if (itm) {
                        sourceArr.push(itm.toUpperCase())
                    }
                })
                if (sourceArr.length > 0) {
                    queryData.where.source = { [Sequelize.Op.in]: sourceArr }
                }
            }

            // if (type != 0) {
            //     queryData.where.value = {}
            //     if (type > 0) {
            //         queryData.where.value.$gt = 0
            //     } else {
            //         queryData.where.value.$lt = 0
            //     }
            // }

            if (symbol) {
                queryData.where.symbol = symbol
            }

            if (side) {
                queryData.where.source = side
            }

            if (start && end) {
                end = `${end.indexOf(' ') < 0 ? `${end} 23:59:59` : end}`
                queryData.where.created_time = {}
                queryData.where.created_time = { [Sequelize.Op.gte]: start, [Sequelize.Op.lte]: end }
            }
            else {
                if (start) {
                    queryData.where.created_time = {}
                    queryData.where.created_time = { [Sequelize.Op.gt]: start }
                }
                if (end) {
                    if (queryData.where.created_time) {
                        queryData.where.created_time = { [Sequelize.Op.lt]: end }
                    } else {
                        queryData.where.created_time = {}
                        queryData.where.created_time = { [Sequelize.Op.lt]: end }
                    }
                }
            }


            //分页
            if (page && per_page) {
                queryData.offset = Number((page - 1) * per_page); //开始的数据索引
                queryData.limit = Number(per_page); //每页限制返回的数据条数
            };
            if (per_page > 100) {
                per_page = max_per_page_size
            }

            console.log(queryData)
            const { rows, count } = await funds_listDAO.findAndCountAll(queryData);
            return result.pageData(null, null, rows, count, page, per_page);
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }

    async marginUnAllFreeze(session) {
        try {
            await lock_margin_listDAO.update({
                enable: false,
                is_delete: 1
            }, {
                where: { userid: session.id },
                fields: ['enable', 'is_delete']
            });

            await merchantDAO.update({
                tradersMarginValue: 0,
                merchantMarginValue: 0
            }, {
                where: { id: session.id },
                fields: ['tradersMarginValue', 'merchantMarginValue']
            })

            return result.success("保证金解冻成功");
            // }
        } catch (error) {
            return result.failed(error);
        }
    }

    /**
     * 冻结保证金
     * @param {*} session
     */
    async marginFreeze(session, { value, symbol = "USDT", source = "4USDT" }) {
        try {
            if (!value) return result.failed('参数错误');
            // let queryData = {
            //     where: {
            //         userid: session.id,
            //         source,
            //         symbol,
            //         enable: 1
            //     },
            //     raw: true,
            //     lock: Sequelize.Transaction.LOCK.UPDATE
            // };

            // const marginFreezeInfo = await lock_margin_listDAO.findOne(queryData);
            // if (marginFreezeInfo) {
            //     return result.failed('已冻结保证金，请勿重复操作', 405601);
            // } else {
            let amountInfo = await this.amount(session, { symbol })

            if (parseInt(amountInfo.data.available) >= parseInt(value)) {
                await lock_margin_listDAO.create({
                    userid: session.id,
                    value,
                    symbol,
                    source,
                    enable: 1,
                    sign: ''
                })


                await merchantDAO.update({
                    tradersMarginValue: value,
                    merchantMarginValue: value
                }, {
                    where: { id: session.id },
                    fields: ['tradersMarginValue', 'merchantMarginValue']
                })

                return result.success("保证金冻结成功");
            } else {
                return result.failed('余额已不足，请充值后再次操作', 405602);
            }
            // }
        } catch (error) {
            return result.failed();
        }
    }

    /**
     * 解冻保证金
     * @param {*} session
     */
    async marginUnfreeze(session, { order_no, google_code, phone_code, mail_code, funds_password }) {
        try {
            if (!order_no) return result.failed('参数错误');

            let queryData = {
                where: {
                    userid: session.id,
                    // is_delete: 0,
                    // enable: 1
                },
                raw: true,
            };

            const marginFreezeInfo = await lock_margin_listDAO.findOne(queryData);
            if (marginFreezeInfo) {

                if (marginFreezeInfo.is_delete) {
                    return result.failed("保证金冻结已删除", 405604);
                } else if (!marginFreezeInfo.enable) {
                    return result.failed("保证金冻结已不可用，请勿重复操作", 405605);
                } else {
                    lock_margin_listDAO.update({
                        enable: false
                    }, {
                        where: { userid: session.id },
                        fields: ['enable']
                    });

                    return result.success("保证金冻结解冻成功");
                }
            } else {
                return result.failed('没有找到冻结信息', 405603);
            }
        } catch (error) {
            return result.failed();
        }
    }

    async createOrderCompleteJob(data) {
        return new Promise((resolve, reject) => {
            try {

                var queue = new Queue(`order_complete`, queueConfig)

                var job = queue.createJob(data).timeout(2000).retries(3)

                job.on('succeeded', function (result) {
                    console.log('完成订单-事务 succeeded ' + job.id);
                    resolve(job.id);
                });

                job.save(function (err, job) {
                    if (err) {
                        console.log('完成订单-事务-发生错误 ' + err);
                    }
                    console.log('完成订单-事务 save ' + job.id);
                });

            } catch (error) {
                reject(error)
            }
        });
    }

    async createOrderFreezeJob(data) {
        return new Promise((resolve, reject) => {
            try {

                var queue = new Queue(`lock`, queueConfig)

                var job = queue.createJob(data).timeout(2000).retries(3)

                job.on('succeeded', function (result) {
                    console.log('冻结事务 succeeded ' + job.id);
                    resolve(job.id);
                });

                job.save(function (err, job) {
                    if (err) {
                        console.log('冻结订单事务发生错误 ' + err);
                    }
                    console.log('冻结事务 save ' + job.id);
                });

            } catch (error) {
                reject(error)
            }
        });
    }

    /**
     * 冻结订单金额
     * @param {*} session
     */
    async orderFreeze(session, { value, order_no, symbol = "USDT", source = "4USDT", convert }) {
        try {
            if (!value || !order_no) return result.failed('参数错误');

            let josResult = await this.createOrderFreezeJob({ session, data: { value, order_no, symbol, source, convert } })

            let queueResult = await RedisDao.get(`order:lock_order_result_${josResult}`)

            if (queueResult) {
                return JSON.parse(queueResult)
            }
            else {
                return result.failed("冻结失败", 40056432);
            }
        } catch (error) {
            console.log(error)
            return result.failed("冻结失败", 40056032);
        }
    }

    /**
     * 解冻订单金额，取消订单使用
     * @param {*} session
     */
    async orderUnfreeze(session, { order_no }) {
        try {
            if (!order_no) return result.failed('参数错误');

            let queryData = {
                where: {
                    orderNo: order_no,
                    is_delete: 0
                },
                raw: true,
            };

            const orderFreezeInfo = await lock_order_listDAO.findOne(queryData);

            console.log(orderFreezeInfo)
            if (orderFreezeInfo) {

                if (orderFreezeInfo.is_delete) {
                    return result.failed("冻结订单已删除", 405604);
                } else if (!orderFreezeInfo.enable) {
                    return result.failed("冻结订单已不可用，请勿重复操作", 405605);
                } else {
                    lock_order_listDAO.update({
                        is_delete: 1,
                        enable: 0
                    }, {
                        where: { orderNo: order_no },
                        fields: ['is_delete', 'enable']
                    });

                    return result.success("订单资金解冻成功");
                }
            } else {
                return result.failed('没有找到冻结信息', 405603);
            }
        } catch (error) {
            return result.failed();
        }
    }


    /**
     * 订单完成转账（自动解冻并转账）
     * @param {*} session
     */
    async transfer_amount(session, { order_no, founds }) { //to_user_id
        try {
            if (session) {

                let josResult = await this.createOrderCompleteJob({ session, data: { order_no, founds } })

                let result = await RedisDao.get(`order:complete_order_result_${josResult}`)

                if (result) {
                    return JSON.parse(result)
                }
                else {
                    return result.failed("订单完成-失败", 4005659);
                }
            }
        } catch (error) {
            console.log(error)
            return result.failed("订单操作异常", 50041);
        }
    }

    async rollbackWithdraw({ no_id }) {
        try {
            let rollbackFundList = await funds_listDAO.findAll({
                where: {
                    orderNo: no_id,
                    use: 0
                }
            })
            // console.log(JSON.stringify(rollbackFundList))
            let foundsList = []
            let rollbackFund = {}
            let rollbackfee = 0

            if (rollbackFundList.length != 3) {
                return false
            }

            await rollbackFundList.map(async item => {
                if (item.source == "WITHDRAW") {
                    let amountInfo = await this.amountBase(item.userid, item.symbol)
                    rollbackFund = {
                        source: "ROLLBACK",
                        symbol: item.symbol,
                        value: Math.abs(item.value),
                        createUserId: 1,
                        orderNo: item.orderNo,
                        userid: item.userid,
                        log_amount: `${add(amountInfo.sum_amount, item.value)}`
                    }
                }
                else if (item.source == "WITHDRAW_FEE_OUT") {

                }
                else if (item.source == "WITHDRAW_FEE_IN") {
                    rollbackfee = item.value
                }
            })

            let rollbackAmountInfo = await this.amountBase(rollbackFund.userid, rollbackFund.symbol)
            let foudInfo_fee_back = {
                source: "ROLLBACK",
                symbol: rollbackFund.symbol || "USDT",
                value: `${rollbackfee}`,
                createUserId: 1,
                orderNo: no_id,
                userid: rollbackFund.userid,
                log_amount: `${add(rollbackAmountInfo.sum_amount, rollbackfee)}`
            }

            let systemAmountInfo = await this.amountBase(1)
            let foudInfo_fee_platform_back = {
                source: "ROLLBACK",
                symbol: rollbackFund.symbol || "USDT",
                value: `-${rollbackfee}`,
                createUserId: 1,
                orderNo: no_id,
                userid: 1,
                log_amount: `${sub(systemAmountInfo.sum_amount, rollbackfee)}`
            }

            rollbackFund.sign = await Sign.getRSASign(JSON.stringify(rollbackFund), 'withdraw_rollback')
            foudInfo_fee_back.sign = await Sign.getRSASign(JSON.stringify(foudInfo_fee_back), 'withdraw_rollback')
            foudInfo_fee_platform_back.sign = await Sign.getRSASign(JSON.stringify(foudInfo_fee_platform_back), 'withdraw_rollback')

            await funds_listDAO.bulkCreate([rollbackFund, foudInfo_fee_back, foudInfo_fee_platform_back]);
            return true
        } catch (error) {
            console.log(error)
            return false
        }
    }

    // /**
    //  * 提现
    //  * @param {*} session
    //  */
    // async !withdraw(session, body) {
    //     try {
    //         const validate = validateTools.validateJWT(jwt);

    //         if (validate) { }

    //     } catch (error) {
    //         console.log(error);
    //         return result.failed();
    //     }
    // }

    /**
     * 充值
     * @param {*} session
     */
    async deposit(session, body) {
        try {
            const validate = validateTools.validateJWT(jwt);

            if (validate) { }

        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }
}

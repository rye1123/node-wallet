import result from '../../tools/Result';
import Utils from '../../tools/Utils';
import sequelize from '../../lib/sequelize';
import middleware from '../../middleware';
import RedisDao from '../../lib/RedisDao';
var UUID = require("uuid")
var sendemail = require('sendemail')
var emailHelper = sendemail.email;
import Crypto from '../../tools/Secret';
var moment = require('moment');


const { webDBUtil, Sequelize } = sequelize;
const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

const user_infoDAO = webDBUtil.import('../../models/Users/user_info');
const user_role_config_menuDAO = webDBUtil.import('../../models/Users/user_role_config_menu');
const user_roles_configDAO = webDBUtil.import('../../models/Users/user_roles_config');
const user_merchant_symbol_settingDAO = webDBUtil.import('../../models/Users/user_merchant_symbol_setting');
const wlt_symbol_infoDAO = webDBUtil.import('../../models/Wallet/wlt_symbol_info');

const apiInfoDAO = webDBUtil.import('../../models/Security/merchant_api_info');
import AmountService from './AmountService';
const amountService = new AmountService()

/**
 * 用户信息服务 AccountService
 * 本页面处理业务逻辑 接收参数与返回处理结果
 */
module.exports = class AccountService {

    /**
     * 获取商户Secret
     * @param {*} ctx
     */
    async clientSecretByApiKey(apiKey) {
        try {
            console.log(apiKey)

            let cacheApi = await RedisDao.get(`4usdt:apiinfo:api:${apiKey}`)

            if (cacheApi) {
                cacheApi = JSON.parse(cacheApi)
                let api_secret = Crypto.decryptByDES(cacheApi.api_secret)
                return result.success(null, { user_id: cacheApi.user_id, secret: api_secret });
            }

            let queryData = {
                where: {
                    api_key: apiKey,
                    is_delete: 0,
                    enable: 1
                },
                raw: true,
                attributes: ["user_id", "api_secret", "bind_ip"]
            };

            const apiInfo = await apiInfoDAO.findOne(queryData);
            if (apiInfo) {
                // let secret = Utils.getMd5(`4usdt-CLIENT:SECRET${JSON.stringify(userInfo)}`)
                await RedisDao.set(`4usdt:apiinfo:api:${apiKey}`, JSON.stringify(apiInfo), 60 * 60)
                let api_secret = Crypto.decryptByDES(apiInfo.api_secret)
                // RedisDao.set(`4usdt-CLIENT:SECRET:${userId}`, secret, 60 * 60 * 24 * 30)

                return result.success(null, { user_id: apiInfo.user_id, secret: api_secret });
            } else {
                return result.failed('商户不存在', 68040);
            }
        } catch (error) {
            return result.failed(error);
        }
    }

    /**
     * 获取商户Secret
     * @param {*} ctx
     */
    async clientSecret(userId) {
        try {
            let queryData = {
                where: {
                    id: userId
                },
                raw: true,
                attributes: ["id", "mobile"]
            };

            const userInfo = await user_infoDAO.findOne(queryData);
            if (userInfo) {
                let secret = Utils.getMd5(`4usdt-CLIENT:SECRET${JSON.stringify(userInfo)}`)

                RedisDao.set(`4usdt-CLIENT:SECRET:${userId}`, secret, 60 * 60 * 24 * 30)

                return result.success(null, { secret });
            } else {
                return result.failed('商户不存在', 68040);
            }
        } catch (error) {
            return result.failed(error);
        }
    }

    async userRoleInfo(id) {

        let roleInfo = await RedisDao.get(`4usdt:user_role_info:${id}`)
        console.log(!!roleInfo)
        if (roleInfo) {
            return JSON.parse(roleInfo)
        }
        else {
            const userRoleConfig = await user_roles_configDAO.findOne({
                where: {
                    user_id: id
                }
            });

            RedisDao.set(`4usdt:user_role_info:${id}`, JSON.stringify(userRoleConfig), 60)
            return userRoleConfig
        }


    }

    async roleList(session) {
        try {
            let queryData = {
                where: {
                    id: session.id
                },
                raw: true,
                attributes: ["id", "mobile", "parent_id", "email", "name", "tradersNo", "tradersStatus", "authGoogleCode", "authPhoneCode", "authMailCode",
                    "authFundsPassword", "merchantId", "merchantName", "merchantStatus"
                ]
            };

            const userInfo = await user_infoDAO.findOne(queryData)

            if (userInfo.parent_id != 0) {
                const userRoleConfig = await this.userRoleInfo(session.id)

                return result.success(null, userRoleConfig);
            }
            else {
                return result.failed('非子账户', 2913930);
            }

        } catch (error) {
            return result.failed(error);
        }
    }

    /**
     * 获取用户信息
     * @param {*} ctx
     */
    async roleConfig(session) {
        try {
            let queryData = {
                where: {
                    id: session.id
                },
                raw: true,
                attributes: ["id", "mobile", "parent_id", "email", "name", "tradersNo", "tradersStatus", "authGoogleCode", "authPhoneCode", "authMailCode",
                    "authFundsPassword", "merchantId", "merchantName", "merchantStatus"
                ]
            };

            const userInfo = await user_infoDAO.findOne(queryData)
            let menuQueryParams = { everyone: 1 }

            if (userInfo.parent_id != 0) {
                const userRoleConfig = await this.userRoleInfo(session.id)

                if (userRoleConfig.administrator) {
                    menuQueryParams.administrator = 1
                }
                else if (userRoleConfig.auditor) {
                    menuQueryParams.auditor = 1
                }
                else if (userRoleConfig.operator) {
                    menuQueryParams.operator = 1
                }
                else if (userRoleConfig.observer) {
                    menuQueryParams.observer = 1
                }
                else if (userRoleConfig.trader) {
                    menuQueryParams.trader = 1
                }
            }
            else {
                menuQueryParams.administrator = 1
            }


            let menu = await user_role_config_menuDAO.findAll({
                where: {
                    [Sequelize.Op.or]: menuQueryParams
                },
                order: [
                    ["sort", "asc"]
                ],
            })

            return result.success(null, menu);
        } catch (error) {
            return result.failed(error);
        }
    }

    /**
     * 获取用户信息
     * @param {*} ctx
     */
    async accountInfo(session) {
        try {
            let queryData = {
                where: {
                    id: session.id
                },
                raw: true,
                attributes: ["id", "area_code", "mobile", "email", "name", "tradersNo", "tradersStatus", "authGoogleCode", "authPhoneCode", "authMailCode",
                    "authFundsPassword", "merchantId", "merchantName", "merchantStatus"
                ]
            };

            const userInfo = await user_infoDAO.findOne(queryData);
            return result.success(null, userInfo);
        } catch (error) {
            return result.failed(error);
        }
    }

    /**
     * 获取用户信息
     * @param {*} ctx
     */
    async accountInfoByMobile(mobile) {
        try {
            let queryData = {
                where: {
                    mobile: mobile
                },
                raw: true,
                attributes: ["id", "area_code", "mobile", "email", "name"
                ]
            };

            const userInfo = await user_infoDAO.findOne(queryData);
            return userInfo;
        } catch (error) {
            return result.failed(error);
        }
    }

    /**
     * 获取用户基本信息
     * @param {*} ctx
     */
    async baseInfo(session) {
        try {
            let queryData = {
                where: {
                    id: session.id
                },
                raw: true,
                attributes: ["parent_id", "mobile", "email", "name", ["name", "displayName"], "kyc_level"]
            };

            let userInfo = await user_infoDAO.findOne(queryData);
            userInfo.displayTitle = "商户"

            if (userInfo.parent_id) {
                let user_role_info = await this.userRoleInfo(session.id)

                if (user_role_info.administrator) {
                    userInfo.displayTitle = "管理员"
                }
                else if (user_role_info.auditor) {
                    userInfo.displayTitle = "审核员"
                }
                else if (user_role_info.operator) {
                    userInfo.displayTitle = "操作员"
                }
                else if (user_role_info.observer) {
                    userInfo.displayTitle = "观察员"
                }
                else if (user_role_info.trader) {
                    userInfo.displayTitle = "交易员"
                }
            }

            //获取余额信息
            let amountInfo = await amountService.amountBase(session.id)

            userInfo.freeze_margin_value = parseFloat(amountInfo.freeze_margin_value)
            return result.success(null, userInfo);
        } catch (error) {
            return result.failed(error);
        }
    }

    /**
     * 获取用户基本信息
     * @param {*} ctx
     */
    async updatePassword(session, { oldpassword, newpassword }) {
        try {
            if (oldpassword == newpassword) {
                return result.failed("新旧密码不可一致", 40602);
            }
            let queryData = {
                where: {
                    id: session.id,
                    password: Utils.getMd5(oldpassword)
                },
                raw: true,
                attributes: ["id"]
            };
            const userInfo = await user_infoDAO.findOne(queryData);

            if (userInfo) {
                await user_infoDAO.update({
                    password: Utils.getMd5(newpassword)
                }, {
                    where: { id: session.id, password: Utils.getMd5(oldpassword) },
                    fields: ['password']
                });
                return result.success("密码修改成功", 1);
            } else {
                return result.failed("旧密码错误", 40605);
            }
        } catch (error) {
            return result.failed(error);
        }
    }

    /**
     * 获取用户基本信息
     * @param {*} ctx
     */
    async resetPassword({ account, newpassword }) {
        try {
            let queryData = {
                where: {
                    mobile: account,
                },
                raw: true,
                attributes: ["id", "mobile"]
            };
            const userInfo = await user_infoDAO.findOne(queryData);

            if (userInfo) {
                await user_infoDAO.update({
                    password: Utils.getMd5(newpassword)
                }, {
                    where: { id: userInfo.id },
                    fields: ['password']
                });
                return result.success("密码修改成功", 1);
            } else {
                return result.failed("账户不存在", 40603);
            }
        } catch (error) {
            return result.failed(error);
        }
    }

    /**
     * 获取用户基本信息
     * @param {*} ctx
     */
    async setNewPassword({ account, newpassword }) {
        try {
            let queryData = {
                where: {
                    mobile: account
                },
                raw: true,
                attributes: ["id", "mobile", "isMerchants", "merchantStatus", "isTraders", "tradersStatus", 'merchantMarginValue', 'tradersMarginValue']
            };
            const userInfo = await user_infoDAO.findOne(queryData);

            let updateInfo = {
                password: Utils.getMd5(newpassword)
            }

            let updateFiledsArray = ["password"]

            //激活
            if (userInfo.isMerchants && userInfo.merchantStatus == 0) {
                if (userInfo.merchantMarginValue == 0) {
                    updateInfo.merchantStatus = 3
                }
                else {
                    updateInfo.merchantStatus = 1
                }
                updateFiledsArray.push("merchantStatus")
            }
            else if (userInfo.isTraders && userInfo.tradersStatus == 0) {
                if (userInfo.tradersMarginValue == 0) {
                    updateInfo.tradersStatus = 3
                }
                else {
                    updateInfo.tradersStatus = 1
                }
                updateFiledsArray.push("tradersStatus")
            }

            // console.log(updateInfo)


            // console.log(updateFiledsArray)

            if (userInfo) {
                await user_infoDAO.update(updateInfo, {
                    where: { id: userInfo.id },
                    fields: updateFiledsArray
                });
                return result.success("设置密码成功", 1);
            } else {
                return result.failed("账户不存在", 40603);
            }
        } catch (error) {
            return result.failed(error);
        }
    }

    /**
     * 获取用户基本信息
     * @param {*} ctx
     */
    async updateBuyProfit(session, { profit, symbol = 'BTC_USDT', app_id = '4USDT' }) {
        try {
            RedisDao.del(`4usdt:merchant_fee_info:${session.parent_id || session.id}-${symbol.replace('BTC_', '')}`)
            let queryData = {
                where: {
                    id: session.parent_id || session.id,
                    isMerchants: 1
                },
                raw: true,
                attributes: ["id"]
            };
            const userInfo = await user_infoDAO.findOne(queryData);
            const symbolInfo = await wlt_symbol_infoDAO.findOne({
                where: {
                    app_id,
                    symbol
                }
            })

            if (userInfo) {

                let merchantSymbolSetting = await user_merchant_symbol_settingDAO.findOne({
                    where: {
                        merchantId: session.parent_id || session.id,
                        symbol_id: symbolInfo.id
                    }
                })

                if (merchantSymbolSetting) {
                    await user_merchant_symbol_settingDAO.update({
                        merchantBuyProfit: profit
                    }, {
                        where: { id: merchantSymbolSetting.id },
                        fields: ['merchantBuyProfit']
                    });
                }
                else {
                    await user_merchant_symbol_settingDAO.create({
                        merchantBuyProfit: profit,
                        symbol_id: symbolInfo.id,
                        merchantId: session.parent_id || session.id
                    })
                }

                return result.success("商家费率修改成功", 1);
            } else {
                return result.failed("商家费率修改错误，没有找到该商家", 40804);
            }
        } catch (error) {
            return result.failed(error);
        }
    }

    /**
     * 获取用户基本信息
     * @param {*} ctx
     */
    async updateSellProfit(session, { profit, symbol = 'BTC_USDT', app_id = '4USDT' }) {
        try {
            RedisDao.del(`4usdt:merchant_fee_info:${session.parent_id || session.id}-${symbol.replace('BTC_', '')}`)

            let queryData = {
                where: {
                    id: session.parent_id || session.id,
                    isMerchants: 1
                },
                raw: true,
                attributes: ["id"]
            };
            const userInfo = await user_infoDAO.findOne(queryData);
            const symbolInfo = await wlt_symbol_infoDAO.findOne({
                where: {
                    app_id,
                    symbol
                }
            })

            if (userInfo) {
                let merchantSymbolSetting = await user_merchant_symbol_settingDAO.findOne({
                    where: {
                        merchantId: session.parent_id || session.id,
                        symbol_id: symbolInfo.id
                    }
                })

                if (merchantSymbolSetting) {
                    await user_merchant_symbol_settingDAO.update({
                        merchantSellProfit: profit
                    }, {
                        where: { id: merchantSymbolSetting.id },
                        fields: ['merchantSellProfit']
                    });
                }
                else {
                    await user_merchant_symbol_settingDAO.create({
                        merchantSellProfit: profit,
                        symbol_id: symbolInfo.id,
                        merchantId: session.parent_id || session.id
                    })
                }

                return result.success("商家费率修改成功", 1);
            } else {
                return result.failed("商家费率修改错误，没有找到该商家", 40804);
            }
        } catch (error) {
            return result.failed(error);
        }
    }

    /**
     * 获取用户基本信息
     * @param {*} ctx
     */
    async updateProfit(session, { buy_profit, sell_profit }) {
        try {
            let queryData = {
                where: {
                    id: session.parent_id || session.id
                },
                raw: true,
                attributes: ["id"]
            };
            const userInfo = await user_infoDAO.findOne(queryData);

            if (userInfo) {
                let saveresult = await user_infoDAO.update({
                    merchantBuyProfit: buy_profit,
                    merchantSellProfit: sell_profit,
                }, {
                    where: { id: session.parent_id || session.id },
                    fields: ['merchantSellProfit', 'merchantBuyProfit']
                });

                if (saveresult[0] == 1) {
                    return result.success("商家费率修改成功");
                }
                else {
                    return result.failed("费率无改动或该账户不存在", 503043);
                }
            } else {
                return result.failed("商家费率修改错误，没有找到该商家", 40804);
            }
        } catch (error) {
            return result.failed(error);
        }
    }

    async sendEmailRequest(session, data) {
        try {
            console.log(data)
            let queryData = {
                where: {
                    id: data.id,
                    // merchantStatus: 0
                },
                raw: true,
                attributes: ["id", "area_code", "name", "email", "mobile", "isTraders", "tradersStatus", "isMerchants", "merchantStatus"]
            };
            const userInfo = await user_infoDAO.findOne(queryData);

            let key = Utils.getMd5(userInfo.email)

            // console.log(Utils.getMd5("gba_asset@outlook.com"))

            let curActCache = await RedisDao.exists(`merchant_active:${key}_lock`)

            if (curActCache) {
                return result.failed("每小时同用户只能发送一次邀请", 40306);
            }

            //2019年7月23日 14:26:15 由每3天发一次，修改为每小时发一次
            await RedisDao.set(`merchant_active:${key}`, Crypto.encryptByDES(JSON.stringify(userInfo)), 60 * 60 * 24)
            await RedisDao.set(`merchant_active:${key}_lock`, '', 60 * 60)

            if (userInfo) {

                var person = {
                    name: '',
                    email: userInfo.email, // person.email can also accept an array of emails
                    subject: ``,
                    username: userInfo.name,
                    url: `${key}`
                }

                if (userInfo.isTraders && userInfo.tradersStatus == 0) {
                    person.name = 'Activation Trader Account - 4USDT'
                    person.subject = 'Activation Trader Account - 4USDT'
                }
                else if (userInfo.isMerchants && userInfo.merchantStatus == 0) {
                    person.name = 'Activation Merchant Account - 4USDT'
                    person.subject = 'Activation Merchant Account - 4USDT'
                }
                else {
                    return result.failed("该商户已经激活，无须重复发送邀请", 40305);
                }

                emailHelper('mail_merchant_request', person, function (error, result) {
                    console.log(` - - - - - - - - - - - - - - - - - - - - -> email sent: ${userInfo.email}`);
                    console.log(result);
                    console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
                    console.log(error)
                })

                return result.success("邀请成功");

                // let saveresult = await user_infoDAO.update({
                //     merchantBuyProfit: buy_profit,
                //     merchantSellProfit: sell_profit,
                // }, {
                //         where: { id: session.id },
                //         fields: ['merchantSellProfit', 'merchantBuyProfit']
                //     });

                // if (saveresult[0] == 1) {
                //     return result.success("商家费率修改成功");
                // }
                // else {
                //     return result.failed("费率无改动或该账户不存在", 503043);
                // }
            } else {
                return result.failed("该商户已经激活，或者没有找到该商家", 40304);
            }
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }

    }
}

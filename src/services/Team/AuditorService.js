import result from '../../tools/Result';
import sequelize from '../../lib/sequelize';
import RedisDao from '../../lib/RedisDao';
import Crypto from '../../tools/Secret';
import Sign from '../../tools/Sign';
import uuid from 'uuid';
import Utils from '../../tools/Utils';
var speakeasy = require("speakeasy");
var moment = require('moment');
var QRCode = require('qrcode');
var _ = require('lodash');

const { webDBUtil, Sequelize } = sequelize;


const ggCodeInfoDAO = webDBUtil.import('../../models/Security/gg_code_info');
const apiInfoDAO = webDBUtil.import('../../models/Security/merchant_api_info');

const user_infoDAO = webDBUtil.import('../../models/Users/user_info');

const user_roles_configDAO = webDBUtil.import('../../models/Users/user_roles_config');

const merchantWithdrawRecordDAO = webDBUtil.import('../../models/Wallet/wlt_withdraw_record_merchants');
const merchantWithdrawApprovalRecordDAO = webDBUtil.import('../../models/Wallet/wlt_withdraw_merchants_approval_record');

/**
 * 谷歌两步验证服务 GoogleService
 * 本页面处理业务逻辑 接收参数与返回处理结果
 */
module.exports = class AuditorService {

    /**
     * 创建Google两步验证
     * @param {*} user
     */
    async orderList(session, { page = 1, per_page = 10 }) {
        try {
            let max_per_page_size = 100
            let queryData = {
                where: {
                    is_delete: 0, user_id: session.parent_id || session.id,
                    strategy_limit_approval: { [Sequelize.Op.ne]: 0 },
                },
                order: [
                    ["id", 'DESC']
                ],
                raw: true,
                attributes: ["id", "user_id", "address", "request_id", "value", "fee", "tx", "created_time", "symbol", "status", "remark", "order_no", "strategy_limit_approval"]
            };

            //分页
            if (page && per_page) {
                queryData.offset = Number((page - 1) * per_page); //开始的数据索引
                queryData.limit = Number(per_page); //每页限制返回的数据条数
            };
            if (per_page > 100) {
                per_page = max_per_page_size
            }

            const { rows, count } = await merchantWithdrawRecordDAO.findAndCountAll(queryData);

            let approvalInfoList = await this.getApprovalInfoList(_.map(rows, 'id'), session.id)

            _.merge(rows, approvalInfoList)

            return result.pageData("SUCCESS", null, rows, count, page, per_page);
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    async getApprovalInfoList(ids, uid) {
        ids = ids.filter(item => {
            return !!item
        })

        if (ids.length > 0) {

            let approvalCacheList = await RedisDao.mget(ids.map(item => { return `4usdt:merchant_withdraw:approvalinfo:${item}-${uid}` }))

            for (var i = 0; i < ids.length; i++) {
                if (ids[i]) {
                    if (!approvalCacheList[i]) {
                        approvalCacheList[i] = await this.approvalInfo(ids[i], uid)
                    }
                    else {
                        approvalCacheList[i] = JSON.parse(approvalCacheList[i])
                    }
                }
            }

            return approvalCacheList
        }
        else {
            return []
        }
    }

    async approvalInfo(id, uid) {
        let resultInfo = {}

        let queryScript = "select id,auditor_user_id from wlt_withdraw_merchants_approval_record where order_no = ? and is_delete = 0 and result = 1;"
        let approvalInfoList = await webDBUtil.query(queryScript,
            { raw: true, replacements: [id], type: sequelize.QueryTypes.SELECT })

        if (approvalInfoList.length > 0) {
        }
        resultInfo.approval_count = approvalInfoList.length
        resultInfo.is_approval = approvalInfoList.filter(item => { return item.auditor_user_id == uid }).length

        await RedisDao.set(`4usdt:merchant_withdraw:approvalinfo:${id}-${uid}`, JSON.stringify(resultInfo), 60 * 6)
        return resultInfo
    }

    /**
     * 创建Google两步验证
     * @param {*} user
     */
    async orderApproval(session, data) {
        try {
            session.parent_id = session.parent_id || session.id
            let queryScript = "select count(*) as count from view_child_account_role where auditor = 1 and parent_id = ? and id =?;"
            let isAuditorResult = await webDBUtil.query(queryScript,
                { raw: true, replacements: [session.parent_id, session.id], type: sequelize.QueryTypes.SELECT })

            console.log(isAuditorResult)

            if (session.parent_id == session.id || (isAuditorResult.length > 0 && isAuditorResult[0].count > 0)) {
                // return result.success(0, child_account_role_cfg_list[0].count)
                let withdrawRecord = await merchantWithdrawRecordDAO.findOne({
                    where: {
                        user_id: session.parent_id,
                        id: data.id,
                        is_delete: 0,
                        strategy_limit_approval: { [Sequelize.Op.ne]: 0 },
                        enable: 0
                    }
                })

                if (withdrawRecord) {
                    //是否已被拒绝
                    let reject_count = await merchantWithdrawApprovalRecordDAO.count({
                        where: {
                            user_id: session.parent_id,
                            auditor_user_id: session.id,
                            order_no: data.id,
                            result: 0,
                            is_delete: 0
                        }
                    })

                    if (reject_count > 0) {
                        return result.failed("该提现已被拒绝", 506683);
                    }


                    let curApprovalInfo = {
                        user_id: session.parent_id,
                        auditor_user_id: session.id,
                        order_no: data.id,
                        result: 1,
                        remark: data.remark || ""
                    }

                    curApprovalInfo.sign = await Sign.getRSASign(JSON.stringify(curApprovalInfo), 'approval_withdraw')
                    RedisDao.del(`4usdt:merchant_withdraw:approvalinfo:${data.id}-${session.id}`)
                    await merchantWithdrawApprovalRecordDAO.create(curApprovalInfo)

                    let approval_count = await merchantWithdrawApprovalRecordDAO.count({
                        where: {
                            user_id: session.parent_id,
                            auditor_user_id: session.id,
                            order_no: data.id,
                            is_delete: 0
                        }
                    })

                    if (approval_count >= withdrawRecord.strategy_limit_approval) {
                        await merchantWithdrawRecordDAO.update({ enable: 1 }, {
                            where: {
                                user_id: session.parent_id,
                                id: data.id,
                                is_delete: 0,
                                strategy_limit_approval: { [Sequelize.Op.ne]: 0 },
                                enable: 0
                            },
                            fields: ['enable']
                        });

                        return result.success("success with withdraw")
                    }
                    else {
                        return result.success("success")
                    }
                }
                else {
                    return result.failed(`没有找到该订单，或该订单已处理`, 506684);
                }
            }
            else {
                return result.failed(`当前用户无此权限`, 506685);
            }
        } catch (error) {
            console.log(error);
            return result.failed("子账号操作失败", 4882024);
        }
    }

    /**
     * 创建Google两步验证
     * @param {*} user
     */
    async orderReject(session, data) {

        try {
            session.parent_id = session.parent_id || session.id
            let queryScript = "select count(*) as count from view_child_account_role where auditor = 1 and parent_id = ? and id =?;"
            let isAuditorResult = await webDBUtil.query(queryScript,
                { raw: true, replacements: [session.parent_id, session.id], type: sequelize.QueryTypes.SELECT })

            console.log(isAuditorResult)

            if (session.parent_id == session.id || (isAuditorResult.length > 0 && isAuditorResult[0].count > 0)) {
                // return result.success(0, child_account_role_cfg_list[0].count)
                let withdrawRecord = await merchantWithdrawRecordDAO.findOne({
                    where: {
                        user_id: session.parent_id,
                        id: data.id,
                        is_delete: 0,
                        strategy_limit_approval: { [Sequelize.Op.ne]: 0 },
                        enable: 0
                    }
                })

                if (withdrawRecord) {
                    //是否已被拒绝
                    let reject_count = await merchantWithdrawApprovalRecordDAO.count({
                        where: {
                            user_id: session.parent_id,
                            auditor_user_id: session.id,
                            order_no: data.id,
                            result: 0,
                            is_delete: 0
                        }
                    })

                    if (reject_count > 0) {
                        return result.failed("该提现已被拒绝", 506683);
                    }

                    let curApprovalInfo = {
                        user_id: session.parent_id,
                        auditor_user_id: session.id,
                        order_no: data.id,
                        result: 0,
                        remark: data.remark || ""
                    }

                    curApprovalInfo.sign = await Sign.getRSASign(JSON.stringify(curApprovalInfo), 'approval_withdraw')
                    RedisDao.del(`4usdt:merchant_withdraw:approvalinfo:${data.id}-${session.id}`)
                    await merchantWithdrawApprovalRecordDAO.create(curApprovalInfo)

                    await merchantWithdrawRecordDAO.update({ status: -1 }, {
                        where: {
                            user_id: session.parent_id,
                            id: data.id,
                            is_delete: 0,
                            strategy_limit_approval: { [Sequelize.Op.ne]: 0 },
                            enable: 0
                        },
                        fields: ['status']
                    });

                    return result.success("success")
                }
                else {
                    return result.failed(`没有找到该订单，或该订单已处理`, 506684);
                }
            }
            else {
                return result.failed(`当前用户无此权限`, 506685);
            }
        } catch (error) {
            console.log(error);
            return result.failed("子账号操作失败", 4882024);
        }
    }
}

import result from '../../tools/Result';
import sequelize from '../../lib/sequelize';
import RedisDao from '../../lib/RedisDao';
import Crypto from '../../tools/Secret';
import AccountService from '../../services/Account/AccountService';
import Sign from '../../tools/Sign';
import uuid from 'uuid';
import Utils from '../../tools/Utils';
var speakeasy = require("speakeasy");
var moment = require('moment');
var QRCode = require('qrcode');
var _ = require('lodash');

const { webDBUtil, Sequelize } = sequelize;


const ggCodeInfoDAO = webDBUtil.import('../../models/Security/gg_code_info');
const apiInfoDAO = webDBUtil.import('../../models/Security/merchant_api_info');

const user_infoDAO = webDBUtil.import('../../models/Users/user_info');

const user_roles_configDAO = webDBUtil.import('../../models/Users/user_roles_config');

const accountService = new AccountService();

/**
 * 谷歌两步验证服务 GoogleService
 * 本页面处理业务逻辑 接收参数与返回处理结果
 */
module.exports = class ChildAccountService {

    /**
     * 创建Google两步验证
     * @param {*} user
     */
    async list(session, { page = 1, per_page = 10 }) {
        try {
            let max_per_page_size = 100

            let queryData = {
                where: {
                    isDelete: 0, parent_id: session.parent_id || session.id
                },
                raw: true,
                order: [
                    ["id", 'DESC']
                ],
                attributes: ["id",
                    "mobile",
                    "email",
                    "avatar",
                    "name",
                    "area",
                    "area_code",
                    "source",
                    "created_time",
                    "updated_time",
                    "enable",
                    "isDelete",
                    "version",
                    "authGoogleCode",
                    "authPhoneCode",
                    "authMailCode",
                    "authPicCode",
                    "identity_no",
                    "parent_id"]
            };

            // user_roles_configDAO.findAll()



            //分页
            if (page && per_page) {
                queryData.offset = Number((page - 1) * per_page); //开始的数据索引
                queryData.limit = Number(per_page); //每页限制返回的数据条数
            };
            if (per_page > 100) {
                per_page = max_per_page_size
            }

            const { rows, count } = await user_infoDAO.findAndCountAll(queryData);

            let userRoleList = await this.getUserRoleList(_.map(rows, 'id'))

            _.merge(rows, userRoleList)

            console.log(rows)
            console.log(userRoleList)

            return result.pageData("SUCCESS", null, rows, count, page, per_page);
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }


    async getUserRoleList(ids) {
        if (ids && ids.length > 0) {
            ids = ids.filter(item => {
                return !!item
            })

            let amountCacheList = await RedisDao.mget(ids.map(item => { return `4usdt:account:user_role_cfg:${item}` }))

            for (var i = 0; i < ids.length; i++) {
                if (ids[i]) {
                    if (!amountCacheList[i]) {
                        amountCacheList[i] = await user_roles_configDAO.findOne({
                            attributes: [
                                ["administrator", "role_administrator"],
                                ["auditor", "role_auditor"],
                                ["operator", "role_operator"],
                                ["observer", "role_observer"],
                                ["trader", "role_trader"],
                                ["remark", "role_remark"]
                            ],
                            where: {
                                user_id: ids[i],
                                isDelete: false
                            }
                        })

                        amountCacheList[i] = JSON.parse(JSON.stringify(amountCacheList[i]))

                        // console.log(JSON.stringify(amountCacheList[i]))

                        await RedisDao.set(`4usdt:account:user_role_cfg:${ids[i]}`, JSON.stringify(amountCacheList[i]), 6)

                    }
                    else {
                        amountCacheList[i] = JSON.parse(amountCacheList[i])
                    }

                }
            }

            return amountCacheList
        }
        else {
            return []
        }
    }

    async approvalCount(session) {
        try {

            // user_infoDAO.findAll()
            let queryScript = "select count(*) as count from view_child_account_role where administrator = 1 and parent_id = ?;"
            let child_account_role_cfg_list = await webDBUtil.query(queryScript, { raw: true, replacements: [session.id], type: sequelize.QueryTypes.SELECT })

            console.log(child_account_role_cfg_list)
            if (child_account_role_cfg_list.length > 0) {
                return result.success(0, child_account_role_cfg_list[0].count)
            }
            else {
                return result.success(0, 0)
            }

        } catch (error) {
            console.log(error)
            return result.failed(`查询异常-${error.name}`, 5030742);
        }
    }

    /**
     * 创建Google两步验证
     * @param {*} user
     */
    async create(session, data, config) {
        try {
            // let queryData = {
            //     where: { id: id },
            //     raw: true
            // };

            let userInfo = {
                "mobile": data.mobile,
                "avatar": data.avatar,
                "licenseNumber": 0,
                "password": '',
                "email": data.email,
                "area_code": data.area_code,
                "name": data.name,
                "merchantMarginValue": 0,
                "authGoogleCode": 0,
                "authPhoneCode": 1,
                "authMailCode": 1,
                "authFundsPassword": 0,
                "merchantId": 0,
                "merchantName": data.merchantName || data.name || "",
                "merchantStatus": 0,
                "isMerchants": 1,
                "identity_no": data.identity_no || "",
                "parent_id": session.parent_id || session.id
            }

            // if (data.margin_value == 0) {
            //     merchantInfo.tradersStatus = 3
            // }


            // console.log(userInfo)
            userInfo = await user_infoDAO.create(userInfo);

            await accountService.sendEmailRequest({}, userInfo)

            const existsCount = await user_roles_configDAO.count({
                where: { isDelete: 0, user_id: userInfo.dataValues.id }
            })


            let configInfo = {
                user_id: userInfo.dataValues.id,
                administrator: config.administrator,
                auditor: config.auditor,
                operator: config.operator,
                observer: config.observer,
                trader: config.trader,
                remark: config.remark
            }

            configInfo.sign = await Sign.getRSASign(JSON.stringify(configInfo), 'user_role_config')

            if (existsCount == 0) {
                await user_roles_configDAO.create(configInfo)

                return result.success('创建成功', { id: userInfo.id });
            }
            else {
                await user_roles_configDAO.update(configInfo, {
                    where: { user_id: userInfo.dataValues.id, isDelete: 0 },
                    fields: ['administrator', "auditor", 'operator', "observer", "trader", "remark"]
                });

                return result.success('创建成功', { id: userInfo.id });
            }

        } catch (error) {
            console.log(error)
            return result.failed(`创建失败，可能该账户已经存在-${error.name}`, 5030485);
        }
    }

    async updateConfig(session, data, config) {
        try {
            const userInfo = await user_infoDAO.findOne({
                where: {
                    parent_id: session.id,
                    id: data.user_id,
                    enable: 1,
                    isDelete: 0
                }
            })

            if (userInfo) {
                let configInfo = {
                    administrator: config.administrator,
                    auditor: config.auditor,
                    operator: config.operator,
                    observer: config.observer,
                    trader: config.trader,
                    remark: config.remark
                }

                await user_roles_configDAO.update(configInfo, {
                    where: { user_id: data.user_id, isDelete: 0 },
                    fields: ['administrator', "auditor", 'operator', "observer", "trader", "remark"]
                });

                return result.success('修改成功');
            }
            else {
                return result.failed(`用户信息错误或未找到`, 5066485);
            }

        } catch (error) {
            console.log(error)
            return result.failed(`修改失败-${error.name}`, 5036485);
        }
    }

    /**
     * 创建Google两步验证
     * @param {*} user
     */
    async destroy(session, data) {
        try {
            const userInfo = await user_infoDAO.findOne({
                where: {
                    parent_id: session.id,
                    id: data.user_id,
                    enable: 1,
                    isDelete: 0
                }
            })

            if (userInfo) {

                let existsCount = await user_infoDAO.count({
                    where: {
                        isDelete: 0, id: data.user_id
                    }
                })

                console.log(`destroy existsCount - ${existsCount}`)
                if (existsCount == 1) {
                    await user_infoDAO.update({
                        isDelete: true,
                        enable: 0
                    }, {
                        where: { id: data.user_id, isDelete: 0 },
                        fields: ['isDelete', 'enable']
                    })

                    return result.success('子账号删除完成');
                }
                else {
                    return result.failed("没有找到子账号信息", 4880236);
                }
            }
            else {
                return result.failed(`用户信息错误或未找到`, 5066485);
            }
        } catch (error) {
            console.log(error);
            return result.failed("子账号操作失败", 4882024);
        }
    }

    /**
     * 创建Google两步验证
     * @param {*} user
     */
    async freeze(session, data) {
        try {
            const userInfo = await user_infoDAO.findOne({
                where: {
                    parent_id: session.id,
                    id: data.user_id,
                    enable: 1,
                    isDelete: 0
                }
            })

            if (userInfo) {
                let existsCount = await user_infoDAO.count({
                    where: {
                        isDelete: 0, id: data.user_id, enable: 1
                    }
                })

                console.log(`destroy existsCount - ${existsCount}`)
                if (existsCount == 1) {
                    await user_infoDAO.update({
                        enable: false
                    }, {
                        where: { id: data.user_id, isDelete: 0, enable: 1 },
                        fields: ['enable']
                    })

                    return result.success('账号已冻结');
                }
                else {
                    return result.failed("没有找到账号信息", 4880265);
                }
            }
            else {
                return result.failed(`用户信息错误或未找到`, 5066485);
            }
        } catch (error) {
            console.log(error);
            return result.failed("账号操作失败", 4880245);
        }
    }

    async unFreeze(session, data) {
        try {
            const userInfo = await user_infoDAO.findOne({
                where: {
                    parent_id: session.id,
                    id: data.user_id,
                    enable: 0,
                    isDelete: 0
                }
            })

            if (userInfo) {
                // console.log(queryData)
                const userInfo = await user_infoDAO.update({
                    enable: 1
                }, {
                    where: { id: data.user_id },
                    fields: ['enable']
                });
                if (userInfo[0] == 1) {
                    return result.success("解冻成功");
                }
                else {
                    return result.failed("账户已禁用或该账户不存在", 5030465);
                }
            }
            else {
                return result.failed(`用户信息错误或未找到`, 5066485);
            }
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }

    async reset_google(session, { user_id }) {
        try {
            const userInfo = await user_infoDAO.findOne({
                where: {
                    parent_id: session.id,
                    id: data.user_id,
                    enable: 0,
                    isDelete: 0
                }
            })

            if (userInfo) {
                //查询google验证码信息
                const ggCodeInfo = await ggCodeInfoDAO.findOne({
                    where: { user_id: user_id, enable: 1, is_delete: 0, status: 1 },
                    raw: true
                });

                if (ggCodeInfo) {
                    await ggCodeInfoDAO.update({
                        status: -2,
                        is_delete: 1
                    }, {
                        where: { id: ggCodeInfo.id },
                        fields: ['status', 'is_delete']
                    });

                    await user_infoDAO.update({
                        authGoogleCode: false
                    }, {
                        where: { id: user_id, parent_id: session.id, isDelete: 0, enable: 1 },
                        fields: ['authGoogleCode']
                    })
                    return result.success("Google两步验证解绑成功");
                } else {
                    return result.failed('未绑定', 400501);
                }
            }
            else {
                return result.failed(`用户信息错误或未找到`, 5066485);
            }
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    async reset_password(session, { id }) {
        try {
            // console.log(queryData)
            const userInfo = await user_infoDAO.update({
                password: ""
            }, {
                where: { id: id, authGoogleCode: 1, isDelete: 0, enable: 1 },
                fields: ['password']
            });
            if (userInfo[0] == 1) {
                return result.success("重置密码成功");
            }
            else {
                return result.failed("账户已禁用或该账户不存在", 5030465);
            }
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }
}

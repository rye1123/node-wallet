import result from '../../tools/Result';
import sequelize from '../../lib/sequelize';
import middleware from '../../middleware';
import RedisDao from '../../lib/RedisDao';
import Sign from '../../tools/Sign';
var moment = require('moment');

const { webDBUtil } = sequelize;

const access_requestDAO = webDBUtil.import('../../models/PlatInfo/access_request');

/**
 * 用户额度服务 AccessService
 * 本页面处理业务逻辑 接收参数与返回处理结果
 */
module.exports = class AccessService {

    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async access_record(session, { page = 1, per_page = 10, order = 0 }, app_id = "BITMALL") {
        try {
            let sortby = 'id'
            let queryData = {
                raw: true,
                order: [
                    [sortby, (order == 0 ? 'DESC' : 'ASC')]
                ]
            };

            //分页
            if (page && per_page) {
                queryData.offset = Number((page - 1) * per_page); //开始的数据索引
                queryData.limit = Number(per_page); //每页限制返回的数据条数
            };
            if (per_page > 100) {
                per_page = 100
            }

            // console.log(queryData)
            const { rows, count } = await access_requestDAO.findAndCountAll(queryData);
            return result.pageData(null, null, rows, count, page, per_page);
        } catch (error) {
            return result.failed(error);
        }
    }

    async access_record_base(session, { page = 1, per_page = 10, order = 0 }, type, attributes, app_id = "BITMALL") {
        try {
            let sortby = 'id'
            let queryData = {
                raw: true,
                order: [
                    [sortby, (order == 0 ? 'DESC' : 'ASC')]
                ]
            };
            if (type) {
                queryData.where = { type }
            }

            if (attributes) {
                queryData.attributes = attributes
            }

            //分页
            if (page && per_page) {
                queryData.offset = Number((page - 1) * per_page); //开始的数据索引
                queryData.limit = Number(per_page); //每页限制返回的数据条数
            };
            if (per_page > 100) {
                per_page = 100
            }

            // console.log(queryData)
            const { rows, count } = await access_requestDAO.findAndCountAll(queryData);
            return result.pageData(null, null, rows, count, page, per_page);
        } catch (error) {
            return result.failed(error);
        }
    }

    async access_record_merchants(session, { page = 1, per_page = 10, order = 0 }, app_id = "BITMALL") {
        return this.access_record_base(session, { page, per_page, order }, "MERCHANTS",
            ["id", "productName", "chat_type", "chat_account", "companyName", "productName", "email", "country", "name", "phone", "status", "remark", "id_card_no", "address", "website", "business", "ownerFrontFile", "ownerBackFile", "vedioFile", "createdTime"], app_id)
    }

    async access_record_traders(session, { page = 1, per_page = 10, order = 0 }, app_id = "BITMALL") {
        return this.access_record_base(session, { page, per_page, order }, "TRADERS",
            ["id", "email", "country", "name", "phone", "status", "remark", "id_card_no", "idFrontFile", "idBackFile", "vedioFile", "createdTime", "ownerFrontFile", "ownerBackFile", "vedioFile"], app_id)
    }

    async regInfo(userInfo) {
        let accessInfo = {
            type: userInfo.type || '无',
            business: userInfo.business || '无',
            productName: userInfo.productName || '',
            companyName: userInfo.company || '',
            email: userInfo.email || '',
            country: userInfo.country || '',
            name: userInfo.name || '',
            phone: userInfo.phone || '',
            companyRegFile: userInfo.companyFilepath || '',
            idFrontFile: userInfo.userIdFrontFilepath || '',
            idBackFile: userInfo.userIdBackFilepath || '',
            ownerFrontFile: userInfo.userFrontFilepath || '',
            ownerBackFile: userInfo.userBackFilepath || '',
            vedioFile: userInfo.vedioFile || '',
            app_id: userInfo.app_id || 'BITMALL',
            website: userInfo.web || '',
            address: userInfo.address || '',
            id_card_no: userInfo.identity || '',
            companyScale: userInfo.scale || '',
            remark: userInfo.remark || '',
            chat_type: userInfo.chat_type || '',
            chat_account: userInfo.chat_account || ''
        }

        const res = await access_requestDAO.create(accessInfo)

        return res
    }
}

import result from '../../tools/Result';
import sequelize from '../../lib/sequelize';
import middleware from '../../middleware';
import AmountService from '../Account/AmountService';
import RedisDao from '../../lib/RedisDao';
import Utils from '../../tools/Utils';
import Sign from '../../tools/Sign';
import Crypto from '../../tools/Secret';
import UUID from 'uuid';
import GoogleService from '../../services/Security/GoogleService'
var moment = require('moment');
var _ = require('lodash');
var sendemail = require('sendemail')
var emailHelper = sendemail.email;


const { webDBUtil, Sequelize } = sequelize;
const {
    ValidateTools
} = middleware.ValidateTools;


const ggService = new GoogleService();
const merchantDAO = webDBUtil.import('../../models/Users/user_info');
const apiInfoDAO = webDBUtil.import('../../models/Security/merchant_api_info');
const accessInfoDAO = webDBUtil.import('../../models/PlatInfo/access_request');
const merchantKycDAO = webDBUtil.import('../../models/Users/user_merchant_kyc_setting');

const amountService = new AmountService();
/**
 * 用户额度服务 AmountService
 * 本页面处理业务逻辑 接收参数与返回处理结果
 */
module.exports = class MerchantService {

    /**
     * 获取商户子账号列表
     * @param {*} ctx
     */
    async merchantChildAccountList(session, { user_id, page = 1, per_page = 10, order = 0, keyword, is_delete }) {
        try {
            if (!user_id) {
                return result.failed('父账户不可为空');
            }
            let sortby = 'id'
            let queryData = {
                where: { isMerchants: 1, parent_id: user_id },
                raw: true,
                order: [
                    [sortby, (order == 0 ? 'DESC' : 'ASC')]
                ],
                attributes: ["id", "area_code", "mobile", "avatar", "email", "name", "authGoogleCode", "authPhoneCode", "authMailCode",
                    "authFundsPassword", "merchantId", "merchantName", "merchantStatus", 'merchantSellProfit', 'merchantBuyProfit',
                    "merchantPlatformBuyProfit", "merchantPlatformSellProfit", "merchantMarginValue", "merchant_max_cancel_order_day", "merchant_max_order_concurrent",
                    "created_time", "isDelete"
                ]
            };

            if (keyword) {
                queryData.where[Sequelize.Op.or] = {
                    mobile: { [Sequelize.Op.like]: keyword },
                    id: { [Sequelize.Op.like]: keyword },
                    email: { [Sequelize.Op.like]: keyword },
                    name: { [Sequelize.Op.like]: keyword }
                }
            }

            if (is_delete) {
                queryData.where.isDelete = is_delete
            }

            //分页
            if (page && per_page) {
                queryData.offset = Number((page - 1) * per_page); //开始的数据索引
                queryData.limit = Number(per_page); //每页限制返回的数据条数
            };
            if (per_page > 100) {
                per_page = 100
            }

            const { rows, count } = await merchantDAO.findAndCountAll(queryData);

            if (count > 0) {
                let amountList = await amountService.getAmountList(_.map(rows, 'id'))
                _.merge(rows, amountList)
            }

            return result.pageData(null, null, rows, count, page, per_page);
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }

    /**
     * 商户列表
     * @param {*} ctx
     */
    async merchantList(session, { page = 1, per_page = 10, order = 0, keyword, is_delete }) {
        try {
            let sortby = 'id'
            let queryData = {
                where: { isMerchants: 1, parent_id: 0 },
                raw: true,
                order: [
                    [sortby, (order == 0 ? 'DESC' : 'ASC')]
                ],
                attributes: ["id", "area_code", "mobile", "avatar", "email", "name", "authGoogleCode", "authPhoneCode", "authMailCode",
                    "authFundsPassword", "merchantId", "merchantName", "merchantStatus", 'merchantSellProfit', 'merchantBuyProfit',
                    "merchantPlatformBuyProfit", "merchantPlatformSellProfit", "merchantMarginValue", "merchant_max_cancel_order_day", "merchant_max_order_concurrent",
                    "created_time", "isDelete"
                ]
            };

            if (keyword) {
                queryData.where[Sequelize.Op.or] = {
                    mobile: { [Sequelize.Op.like]: `%${keyword}%` },
                    id: { [Sequelize.Op.like]: `%${keyword}%` },
                    email: { [Sequelize.Op.like]: `%${keyword}%` },
                    name: { [Sequelize.Op.like]: `%${keyword}%` }
                }
            }

            if (is_delete) {
                queryData.where.isDelete = is_delete
            }

            //分页
            if (page && per_page) {
                queryData.offset = Number((page - 1) * per_page); //开始的数据索引
                queryData.limit = Number(per_page); //每页限制返回的数据条数
            };
            if (per_page > 100) {
                per_page = 100
            }

            const { rows, count } = await merchantDAO.findAndCountAll(queryData);
            if (count > 0) {
                let amountList = await amountService.getAmountList(_.map(rows, 'id'))
                _.merge(rows, amountList)
            }

            return result.pageData(null, null, rows, count, page, per_page);
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }


    async merchantsBalance(session, { ids }) {
        try {
            let idsArr = ids.split(",")

            let amountList = await amountService.getAmountList(idsArr)

            return result.success("success", amountList)
        }
        catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }

    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async merchantDetail(session, { id }) {
        try {
            let queryData = {
                where: { id: id, isDelete: 0, isMerchants: 1 },
                raw: true,
                attributes: ["id", "area_code", "mobile", "avatar", "email", "name", "authGoogleCode", "authPhoneCode", "authMailCode",
                    "authFundsPassword", "merchantId", "merchantName", "merchantStatus", 'merchantSellProfit', 'merchantBuyProfit',
                    "merchantPlatformBuyProfit", "merchantPlatformSellProfit", "merchantMarginValue", "merchant_max_cancel_order_day", "merchant_max_order_concurrent",
                    "access_id"
                ]
            };

            const userInfo = await merchantDAO.findOne(queryData);

            if (!userInfo) {
                return result.failed('没有找到用户', 500402);
            }

            if (userInfo.access_id > 0) {
                let accessInfo = await accessInfoDAO.findOne({
                    where: {
                        id: userInfo.access_id
                    },
                    attributes: ["companyRegFile", "idFrontFile", "idBackFile", "ownerFrontFile", "ownerBackFile", "vedioFile",
                        "chat_type", "chat_account"]
                })

                if (accessInfo) {
                    _.merge(userInfo, JSON.parse(JSON.stringify(accessInfo)))
                }
            }

            let amountInfo = await amountService.getAmountList([userInfo.id])

            _.merge(userInfo, amountInfo[0])
            return result.success(1, userInfo);
        } catch (error) {
            return result.failed(error);
        }
    }

    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async merchantAdd(session, data) {
        try {
            let merchantInfo = {
                "mobile": `${data.mobile}`.trim(),
                "area_code": `${data.area_code}`.trim() || "+86",
                "avatar": data.avatar,
                "licenseNumber": 0,
                "password": '',
                "email": `${data.email}`.trim(),
                "name": data.name,
                "merchantMarginValue": data.margin_value,
                "authGoogleCode": 0,
                "authPhoneCode": 1,
                "authMailCode": 1,
                "authFundsPassword": 0,
                "merchantId": 0,
                "merchantName": data.merchantName,
                "merchantStatus": 0,
                "isMerchants": 1,
                "identity_no": data.identity_no || "",
                "access_id": data.access_id || 0
            }

            const userInfo = await merchantDAO.create(merchantInfo);
            return result.success('创建成功', { id: userInfo.id });
        } catch (error) {
            console.log(error.name)
            return result.failed(`创建失败，可能该账户已经存在-${error.name}`, 503048);
        }
    }

    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async merchantEdit(session, data) {
        try {
            let merchantInfo = {
                "avatar": data.avatar,
                "mobile": data.mobile,
                "area_code": data.area_code || "+86",
                "licenseNumber": 0,
                "email": data.email,
                "name": data.name,
                "merchantName": data.merchantName,
                "merchantBuyProfit": data.buy_profit,
                "merchantSellProfit": data.sell_profit,
                "merchantPlatformBuyProfit": data.platform_buy_profit,
                "merchantPlatformSellProfit": data.platform_sell_profit,
                "merchant_max_cancel_order_day": data.merchant_max_cancel_order_day || 3,
                "merchant_max_order_concurrent": data.merchant_max_order_concurrent || 1,
                "identity_no": data.identity_no || "",
                "access_id": data.access_id || 0
            }

            // console.log(queryData)
            const userInfo = await merchantDAO.update(merchantInfo, {
                where: { id: data.id, isDelete: 0 },
                fields: ['avatar', "licenseNumber", 'mobile', 'area_code', "password", "email", "name", "merchantName",
                    "merchantBuyProfit", "merchantSellProfit", "merchantPlatformBuyProfit", "merchantPlatformSellProfit", "merchant_max_cancel_order_day", "merchant_max_order_concurrent"]
            });
            if (userInfo[0] == 1) {
                return result.success("修改成功");
            }
            else {
                return result.failed("没有修改或该账户不存在", 503049);
            }
        } catch (error) {
            console.log(error)
            return result.failed(`修改失敗 - ${error}`);
        }
    }

    /**
     * 解除Google验证码绑定
     * @param {*} ctx
     */
    async merchantUnbindGoogleCode(session, { id }) {
        try {
            // let userInfo = await merchantDAO.update({
            //     authGoogleCode: 0
            // }, {
            //     where: { id: id, isDelete: 0 },
            //     fields: ['authGoogleCode']
            // });

            userInfo = await merchantDAO.findOne({
                where: { id: id }
            })

            // console.log(await ggService.unbind({ id: id }))

            if (userInfo) {
                let key = `${Utils.getMd5(userInfo.email)}_${UUID.v4().replace(/-/g, '')}`
                await RedisDao.set(`merchant_unbind_google_code:${key}`, Crypto.encryptByDES(JSON.stringify(userInfo)), 60 * 60 * 24)
                await RedisDao.set(`merchant_unbind_google_code:${key}_lock`, '', 60 * 60)

                var person = {
                    name: 'Cancel Google Authenticator - Bitmall',
                    email: userInfo.email, // person.email can also accept an array of emails
                    subject: `Cancel Google Authenticator - Bitmall`,
                    username: userInfo.name,
                    url: `${key}`
                }

                emailHelper('unbind_google_code', person, function (error, result) {
                    console.log(` - - - - - - - - - - - - - - - - - - - - -> Cancel Google Authenticator email sent: ${userInfo.email}`);
                    console.log(result);
                    console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
                    console.log(error)
                })

                return result.success("谷歌验证码解除邮件已发送");
            }
            else {
                return result.failed("账户已禁用或该账户不存在", 503046);
            }
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }

    /**
     * 重置商户密码
     * @param {*} ctx
     */
    async merchantResetPassword(session, { id }) {
        try {
            let userInfo = await merchantDAO.update({
                password: ''
            }, {
                where: { id: id, isDelete: 0 },
                fields: ['password']
            });

            userInfo = await merchantDAO.findOne({
                where: { id: id }
            })

            if (userInfo) {
                let key = `${Utils.getMd5(userInfo.email)}_${UUID.v4().replace(/-/g, '')}`
                await RedisDao.set(`merchant_active:${key}`, Crypto.encryptByDES(JSON.stringify(userInfo)), 60 * 60 * 24)
                await RedisDao.set(`merchant_active:${key}_lock`, '', 60 * 60)

                var person = {
                    name: 'Reset Account Password - 4USDT',
                    email: userInfo.email, // person.email can also accept an array of emails
                    subject: `Reset Account Password - 4USDT`,
                    username: userInfo.name,
                    url: `${key}`
                }

                emailHelper('mail_merchant_request', person, function (error, result) {
                    console.log(` - - - - - - - - - - - - - - - - - - - - -> Reset Account Password email sent: ${userInfo.email}`);
                    console.log(result);
                    console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
                    console.log(error)
                })

                return result.success("重置邮件已发送");
            }
            else {
                return result.failed("账户已禁用或该账户不存在", 503046);
            }
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }

    /**
     * 恢复商户账号
     * @param {*} ctx
     */
    async merchantEnable(session, { id }) {
        try {
            let curUserId = id
            const userInfo = await merchantDAO.update({
                isDelete: 0
            }, {
                where: { id: curUserId, isDelete: 1 },
                fields: ['isDelete']
            });
            if (userInfo[0] == 1) {

                await apiInfoDAO.update({
                    enable: true
                }, {
                    where: { userid: curUserId, is_delete: 0 },
                    fields: ['enable']
                })

                return result.success("账号恢复成功");
            }
            else {
                return result.failed("账户不存在或未被注销", 503146);
            }
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }

    /**
     * 禁用商户账号
     * @param {*} ctx
     */
    async merchantDisable(session, { id }) {
        try {
            let curUserId = id
            // console.log(queryData)
            const userInfo = await merchantDAO.update({
                isDelete: 1
            }, {
                where: { id: curUserId, isDelete: 0 },
                fields: ['isDelete']
            });
            if (userInfo[0] == 1) {
                let queryData = {
                    where: {
                        user_id: curUserId,
                        is_delete: 0,
                        enable: 1
                    },
                    raw: true,
                    attributes: ["api_key"]
                };

                const apiList = await apiInfoDAO.findAll(queryData);
                apiList.map(async item => {
                    await RedisDao.del(`4usdt:apiinfo:api:${item.api_key}`)
                })

                await apiInfoDAO.update({
                    enable: false
                }, {
                    where: { user_id: curUserId, is_delete: 0 },
                    fields: ['enable']
                })

                return result.success("禁用成功");
            }
            else {
                return result.failed("账户已禁用或该账户不存在", 503046);
            }
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }

    //获取实名验证配置信息
    async getMerchantKycSetting(session, { id }) {
        var cacheKey = `4usdt:merchant_kyc_info:${id}`
        let filter = {
            where: {
                user_id: id,
                is_delete: 0
            },
            attributes:
                [
                    'id_check',
                    'id_check_with_phone',
                    'vedio_id_check',
                    'vedio_id_check_min_amount',
                    'vedio_id_check_max_amount',
                    'vedio_id_check_count_cycle',
                    'vedio_id_check_retry_count'
                ]
        }
        let kycInfo = await merchantKycDAO.findOne(filter)

        if (!kycInfo) {
            await merchantKycDAO.create({
                user_id: id
            })
            kycInfo = await merchantKycDAO.findOne(filter)
        }

        await RedisDao.set(cacheKey, JSON.stringify(kycInfo), 100)

        return result.success("SUCCESS", kycInfo)
    }

    //设置实名验证配置信息
    async setMerchantKycSetting(session, data) {
        var cacheKey = `4usdt:merchant_kyc_info:${data.id}`

        let setting = {}
        let settingAttrs = []

        if (data.id_check || data.id_check == 0) {
            setting.id_check = data.id_check
            settingAttrs.push("id_check")
        }
        if (data.id_check_with_phone || data.id_check_with_phone == 0) {
            setting.id_check_with_phone = data.id_check_with_phone
            settingAttrs.push("id_check_with_phone")
        }
        if (data.vedio_id_check || data.vedio_id_check == 0) {
            setting.vedio_id_check = data.vedio_id_check
            settingAttrs.push("vedio_id_check")
        }
        if (data.vedio_id_check_min_amount || data.vedio_id_check_min_amount == 0) {
            setting.vedio_id_check_min_amount = data.vedio_id_check_min_amount
            settingAttrs.push("vedio_id_check_min_amount")
        }
        if (data.vedio_id_check_max_amount || data.vedio_id_check_max_amount == 0) {
            setting.vedio_id_check_max_amount = data.vedio_id_check_max_amount
            settingAttrs.push("vedio_id_check_max_amount")
        }
        if (data.vedio_id_check_count_cycle || data.vedio_id_check_count_cycle == 0) {
            setting.vedio_id_check_count_cycle = data.vedio_id_check_count_cycle
            settingAttrs.push("vedio_id_check_count_cycle")
        }
        if (data.vedio_id_check_retry_count || data.vedio_id_check_retry_count == 0) {
            setting.vedio_id_check_retry_count = data.vedio_id_check_retry_count
            settingAttrs.push("vedio_id_check_retry_count")
            if (setting.vedio_id_check_retry_count < 1) {
                setting.vedio_id_check_retry_count = 1
            }
            if (setting.vedio_id_check_retry_count > 5) {
                setting.vedio_id_check_retry_count = 5
            }
        }

        await merchantKycDAO.update(setting, {
            where: { user_id: data.id, is_delete: 0 },
            fields: settingAttrs
        });

        await RedisDao.del(cacheKey)
        return result.success("SUCCESS")
    }
}

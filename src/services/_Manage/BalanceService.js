import result from '../../tools/Result';
import sequelize from '../../lib/sequelize';
import middleware from '../../middleware';
import RedisDao from '../../lib/RedisDao';
import Sign from '../../tools/Sign';
var moment = require('moment');

const { webDBUtil } = sequelize;

const coboCoinBalanceDAO = webDBUtil.import('../../models/Wallet/wlt_cobo_symbol_balance');

/**
 * 用户额度服务 AccessService
 * 本页面处理业务逻辑 接收参数与返回处理结果
 */
module.exports = class BalanceService {

    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async cobo_balanceInfo(session) {
        try {
            const data = await coboCoinBalanceDAO.findAll({});
            return result.success("success", data);
        } catch (error) {
            return result.failed(error);
        }
    }
}

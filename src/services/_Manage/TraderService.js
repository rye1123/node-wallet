import result from '../../tools/Result';
import sequelize from '../../lib/sequelize';
import middleware from '../../middleware';
import AmountService from '../Account/AmountService';
import Utils from '../../tools/Utils';
import Sign from '../../tools/Sign';
var _ = require('lodash');

var moment = require('moment');


const { webDBUtil, Sequelize } = sequelize;
const {
    ValidateTools
} = middleware.ValidateTools;


const merchantDAO = webDBUtil.import('../../models/Users/user_info');
const accessInfoDAO = webDBUtil.import('../../models/PlatInfo/access_request');

const amountService = new AmountService();

/**
 * 用户额度服务 AmountService
 * 本页面处理业务逻辑 接收参数与返回处理结果
 */
module.exports = class MerchantService {



    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async traderList(session, { page = 1, per_page = 10, order = 0 }) {
        try {
            let sortby = 'id'
            let queryData = {
                where: { isTraders: 1, isDelete: 0 },
                raw: true,
                order: [
                    [sortby, (order == 0 ? 'DESC' : 'ASC')]
                ],
                attributes: ["id", "area_code", "mobile", "avatar", "email", "name", "authGoogleCode", "authPhoneCode", "authMailCode",
                    "authFundsPassword", "tradersNo", 'tradersName', 'tradersStatus', "tradersMarginValue", "created_time", "isDelete"
                ]
            };

            //分页
            if (page && per_page) {
                queryData.offset = Number((page - 1) * per_page); //开始的数据索引
                queryData.limit = Number(per_page); //每页限制返回的数据条数
            };
            if (per_page > 100) {
                per_page = 100
            }

            // console.log(queryData)
            const { rows, count } = await merchantDAO.findAndCountAll(queryData);


            let amountList = await amountService.getAmountList(_.map(rows, 'id'))

            _.merge(rows, amountList)

            return result.pageData(null, null, rows, count, page, per_page);
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }

    async traderBalance(session, { ids }) {
        try {
            let idsArr = ids.split(",")

            let amountList = await amountService.getAmountList(idsArr)

            return result.success("success", amountList)
        }
        catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }

    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async traderDetail(session, { id }) {
        try {
            let queryData = {
                where: { isTraders: 1, id: id, isDelete: 0 },
                raw: true,
                attributes: ["id", "area_code", "mobile", "avatar", "email", "name", "authGoogleCode", "authPhoneCode", "authMailCode",
                    "authFundsPassword", "tradersNo", 'tradersName', 'tradersStatus', "tradersMarginValue", "created_time", "isDelete",
                    "access_id"
                ]
            };

            const userInfo = await merchantDAO.findOne(queryData);

            let amountInfo = await amountService.getAmountList([userInfo.id])

            if (userInfo.access_id > 0) {
                let accessInfo = await accessInfoDAO.findOne({
                    where: {
                        id: userInfo.access_id
                    },
                    attributes: ["companyRegFile", "idFrontFile", "idBackFile", "ownerFrontFile", "ownerBackFile", "vedioFile",
                        "chat_type", "chat_account"]
                })

                if (accessInfo) {
                    _.merge(userInfo, JSON.parse(JSON.stringify(accessInfo)))
                }
            }

            _.merge(userInfo, amountInfo[0])
            return result.success(1, userInfo);
        } catch (error) {
            return result.failed(error);
        }
    }

    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async traderAdd(session, data) {
        try {
            // let queryData = {
            //     where: { id: id },
            //     raw: true
            // };

            let merchantInfo = {
                "mobile": `${data.mobile}`.trim(),
                "area_code": `${data.area_code}`.trim() || "+86",
                "avatar": data.avatar,
                "licenseNumber": 0,
                "password": Utils.getMd5(data.password),
                "email": `${data.email}`.trim(),
                "name": data.name,
                "tradersMarginValue": data.margin_value,
                "authGoogleCode": 0,
                "authPhoneCode": 1,
                "authMailCode": 1,
                "authFundsPassword": 0,
                "tradersNo": 0,
                "tradersName": data.name,
                "tradersStatus": 0,
                "isTraders": 1,
                "identity_no": data.identity_no,
                "access_id": data.access_id || 0
            }

            // if (data.margin_value == 0) {
            //     merchantInfo.tradersStatus = 3
            // }

            console.log(merchantInfo)
            const userInfo = await merchantDAO.create(merchantInfo);
            return result.success('创建成功', { id: userInfo.id });
        } catch (error) {
            console.log(error)
            return result.failed(`创建失败，可能该账户已经存在-${error.name}`, 503048);
        }
    }

    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async traderEdit(session, data) {
        try {
            // let queryData = {
            //     where: { id: id },
            //     raw: true
            // };
            let saveFileds = ['avatar', 'mobile', 'area_code', "email", "name"]
            let merchantInfo = {
                "avatar": data.avatar,
                "email": data.email,
                "mobile": data.mobile,
                "area_code": data.area_code || "+86",
                "name": data.name,
                "access_id": data.access_id || 0
            }

            if (data.margin_value) {
                merchantInfo.tradersMarginValue = data.margin_value
                saveFileds.push("tradersMarginValue")
            }

            if (data.access_id) {
                merchantInfo.access_id = data.access_id
                saveFileds.push("access_id")
            }

            console.log(merchantInfo)

            const userInfo = await merchantDAO.update(merchantInfo, {
                where: { id: data.id },
                fields: saveFileds
            });
            if (userInfo[0] == 1) {
                return result.success("修改成功");
            }
            else {
                return result.failed("没有修改或该账户不存在", 503049);
            }
        } catch (error) {
            return result.failed(error);
        }
    }

    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async traderDisable(session, { id }) {
        try {
            // console.log(queryData)
            const userInfo = await merchantDAO.update({
                isDelete: 1
            }, {
                where: { id: id },
                fields: ['isDelete']
            });
            if (userInfo[0] == 1) {
                return result.success("禁用成功");
            }
            else {
                return result.failed("账户已禁用或该账户不存在", 503046);
            }
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }
}

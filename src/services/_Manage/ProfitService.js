import result from '../../tools/Result';
import sequelize from '../../lib/sequelize';
import middleware from '../../middleware';
import RedisDao from '../../lib/RedisDao';
import Sign from '../../tools/Sign';
var moment = require('moment');

const { webDBUtil } = sequelize;

const access_requestDAO = webDBUtil.import('../../models/PlatInfo/access_request');

/**
 * 用户额度服务 ProfitService
 * 本页面处理业务逻辑 接收参数与返回处理结果
 */
module.exports = class ProfitService {

    async platform_profit(session, { startTime, endTime }) {
        startTime = moment(startTime, "YYYY-MM-DD")
        endTime = moment(endTime, "YYYY-MM-DD")
        let queryScript = "select sum(value) as profit from funds_list where userid  = 1 and created_time > ? and created_time < ? and source = ?;"
        let dealers_profit = await webDBUtil.query(queryScript, { raw: true, replacements: [startTime, endTime, 'DEALERS_FEE'], type: sequelize.QueryTypes.SELECT })
        if (dealers_profit) {
            dealers_profit = dealers_profit[0].profit
        }
        let merchants_profit = await webDBUtil.query(queryScript, { raw: true, replacements: [startTime, endTime, 'MERCHANTS_FEE'], type: sequelize.QueryTypes.SELECT })
        if (merchants_profit) {
            merchants_profit = merchants_profit[0].profit
        }
    }

    async withdraw_fee_profit_day(session, { startTime, endTime }) {
        let resdata = await this.funds_profit_day({ startTime, endTime, source: "WITHDRAW_FEE_IN" })
        return result.success("SUCCESS", resdata)
    }

    async traders_profit_day(session, { startTime, endTime }) {
        let resdata = await this.funds_profit_day({ startTime, endTime, source: "MERCHANTS_FEE" })
        return result.success("SUCCESS", resdata)
    }

    async merchants_profit_day(session, { startTime, endTime }) {
        let resdata = await this.funds_profit_day({ startTime, endTime, source: "MERCHANTS_FEE" })
        return result.success("SUCCESS", resdata)
    }

    async funds_profit_day({ startTime, endTime, source }) {
        try {
            console.log(startTime)
            console.log(endTime)
            // startTime = moment(startTime, "YYYY-MM-DD")
            // endTime = moment(endTime, "YYYY-MM-DD")

            let queryScript = `select DATE_FORMAT(created_time, '%Y-%m-%e') as time, sum(value) as profit from funds_list where userid  = 1 and created_time >= ? and created_time <= ? and source = ? GROUP BY DATE_FORMAT(created_time, '%Y%m%e');`
            let funds_profit = await webDBUtil.query(queryScript, { raw: true, replacements: [startTime, endTime, source], type: sequelize.QueryTypes.SELECT })
            return funds_profit
        }
        catch (err) {
            console.log(err)
        }
    }

}

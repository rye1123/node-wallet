import result from '../../tools/Result';
import CommonService from '../Common/CommonService';
import sequelize from '../../lib/sequelize';
import middleware from '../../middleware';
import Utils from '../../tools/Utils';
import AmountService from '../Account/AmountService';
import RedisDao from '../../lib/RedisDao';
import Sign from '../../tools/Sign';

var moment = require('moment');

const {webDBUtil, Sequelize} = sequelize;
const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();
const amountService = new AmountService();

const commonService = new CommonService();

const withdrawDAO = webDBUtil.import('../../models/Wallet/wlt_withdraw_record');

const userInfoDAO = webDBUtil.import('../../models/Users/user_info')

/**
 * 用户额度服务 AmountService
 * 本页面处理业务逻辑 接收参数与返回处理结果
 */
module.exports = class WalletService {

    async platform_balance_info(session, {symbol}) {
        var cacheKey = `4usdt:platform_balance_info:${symbol}`
        let balanceInfo = {}

        if (!symbol) {
            symbol = "USDT"
        }

        if (await RedisDao.exists(cacheKey)) {
            balanceInfo = JSON.parse(await RedisDao.get(cacheKey))
        } else {
            let script = `select 'sum_amount' as type, sum(value) as amount from funds_list where symbol = '${symbol}' UNION
            select 'traders_amount' as type, sum(value) as amount from funds_list where symbol = '${symbol}' and userid in (select id from user_info where isTraders = 1) UNION
            select 'merchants_amount' as type,sum(value) as amount from funds_list where symbol = '${symbol}' and userid in (select id from user_info where isMerchants = 1) UNION
            select 'platform_profit' as type,sum(value) as amount from funds_list where symbol = '${symbol}' and userid = 1 UNION
            select 'hedging_amount' as type,sum(value) as amount from funds_list where symbol = '${symbol}' and userid = 100; `

            balanceInfo = await webDBUtil.query(script, {
                raw: true,
                replacements: [],
                type: sequelize.QueryTypes.SELECT
            })

            await RedisDao.set(cacheKey, JSON.stringify(balanceInfo), 60 * 6)
        }
        return result.success('SUCCESS', balanceInfo)
    }

    async cobo_take_fee_info(session) {
        var cacheKey = `4usdt:platform_cobo_take_fee_info`
        let balanceInfo = {}

        if (await RedisDao.exists(cacheKey)) {
            balanceInfo = JSON.parse(await RedisDao.get(cacheKey))
        } else {
            //矿工费语句
            // let coboTake_miner_fee_script = "SELECT sum(tx_fee),tx_fee_coin from `wlt_cobo_send_record` group by tx_fee_coin"

            //cobo手续费
            // let coboTake_cobo_fee_script = "SELECT sum(cobo_fee),coin from `wlt_cobo_send_record` group by coin"

            let coboTake_fee_script = `SELECT sum(tx_fee) as fee ,tx_fee_coin from wlt_cobo_send_record group by tx_fee_coin union SELECT sum(cobo_fee) as fee,coin from wlt_cobo_send_record group by coin`

            balanceInfo = await webDBUtil.query(coboTake_fee_script, {
                raw: true,
                replacements: [],
                type: sequelize.QueryTypes.SELECT
            })

            await RedisDao.set(cacheKey, JSON.stringify(balanceInfo), 60 * 1)
        }
        return result.success('SUCCESS', balanceInfo)

    }

    /**
     * 获取用户提现记录
     * @param {*} ctx
     */
    async withdrawList(session, {page = 1, per_page = 10, order = 0, user_id}) {
        try {
            let sortby = 'id'
            let queryData = {
                where: {is_delete: 0},
                raw: true,
                order: [
                    [sortby, (order == 0 ? 'DESC' : 'ASC')]
                ]
            };

            //分页
            if (page && per_page) {
                queryData.offset = Number((page - 1) * per_page); //开始的数据索引
                queryData.limit = Number(per_page); //每页限制返回的数据条数
            }
            ;
            if (per_page > 100) {
                per_page = 100
            }

            if (user_id) {
                queryData.where.userid = user_id
            }

            // console.log(queryData)
            const {rows, count} = await withdrawDAO.findAndCountAll(queryData);
            return result.pageData(null, null, rows, count, page, per_page);
        } catch (error) {
            return result.failed(error);
        }
    }

    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async approval(session, {id}) {
        try {
            const withdrawInfo = await withdrawDAO.findOne({where: {id: id, status: 0}})

            let curUserInfo = await userInfoDAO.findOne({
                attributes: ["isTraders", "is_ispring_address"],
                where: {id: withdrawInfo.userid}
            })

            let status = 1;
            if (curUserInfo.is_ispring_address == 1) {
                status = 11;
            }

            console.log('approval userId=' + withdrawInfo.userid + ' id=' + id + ' is_ispring_address=' + curUserInfo.is_ispring_address + ' status=' + status);

            // console.log(queryData)
            const userInfo = await withdrawDAO.update({
                status: status
            }, {
                where: {id: id, status: 0},
                fields: ['status']
            });
            let postData = {
                //推送消息
                "alert": "您申请的提币已审核通过",
                //推送目标
                "aliasArr": [withdrawInfo.userid],
                //场景码
                "code": 4003,
                //推送标题
                "title": "提现结果",
                "data": {id: id}
            }


            if (curUserInfo.isTraders) {
                console.log(`交易员推送提现通知`)
                console.log(postData)
                commonService.app_pushMsg(postData)
            }

            if (userInfo[0] == 1) {
                return result.success("审批提现成功");
            } else {
                return result.failed("该记录已经审批完成或该记录不存在", 503056);
            }
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }

    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async reject(session, {id, reason}) {
        try {
            // console.log(queryData)
            console.log(`${id} - 拒绝了提现申请 原因${reason}`)
            const userInfo = await withdrawDAO.update({
                remark: reason,
                status: -1
            }, {
                where: {id: id, status: 0},
                fields: ['remark', 'status']
            });
            if (userInfo[0] == 1) {

                let withdrawInfo = await withdrawDAO.findOne({
                    where: {id: id, status: -1},
                    raw: true
                })

                // console.log(withdrawInfo)
                if (withdrawInfo.no_id) {
                    if (await amountService.rollbackWithdraw(withdrawInfo)) {
                        console.log(`提现拒绝资金 - ${id} - 回退完成`)
                    } else {
                        console.log(`提现拒绝资金 - ${id} - 回退失败`)
                    }
                }

                return result.success("拒绝提现成功");
            } else {
                return result.failed("该记录已经审批完成或该记录不存在", 503056);
            }
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }
}

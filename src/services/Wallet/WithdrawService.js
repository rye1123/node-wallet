import result from '../../tools/Result';
import sequelize from '../../lib/sequelize';
import middleware from '../../middleware';
import Utils from '../../tools/Utils';

const { webDBUtil, Sequelize } = sequelize;
const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();
const walletWithdrawDAO = webDBUtil.import('../../models/Wallet/wlt_withdraw_record');


module.exports = class WithdrawService {

    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async list(session, { page = 1, per_page = 10, order = 0, symbol, userid = 0 }) {
        try {
            let max_per_page_size = 100
            let sortby = 'id'
            let queryData = {
                where: { userid: session.parent_id || session.id },
                raw: true,
                order: [
                    [sortby, (order == 0 ? 'DESC' : 'ASC')]
                ]
            };

            if (queryData.where.userid == 0 || queryData.where.userid == undefined) {
                queryData.where.userid = { [Sequelize.Op.ne]: 1 }
            }

            if (symbol) {
                queryData.where.symbol = symbol
                if (queryData.where.symbol.indexOf('USDT') > -1) {
                    queryData.where.symbol = { [Sequelize.Op.like]: "%USDT%" }
                }
            }

            //分页
            if (page && per_page) {
                queryData.offset = Number((page - 1) * per_page); //开始的数据索引
                queryData.limit = Number(per_page); //每页限制返回的数据条数
            };
            if (per_page > 100) {
                per_page = max_per_page_size
            }

            console.log(queryData)
            const { rows, count } = await walletWithdrawDAO.findAndCountAll(queryData);
            return result.pageData(null, null, rows, count, page, per_page);
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }


}

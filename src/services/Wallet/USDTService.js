import result from '../../tools/Result';
import sequelize from '../../lib/sequelize';
import Sign from '../../tools/Sign';
import middleware from '../../middleware';
import CryptoJS from 'crypto-js'
import {cobo_getNewAddres} from "../../tools/coboWallet-client"
import request from '../../tools/request';

const {webDBUtil, Sequelize} = sequelize;
var QRCode = require('qrcode');

var OmniClient = require('../../lib/OmniClient.js').OmniClient;
var OmniBC = require('../../lib/OmniClient.js').Omni;
const user_infoDAO = webDBUtil.import('../../models/Users/user_info');
var moment = require('moment');

var client = new OmniClient({
    host: '47.52.220.250',
    port: 8888,
    user: 'admin',
    pass: "123456",
    timeout: 300000
});

const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

const walletDAO = webDBUtil.import('../../models/Wallet/wlt_address_list');

OmniBC.init("admin", "123456", '47.52.220.250', false)

/**
 * 用户信息服务 AccountService
 * 本页面处理业务逻辑 接收参数与返回处理结果
 */
module.exports = class USDTService {

    /**
     * 获取用户信息
     * @param {*} ctx
     */
    async getNewAddress(session, {symbol}) {
        try {
            let userId = session.parent_id || session.id;

            let where = {userid: userId, symbol: 'USDT'};
            if (symbol != 'BTC_USDT') {
                where.symbol = symbol;
            }
            console.log(where);

            await walletDAO.update({
                enable: false
            }, {
                where: where,
                fields: ['enable']
            });

            let curUserInfo = await user_infoDAO.findOne({
                where: {
                    id: userId
                },
                raw: true
            })
            let isSpringAddress = 0;
            if (curUserInfo) {
                isSpringAddress = curUserInfo.is_ispring_address;
            } else {
                return result.failed("can't find userId");
            }
            console.log("getNewAddress userId=" + userId + " isSpringAddress=" + isSpringAddress);


            // omni服务 | cobo服务
            // let address = await OmniBC.getnewaddress('bitmall')
            let address;
            if (isSpringAddress == 0) {
                console.log('--------call getCoboAddress------------ userid=' + userId);
                let addressResp = await cobo_getNewAddres(symbol);
                if (addressResp.success == true) {
                    address = addressResp.result.address;
                    console.log('get cobo address success userId=' + userId + ' val=' + address);
                } else {
                    console.log('get cobo address error userId=' + userId);
                    return result.failed("获取cobo地址失败", 50128);
                }
            } else {
                console.log('--------call getIspringAddress------------userid=' + userId);
                let ispringResp = await this.getIspringAddress(symbol);
                if (ispringResp.success) {
                    address = ispringResp.data;
                    console.log('get ispring address success userId' + userId + ' val=' + address);
                } else {
                    console.log('get ispring address error userId' + userId);
                    return result.failed("获取ispring地址失败", 50128);
                }
            }

            // console.log(address.success)
            // console.log(address.result)
            // console.log(address.result.address)


            let record = {
                userid: session.parent_id || session.id,
                address: address,
                symbol: symbol,
                ispring_status:isSpringAddress
            }

            record.sign = await Sign.getRSASign(JSON.stringify(record), 'wallet_address')
            await walletDAO.create(record)
            delete record.sign
            delete record.userid

            record.qrcode = await QRCode.toDataURL(address)
            record.qrcode = record.qrcode.replace("data:image/png;base64,", "")
            // addressInfo.qrcode = addressInfo.qrcode.replace("data:image/png;base64,", "")
            return result.success(null, record);
        } catch (error) {
            return result.failed(error);
        }
    }

    async getTradersMarginAddress(session, {symbol}) {
        if (!symbol) {
            symbol = "BTC_USDT"
        }
        if (symbol == "USDT") {
            symbol = "BTC_USDT"
        }

        let where = {userid: session.id, enable: 1, symbol: symbol}
        if (symbol != 'BTC_USDT') {
            where.symbol = symbol
        }
        try {
            //查询google验证码信息
            const addressInfo = await walletDAO.findOne({
                where: where,
                raw: true
            });

            if (addressInfo) {
                delete addressInfo.id
                delete addressInfo.userid
                delete addressInfo.created_time
                delete addressInfo.enable
                delete addressInfo.sign
                delete addressInfo.is_delete
                addressInfo.qrcode = await QRCode.toDataURL(addressInfo.address)
                addressInfo.qrcode = addressInfo.qrcode.replace("data:image/png;base64,", "")

                let curUserInfo = await user_infoDAO.findOne({
                    attributes: ['tradersMarginValue'],
                    where: {
                        id: session.id
                    },
                    raw: true
                })
                addressInfo.marginValue = curUserInfo.tradersMarginValue;
                return result.success(null, addressInfo);
            } else {
                return await this.getNewAddress(session, {symbol})
            }
        } catch (error) {
            console.log(error)
            return result.failed("获取地址失败", 566681);
        }
    }

    async getAddress(session, {symbol}) {
        try {
            if (!symbol || symbol == "USDT") {
                symbol = "BTC_USDT"
            }

            let where = {userid: session.parent_id || session.id, symbol: symbol, enable: 1}

            //查询google验证码信息
            const addressInfo = await walletDAO.findOne({
                where: where,
                raw: true
            });

            if (addressInfo) {
                delete addressInfo.id
                delete addressInfo.userid
                delete addressInfo.created_time
                delete addressInfo.enable
                delete addressInfo.sign
                delete addressInfo.is_delete
                addressInfo.qrcode = await QRCode.toDataURL(addressInfo.address)
                addressInfo.qrcode = addressInfo.qrcode.replace("data:image/png;base64,", "")
                return result.success(null, addressInfo);
            } else {
                return await this.getNewAddress(session, {symbol})
            }
        } catch (error) {
            console.log(error)
            return result.failed("获取地址失败", 566680);
        }
    }

    async getIspringAddress(symbol) {
        const env = process.env.SYS_ENV || 'development';

        let host = '';
        if (env == 'development') {
            host = 'http://47.110.139.56:8090';
        } else {
            host = 'http://47.244.217.215:8090';
        }
        console.log('getIspringAddress---------env=' + env + ' host=' + host);

        let {response, body} = await request.post(host + '/ispring/getAddress', {'symbol': symbol});
        console.log('call create ispring address success symbol=' + symbol + ' body=' + JSON.stringify(body));
        let result = {
            success: true,
            data: null,
            msg: ''
        }
        if (body.code == '0') {
            result.data = body.data;
            console.log('create ispring success ');
        } else {
            result.success = false;
            result.msg = 'call create ispring error';
            console.log('create ispring error body=' + JSON.stringify(body));
        }
        return result;
    }
}

import result from '../../tools/Result';
import sequelize from '../../lib/sequelize';
import Sign from '../../tools/Sign';
import middleware from '../../middleware';
import Utils from '../../tools/Utils';
import CryptoJS from 'crypto-js'
import uuid from "uuid"
import { add, sub, fool } from "../../lib/operation"
import { queueConfig } from "../../lib/queueUtil"
import RedisDao from '../../lib/RedisDao';
// import { RedisConfig } from '../config';
const Queue = require('bee-queue');
var QRCode = require('qrcode');

var moment = require('moment');

const { webDBUtil, Sequelize } = sequelize;
const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

const walletWithdrawDAO = webDBUtil.import('../../models/Wallet/wlt_withdraw_record');
const walletDepositDAO = webDBUtil.import('../../models/Wallet/wlt_deposit_record');
const funds_listDAO = webDBUtil.import('../../models/Amount/funds_list');
const apiInfoDAO = webDBUtil.import('../../models/Security/merchant_api_info');
const withdrawMerchantRecordDAO = webDBUtil.import('../../models/Wallet/wlt_withdraw_record_merchants');
const withdrawRecordDAO = webDBUtil.import('../../models/Wallet/wlt_withdraw_record');

const walletAddressDAO = webDBUtil.import('../../models/Wallet/wlt_address_list');
const symbolInfoDAO = webDBUtil.import('../../models/Wallet/wlt_symbol_info');


// const userMerchantSettingDAO = webDBUtil.import('../../models/Users/user_merchant_setting');
// let merchantCfg = await userMerchantSettingDAO.findOne({
//     where: {
//         user_id: apiInfo.user_id,
//         isDelete: 0
//     }
// })
import accountAmountService from '../Account/AmountService'
const aAService = new accountAmountService()

/**
 * 用户信息服务 AccountService
 * 本页面处理业务逻辑 接收参数与返回处理结果
 */
module.exports = class AmountService {

    async address_check(data) {


        const existsAddressInfo = await walletAddressDAO.findOne({
            where: {
                address: data.address
            },
            attributes: ["symbol"]
        })

        return result.success("", existsAddressInfo)
    }

    async api_withdraw_info({ api_key, request_id }) {
        try {

            //获取API信息
            let apiInfo = await apiInfoDAO.findOne({
                where: {
                    api_key: api_key,
                    enable: 1,
                    readonly: 1,
                    is_delete: 0
                }
            })

            if (apiInfo) {
                let withdrawRecord = await withdrawMerchantRecordDAO.findOne({
                    attributes: ["request_id", "address", "value", "tx", "symbol", "created_time", "order_no"],
                    where: {
                        request_id: request_id,
                        user_id: apiInfo.user_id,
                        is_delete: 0,
                        enable: 1
                    }, row: true
                })
                if (withdrawRecord) {
                    withdrawRecord = withdrawRecord.dataValues
                    let withdrawinfo = await withdrawRecordDAO.findOne({
                        where: {
                            no_id: withdrawRecord.order_no,
                            is_delete: 0
                        }
                    })

                    if (withdrawRecord.symbol == "BTC_USDT") {
                        withdrawRecord.fee = 5
                        withdrawRecord.chain = "OMNI"
                        withdrawRecord.symbol = "USDT"
                    }
                    else if (withdrawRecord.symbol == "ETH_USDT") {
                        withdrawRecord.fee = 3
                        withdrawRecord.chain = "ERC20"
                        withdrawRecord.symbol = "USDT"
                    }

                    if (withdrawinfo) {
                        withdrawRecord.tx = withdrawinfo.tx
                        withdrawRecord.status = "SUCCESS"
                    }
                    else {
                        withdrawRecord.status = "CREATED"
                    }

                    withdrawRecord.value = parseFloat(withdrawRecord.value)
                    return result.success("SUCCESS", withdrawRecord)
                }
                else {
                    return result.failed("未找到", 4007911)
                }
                // withdrawRecord

            } else {
                return result.failed("Api错误", 4003811)
            }
        }
        catch (err) {
            console.log(err)
            return result.failed("查询异常", 4001811)
        }
    }

    async api_merchants_withdraw(data, request_ip) {
        try {
            data.request_ip = request_ip
            let josResult = await this.createMerchantWithdrawJob(data)

            let queueResult = await RedisDao.get(`withdraw_merchants_trans:lock_result_${josResult}`)
            console.log(queueResult)

            if (queueResult) {
                return JSON.parse(queueResult)
            }
            else {
                return result.failed(`提币失败-${queueResult}`, 40056431);
            }
        }
        catch (error) {
            console.log(error)
            return result.failed("error")
        }

    }

    async createMerchantWithdrawJob(data) {
        return new Promise((resolve, reject) => {
            try {
                var queue = new Queue(`withdraw_merchants`, queueConfig)

                var job = queue.createJob(data).timeout(2000).retries(3)

                job.on('succeeded', function (result) {
                    console.log('商户提现事务 succeeded ' + job.id);
                    resolve(job.id);
                });

                job.save(function (err, job) {
                    if (err) {
                        console.log('商户提现事务发生错误 ' + err);
                        reject(err)
                    }
                    console.log('商户提现事务 save ' + job.id);
                });

            } catch (error) {
                reject(error)
            }
        });
    }

    /**
     * 获取用户信息
     * @param {*} ctx
     */
    async withdraw(session, data) {
        try {

            console.log(`申请提币`)
            console.log(data);

            if (!Utils.verifyAddress(data.symbol, data.address)) {
                return result.failed(`提币地址错误`, 50078);
            }

            let balanceSymbol = data.symbol
            if (balanceSymbol == "BTC_USDT" || balanceSymbol == "ETH_USDT") {
                balanceSymbol = "USDT"
            }

            let amountInfo = await aAService.amountBase(session.id, balanceSymbol)
            let decimalLength = 8

            console.log(data)
            let fee = 5

            //以太坊节点的USDT，只有小数点后6位
            if (data.symbol == "ETH_USDT") {
                data.value = Math.floor(data.value * 1000000) / 1000000;
                fee = 3
                decimalLength = 6
            }

            if (data.symbol == "USDT") {
                data.symbol = "BTC_USDT"
            }

            let symbolInfo = await symbolInfoDAO.findOne({
                where: { enable: 1, is_delete: 0, symbol: data.symbol, app_id: "4USDT" },
                attributes: ["name", "symbol", "display_name", "withdrawFee", "withdrawMin", "address_role"]
            })
            fee = symbolInfo.withdrawFee

            console.log('withdraw amountInfo=' + JSON.stringify(amountInfo) + ' symbolInfo=' + JSON.stringify(amountInfo));
            console.log(amountInfo.available >= data.value);
            console.log(data.value + ' ' + symbolInfo.withdrawMin);
            console.log(data.value >= symbolInfo.withdrawMin);

            if (amountInfo.available >= data.value && data.value >= symbolInfo.withdrawMin) {
                console.log('withdraw －－－－－－－－－－－－');
                const existsAddressInfo = await walletAddressDAO.findOne({
                    where: {
                        address: data.address,
                        symbol: data.symbol
                    }
                })

                if (existsAddressInfo) {
                    console.log('内部地址-' + JSON.stringify(existsAddressInfo));
                    return await this.internal_withdraw(session, amountInfo, data, existsAddressInfo)
                }

                //这个格式不要变，不然会比较难验证签名
                //2019年7月13号，增加了logBalance日志
                //2019年7月24日，增加了no_id
                let record = {
                    no_id: uuid.v4().replace(/-/g, ''),
                    userid: parseInt(session.id),
                    address: data.address,
                    symbol: data.symbol,
                    value: `${fool(sub(data.value, fee), decimalLength)}`,
                    enable: 1,
                    status: 0
                    // ,logBalance: `${sub(amountInfo.sum_amount, data.value)}`
                }
                console.log(record)

                record.sign = await Sign.getRSASign(JSON.stringify(record), 'withdraw')
                record.logBalance = `${fool(sub(data.value, fee), decimalLength)}`
                await walletWithdrawDAO.create(record)


                //2019年8月1日 19:19:55  增加了log_amount
                let foudInfo = {
                    source: "WITHDRAW",
                    symbol: balanceSymbol,
                    value: "-" + Math.abs(fool(sub(data.value, fee), decimalLength)),
                    createUserId: session.id,
                    orderNo: record.no_id,
                    userid: session.id,
                    log_amount: `${fool(sub(amountInfo.sum_amount, sub(data.value, fee)), decimalLength)}`
                }
                amountInfo.sum_amount = sub(amountInfo.sum_amount, sub(data.value, fee))

                //2019年8月1日 19:19:55  增加了log_amount
                let foudInfo_fee_out = {
                    source: "WITHDRAW_FEE_OUT",
                    symbol: balanceSymbol,
                    value: -fee,
                    createUserId: session.id,
                    orderNo: record.no_id,
                    userid: session.id,
                    log_amount: `${fool(sub(amountInfo.sum_amount, fee), decimalLength)}`
                }
                amountInfo.sum_amount = sub(amountInfo.sum_amount, fee)

                let systemAmountInfo = await aAService.amountBase(1, data.symbol)
                //2019年8月1日 19:19:55  增加了log_amount
                let foudInfo_fee_in = {
                    source: "WITHDRAW_FEE_IN",
                    symbol: balanceSymbol,
                    value: fee,
                    createUserId: session.id,
                    orderNo: record.no_id,
                    userid: 1,
                    log_amount: `${add(systemAmountInfo.sum_amount, fee)}`
                }
                amountInfo.sum_amount = add(systemAmountInfo.sum_amount, fee)

                foudInfo.sign = await Sign.getRSASign(JSON.stringify(foudInfo), 'withdraw')
                foudInfo_fee_out.sign = await Sign.getRSASign(JSON.stringify(foudInfo_fee_out), 'withdraw')
                foudInfo_fee_in.sign = await Sign.getRSASign(JSON.stringify(foudInfo_fee_in), 'withdraw')

                await funds_listDAO.bulkCreate([foudInfo, foudInfo_fee_out, foudInfo_fee_in]);

                return result.success(null);
            } else {
                return result.failed(`余额不足，当前可用余额：${amountInfo.available}`, 50082);
            }

        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }

    //内部提币
    async internal_withdraw(session, amountInfo, data, existsAddressInfo) {

        if (data.symbol == "BTC_USDT" || data.symbol == "ETH_USDT") {
            data.symbol = "USDT"
        }

        //这个格式不要变，不然会比较难验证签名
        //2019年7月13号，增加了logBalance日志
        //2019年7月24日，增加了no_id
        //2019年8月1日 19:19:55  增加了log_amount
        let record = {
            no_id: uuid.v4().replace(/-/g, ''),
            userid: session.id,
            address: data.address,
            symbol: data.symbol,
            value: `${data.value}`,
            enable: 1,
            status: 3,
            logBalance: `${amountInfo.available - data.value}`
        }
        record.no_id = Utils.getMd5(JSON.stringify(record))
        console.log(record)

        record.sign = await Sign.getRSASign(JSON.stringify(record), 'withdraw')
        //2019-08-26 16:13:24 增加新字段is_internal，标识该记录是否为内部提现
        record.is_internal = 1
        await walletWithdrawDAO.create(record)

        let depositInfo = {
            userid: existsAddressInfo.userid,
            value: `${data.value}`,
            symbol: data.symbol,
            tx: record.no_id,
            is_delete: 0,
            status: 1
        }

        depositInfo.sign = await Sign.getRSASign(JSON.stringify(depositInfo), 'deposit')

        await walletDepositDAO.create(depositInfo);

        //2019年8月1日 19:19:55  增加了log_amount
        let foudInfo_out = {
            source: "WITHDRAW",
            symbol: data.symbol,
            value: `-${Math.abs(data.value)}`,
            createUserId: session.id,
            orderNo: record.no_id,
            userid: session.id,
            log_amount: `${sub(amountInfo.sum_amount, data.value)}`
        }
        let depositUserAmountInfo = await aAService.amountBase(existsAddressInfo.userid, data.symbol)

        //2019年8月1日 19:19:55  增加了log_amount
        let foudInfo_in = {
            source: "DEPOSIT",
            symbol: data.symbol,
            value: `${Math.abs(data.value)}`,
            createUserId: 1,
            orderNo: record.no_id,
            userid: existsAddressInfo.userid,
            log_amount: `${add(depositUserAmountInfo.sum_amount, data.value)}`
        }
        foudInfo_out.sign = await Sign.getRSASign(JSON.stringify(foudInfo_out), 'withdraw')
        foudInfo_in.sign = await Sign.getRSASign(JSON.stringify(foudInfo_in), 'deposit')

        await funds_listDAO.bulkCreate([foudInfo_out, foudInfo_in]);
        return result.success(null);
    }

}

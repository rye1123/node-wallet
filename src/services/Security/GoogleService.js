import result from '../../tools/Result';
import sequelize from '../../lib/sequelize';
import middleware from '../../middleware';
import Crypto from '../../tools/Secret';
var speakeasy = require("speakeasy");
var moment = require('moment');
var QRCode = require('qrcode');

const { webDBUtil, Sequelize } = sequelize;
const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

const ggCodeInfoDAO = webDBUtil.import('../../models/Security/gg_code_info');

const user_infoDAO = webDBUtil.import('../../models/Users/user_info');

/**
 * 谷歌两步验证服务 GoogleService
 * 本页面处理业务逻辑 接收参数与返回处理结果
 */
module.exports = class GoogleService {

    /**
     * 创建Google两步验证
     * @param {*} user
     */
    async create({
        jwt
    }) {
        try {
            const validate = validateTools.validateJWT(jwt);

            // console.log(validate)

            if (validate) {
                // console.log(validate);

                ggCodeInfoDAO.update({
                    is_delete: true
                }, {
                        where: { user_id: validate.data.id },
                        fields: ['is_delete']
                    });

                //查询google验证码信息
                const ggCodeInfo = await ggCodeInfoDAO.findOne({
                    where: { user_id: validate.data.id, enable: 1, is_delete: 0, status: 1 },
                    raw: true
                });

                if (ggCodeInfo) {
                    return result.success('未绑定', 400501);
                } else {
                    var secret = speakeasy.generateSecret({ length: 20 });
                    var token = speakeasy.totp({
                        secret: secret.base32,
                        encoding: 'base32',
                        // counter: 123
                    });

                    // var qrurl = speakeasy.otpauthURL({
                    //     secret: secret.base32,
                    //     label: `${validate.data.name}@4USDT-${moment().format('L')}`,
                    //     issuer: '4USDT'
                    // });

                    await ggCodeInfoDAO.create({
                        user_id: validate.data.id,
                        gg_sc_key: Crypto.encryptByDES(secret.base32),
                        is_delete: 0,
                        enable: 1,
                        version: Date.parse(new Date()),
                        status: 0
                    })

                    let qrurl = secret.otpauth_url + '&issuer=' + encodeURIComponent("4USDT 法币速通")

                    let generateQR = await QRCode.toDataURL(qrurl)
                    generateQR = generateQR.replace("data:image/png;base64,", "")
                    return result.success('获取成功', { generateQR, secret });
                }

            } else {
                return result.authorities();
            }

        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    async bind({ jwt }, body) {
        try {
            const validate = validateTools.validateJWT(jwt);

            if (validate) {
                const { code } = body
                if (!code) return result.paramsLack();

                //查询google验证码信息
                const ggCodeInfo = await ggCodeInfoDAO.findOne({
                    where: { user_id: validate.data.id, enable: 1, is_delete: 0, status: 0 },
                    raw: true
                });
                if (ggCodeInfo) {


                    console.log(Crypto.decryptByDES(ggCodeInfo.gg_sc_key))

                    var token = speakeasy.totp({
                        secret: Crypto.decryptByDES(ggCodeInfo.gg_sc_key),
                        encoding: 'base32'
                    });

                    console.log(`${token} - ${code}`)

                    // Verify a given token
                    var tokenValidates = speakeasy.totp.verify({
                        secret: Crypto.decryptByDES(ggCodeInfo.gg_sc_key),
                        encoding: 'base32',
                        token: code,
                        window: 6,
                        // step: 60,
                        // counter: 123
                    });

                    if (tokenValidates) {
                        await ggCodeInfoDAO.update({
                            status: 1
                        }, {
                                where: { id: ggCodeInfo.id },
                                fields: ['status']
                            });

                        await user_infoDAO.update({
                            authGoogleCode: true
                        }, {
                                where: { id: validate.data.id },
                                fields: ['authGoogleCode']
                            })

                        return result.success("Google两步验证绑定成功");
                    } else {
                        return result.failed('验证失败', 400504);
                    }
                } else {
                    return result.failed('未绑定', 400501);
                }

            } else {
                return result.authorities();
            }

        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    async unbind(session) {
        try {
            //查询google验证码信息
            const ggCodeInfo = await ggCodeInfoDAO.findOne({
                where: { user_id: session.id, enable: 1, is_delete: 0, status: 1 },
                raw: true
            });

            if (ggCodeInfo) {
                await ggCodeInfoDAO.update({
                    status: -2,
                    is_delete: 1
                }, {
                        where: { id: ggCodeInfo.id },
                        fields: ['status', 'is_delete']
                    });

                await user_infoDAO.update({
                    authGoogleCode: false
                }, {
                        where: { id: session.id },
                        fields: ['authGoogleCode']
                    })
                return result.success("Google两步验证解绑成功");
            } else {
                return result.failed('未绑定', 400501);
            }
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    async status(data) {
        try {

            //查询用户信息
            const ggCodeInfo = await ggCodeInfoDAO.findOne({
                where: { user_id: data.id, enable: 1, is_delete: 0, status: 1 },
                raw: true
            });

            if (ggCodeInfo) {
                return result.success('已经绑定', 1);
            } else {
                return result.success('未绑定', 0);
            }


        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    async _Validate(uid, code) {
        //查询google验证码信息
        const ggCodeInfo = await ggCodeInfoDAO.findOne({
            where: { user_id: uid, enable: 1, is_delete: 0, status: 1 },
            raw: true
        });

        if (ggCodeInfo) {

            // Verify a given token
            var tokenValidates = speakeasy.totp.verify({
                secret: Crypto.decryptByDES(ggCodeInfo.gg_sc_key),
                encoding: 'base32',
                token: code
                // ,window: 6,
                // step: 60,
                // counter: 123
            });
            console.log(tokenValidates)
            if (tokenValidates) {
                return result.success("Google验证成功");
            } else {
                return result.failed('验证失败', 400504);
            }
        } else {
            return result.failed('未绑定', 400501);
        }
    }

    async _ValidateBase(uid, code) {
        //查询google验证码信息
        const ggCodeInfo = await ggCodeInfoDAO.findOne({
            where: { user_id: uid, enable: 1, is_delete: 0, status: 1 },
            raw: true
        });

        if (ggCodeInfo) {

            // Verify a given token
            var tokenValidates = speakeasy.totp.verify({
                secret: Crypto.decryptByDES(ggCodeInfo.gg_sc_key),
                encoding: 'base32',
                token: code
                // ,window: 6,
                // step: 60,
                // counter: 123
            });
            return tokenValidates
        } else {
            return false;
        }
    }

    /**
     * 用户登录
     * @param {*} user
     */
    async validate({ jwt }, { code }) {
        try {
            const validate = validateTools.validateJWT(jwt);

            if (validate) {
                if (!code) return result.paramsLack();

                //查询google验证码信息
                const ggCodeInfo = await ggCodeInfoDAO.findOne({
                    where: { user_id: validate.data.id, enable: 1, is_delete: 0, status: 1 },
                    raw: true
                });


                if (ggCodeInfo) {

                    // Verify a given token
                    var tokenValidates = speakeasy.totp.verify({
                        secret: Crypto.decryptByDES(ggCodeInfo.gg_sc_key),
                        encoding: 'base32',
                        token: code
                        // ,window: 6,
                        // step: 60,
                        // counter: 123
                    });

                    if (tokenValidates) {
                        return result.success("Google验证成功");
                    } else {
                        return result.failed('验证失败', 400504);
                    }
                } else {
                    return result.failed('未绑定', 400501);
                }

            } else {
                return result.authorities();
            }

        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }
}
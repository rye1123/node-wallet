import result from '../../tools/Result';
import sequelize from '../../lib/sequelize';
import Crypto from '../../tools/Secret';
import Sign from '../../tools/Sign';
import uuid from 'uuid';
import Utils from '../../tools/Utils';
import RedisDao from '../../lib/RedisDao';
var speakeasy = require("speakeasy");
var moment = require('moment');
var QRCode = require('qrcode');

const { webDBUtil, Sequelize } = sequelize;

const apiInfoDAO = webDBUtil.import('../../models/Security/merchant_api_info');

const user_infoDAO = webDBUtil.import('../../models/Users/user_info');

/**
 * 谷歌两步验证服务 GoogleService
 * 本页面处理业务逻辑 接收参数与返回处理结果
 */
module.exports = class ApiService {

    /**
     * 创建Google两步验证
     * @param {*} user
     */
    async list(session, data) {
        try {

            let apiList = await apiInfoDAO.findAll({
                attributes: ["id", "user_id", "api_key", "bind_ip", "remark", "enable", "created_time", "last_use_time", "readonly", "wallet_deposit", "wallet_withdraw"],
                raw: true,
                where: {
                    is_delete: 0, user_id: session.parent_id || session.id
                }
            })

            return result.success('获取成功', apiList);
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    /**
     * 创建Google两步验证
     * @param {*} user
     */
    async create(session, data) {
        try {

            let existsCount = await apiInfoDAO.count({
                where: {
                    is_delete: 0, user_id: session.id
                },
                col: 'id'
            })

            if (!(data.bind_ip && /^(((25[0-5]|2[0-4]\d|1?\d?\d)\.){3}(25[0-5]|2[0-4]\d|1?\d?\d)[,$]?)+$/.test(data.bind_ip))) {
                data.bind_ip = "0.0.0.0"
            }

            console.log(` existsCount - ${existsCount}`)
            if (existsCount < 10) {
                let secret = uuid.v4();
                let apiInfo = {
                    user_id: session.id,
                    api_key: `${uuid.v4()}`.replace(/-/g, ''),
                    bind_ip: data.bind_ip,
                    remark: data.remark,
                    enable: 1
                }

                data.type.map(itm => {
                    switch (itm) {
                        case "readonly":
                            apiInfo.readonly = 1
                            break;
                        case "deal":
                            apiInfo.wallet_deposit = 1
                            break;
                        case "withdraw":
                            apiInfo.wallet_withdraw = 1
                            break;
                        default:
                            apiInfo.readonly = 1
                            break;
                    }
                })

                apiInfo.sign = await Sign.getRSASign(JSON.stringify(apiInfo), 'merchant_api')
                apiInfo.status = 1
                secret = secret.replace(/-/g, '') + Utils.getMd5(apiInfo.sign)
                apiInfo.api_secret = Crypto.encryptByDES(secret)
                await apiInfoDAO.create(apiInfo)

                return result.success('API信息创建完成', { api_key: apiInfo.api_key, api_secret: secret });
            }
            else {
                return result.failed("api数量不可超过10", 488018);
            }
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    /**
     * 创建Google两步验证
     * @param {*} user
     */
    async destroy(session, data) {
        try {

            let existsCount = await apiInfoDAO.count({
                where: {
                    is_delete: 0, id: data.id, user_id: session.id
                }
            })

            let apiInfo = await apiInfoDAO.findOne({
                where: {
                    id: data.id, user_id: session.id
                }
            })

            RedisDao.del(`4usdt:apiinfo:api:${apiInfo.api_key}`)

            console.log(`destroy existsCount - ${existsCount}`)
            if (existsCount == 1) {
                await apiInfoDAO.update({
                    is_delete: true
                }, {
                        where: { id: data.id, user_id: session.id, is_delete: 0 },
                        fields: ['is_delete']
                    })

                return result.success('API信息删除完成');
            }
            else {
                return result.failed("没有找到API信息", 488026);
            }
        } catch (error) {
            console.log(error);
            return result.failed("API操作失败", 488024);
        }
    }

}

import result from '../../tools/Result';
import sequelize from '../../lib/sequelize';
import middleware from '../../middleware';
import Crypto from '../../tools/Secret';
import RedisDao from '../../lib/RedisDao';
var speakeasy = require("speakeasy");
var moment = require('moment');
var QRCode = require('qrcode');
var sendemail = require('sendemail')
var emailHelper = sendemail.email;


const { webDBUtil, Sequelize } = sequelize;
const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

const ggCodeInfoDAO = webDBUtil.import('../../models/Security/gg_code_info');
const userInfoDAO = webDBUtil.import('../../models/Users/user_info')


/**
 * 谷歌两步验证服务 EmailService
 * 本页面处理业务逻辑 接收参数与返回处理结果
 */
module.exports = class EmailService {

    async send_reg(session, { email }) {

        try {
            if (!email) return result.paramsLack();

            //查询Email验证码信息
            const userInfo = await userInfoDAO.findOne({
                where: { id: session.id, isDelete: 0 },
                raw: true
            });
            if (userInfo.authMailCode == 0) {
                let valiCode = Math.floor(Math.random() * 1000000)
                RedisDao.set(`session_verify:mail:_Bind_Code:_${session.id}`, Crypto.encryptByDES(valiCode), 600)

                var person = {
                    name: '4USDT',
                    email: userInfo.email, // person.email can also accept an array of emails
                    subject: `绑定邮箱验证码 - 4USDT - ${valiCode}`,
                    mail_code: valiCode
                }

                emailHelper('mail_vali_code', person, function(error, result) {
                    console.log(' - - - - - - - - - - - - - - - - - - - - -> email sent: ');
                    console.log(result);
                    console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
                    console.log(error)
                })

                userInfoDAO.update({
                    email: email
                }, {
                    where: { id: session.id },
                    fields: ['email']
                });
                return result.success(null, { type: "mail_bind_code", expires_in: 600 });
            } else {
                return result.failed("该账户已经绑定过邮箱了", 50031)
            }
        } catch (ex) {
            console.log(ex)
            return result.failed()
        }

    }

    async bind(session, { code }) {
        try {
            if (!code) return result.paramsLack();

            //查询google验证码信息
            const userInfo = await userInfoDAO.findOne({
                where: { id: session.id, authMailCode: 0 },
                raw: true
            });

            if (userInfo) {
                let bindEmailCode = await RedisDao.get(`session_verify:mail:_Bind_Code:_${session.id}`)

                if (bindEmailCode == Crypto.encryptByDES(code)) {
                    userInfoDAO.update({
                        authMailCode: 1
                    }, {
                        where: { id: session.id, authMailCode: 0 },
                        fields: ['authMailCode']
                    });

                    return result.success("Email绑定成功");
                } else {
                    return result.failed('验证码错误', 400361);
                }
            } else {
                return result.failed('该用户已经绑定过邮箱', 400341);
            }

        } catch (error) {
            return result.failed();
        }
    }

    async unbind({ jwt }, body) {
        try {
            const validate = validateTools.validateJWT(jwt);

            if (validate) {
                const { code } = body
                if (!code) return result.paramsLack();

                //查询google验证码信息
                const ggCodeInfo = await ggCodeInfoDAO.findOne({
                    where: { user_id: validate.data.id, enable: 1, is_delete: 0, status: 1 },
                    raw: true
                });

                if (ggCodeInfo) {

                    // Verify a given token
                    var tokenValidates = speakeasy.totp.verify({
                        secret: Crypto.decryptByDES(ggCodeInfo.gg_sc_key),
                        encoding: 'base32',
                        token: code,
                        window: 6,
                        // step: 60,
                        // counter: 123
                    });

                    if (tokenValidates) {
                        ggCodeInfoDAO.update({
                            status: -2,
                            is_delete: 1
                        }, {
                            where: { id: ggCodeInfo.id },
                            fields: ['status', 'is_delete']
                        });
                        return result.success("Google两步验证解绑成功");
                    } else {
                        return result.failed('验证失败', 400504);
                    }
                } else {
                    return result.failed('未绑定', 400501);
                }

            } else {
                return result.authorities();
            }

        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    async status({
        jwt
    }) {
        try {
            const validate = validateTools.validateJWT(jwt);

            console.log(validate)
            if (validate) {


                //查询用户信息
                const ggCodeInfo = await ggCodeInfoDAO.findOne({
                    where: { user_id: validate.data.id, enable: 1, is_delete: 0, status: 1 },
                    raw: true
                });

                if (ggCodeInfo) {
                    return result.success('已经绑定', 1);
                } else {
                    return result.success('未绑定', 0);
                }



            } else {
                return result.authorities();
            }

        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }


    /**
     * 验证邮箱验证码
     * @param {*} user
     */
    async validate({ jwt }, body) {
        try {
            const validate = validateTools.validateJWT(jwt);

            if (validate) {
                const { code } = body
                if (!code) return result.paramsLack();

                //查询google验证码信息
                const ggCodeInfo = await ggCodeInfoDAO.findOne({
                    where: { user_id: validate.data.id, enable: 1, is_delete: 0, status: 1 },
                    raw: true
                });

                if (ggCodeInfo) {

                    // Verify a given token
                    var tokenValidates = speakeasy.totp.verify({
                        secret: Crypto.decryptByDES(ggCodeInfo.gg_sc_key),
                        encoding: 'base32',
                        token: code,
                        window: 6,
                        // step: 60,
                        // counter: 123
                    });

                    if (tokenValidates) {
                        return result.success("Google验证成功");
                    } else {
                        return result.failed('验证失败', 400504);
                    }
                } else {
                    return result.failed('未绑定', 400501);
                }

            } else {
                return result.authorities();
            }

        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }
}
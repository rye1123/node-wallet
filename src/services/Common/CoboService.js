// import svgCaptcha from 'svg-captcha';
import result from '../../tools/Result';
// import mongoUtil from '../../lib/mongoUtil';
// import mysqldb from '../../lib/mysql-db';
// var CryptoJS = require("crypto-js");
import Sign from '../../tools/Sign';
import sequelize from '../../lib/sequelize';
import { add, sub } from "../../lib/operation"
const { webDBUtil, Sequelize } = sequelize;

const walletWithdrawDAO = webDBUtil.import('../../models/Wallet/wlt_withdraw_record');
const walletDepositDAO = webDBUtil.import('../../models/Wallet/wlt_deposit_record');
const funds_listDAO = webDBUtil.import('../../models/Amount/funds_list');
const walletDAO = webDBUtil.import('../../models/Wallet/wlt_address_list');
const user_infoDAO = webDBUtil.import('../../models/Users/user_info');
import CommonService from '../../services/Common/CommonService';

import accountAmountService from '../Account/AmountService'
const aAService = new accountAmountService()
const commonService = new CommonService();

// /**
//  * 公共服务系统
//  */
module.exports = class CoboService {

    /**
     * 获取图片验证码
     */
    async callback(data) {
        try {
            if (data.side == "deposit") {

                const usdtAmount = data.abs_amount

                let depositInfo = {
                    userid: 0,
                    value: usdtAmount,
                    symbol: data.coin,
                    tx: data.txid,
                    is_delete: 0
                }
                depositInfo.sign = await Sign.getRSASign(JSON.stringify(depositInfo), 'deposit')
                const addressInfo = await walletDAO.findOne({
                    where: { address: data.address },
                    raw: true
                });

                let foundSymbol = data.coin

                switch (data.coin) {
                    case "BTC_USDT":
                    case "ETH_USDT":
                        foundSymbol = "USDT"
                        break;
                    default:
                        foundSymbol = data.coin;
                        break;
                }


                if (addressInfo) {

                    let notifyUserInfo = await user_infoDAO.findOne({
                        attributes: ['id', "mobile", 'area_code', 'isTraders'],
                        where: {
                            id: addressInfo.userid
                        },
                        raw: true
                    })

                    //push申请
                    let postData = {
                        //推送消息
                        "alert": `您充值的${usdtAmount} ${foundSymbol} 已经到账，请查收`,
                        //推送目标
                        "aliasArr": [notifyUserInfo.id],
                        //场景码
                        "code": 4003,
                        //推送标题
                        "title": "充值到账",
                        "data": {}
                    }


                    if (notifyUserInfo.isTraders) {
                        console.log(`交易员推送提现通知`)
                        console.log(postData)
                        commonService.app_pushMsg(postData)
                    }

                    let amountInfo = await aAService.amountBase(addressInfo.userid, foundSymbol)
                    depositInfo.userid = addressInfo.userid;
                    depositInfo.sign = await Sign.getRSASign(JSON.stringify(depositInfo), 'deposit')
                    depositInfo.status = 1
                    //2019年8月1日 19:19:55  增加了log_amount
                    let foundsInfo = {
                        parent_id: 0,
                        userid: addressInfo.userid,
                        orderNo: data.txid,
                        value: usdtAmount,
                        symbol: foundSymbol,
                        source: "DEPOSIT",
                        createUserId: 0,
                        log_amount: `${add(amountInfo.sum_amount, usdtAmount)}`
                    }
                    foundsInfo.sign = await Sign.getRSASign(JSON.stringify(foundsInfo), 'deposit_founds')
                    foundsInfo.use = 0;
                    // console.log(foundsInfo)

                    let existDepositInfo = await walletDepositDAO.findOne({
                        attributes: ['id'],
                        where: {
                            tx: data.txid
                        },
                        raw: true
                    })

                    if (existDepositInfo) {
                        return true
                    }
                    await walletDepositDAO.create(depositInfo);


                    let existFoundInfo = await funds_listDAO.findOne({
                        attributes: ['id'],
                        where: {
                            orderNo: data.txid
                        },
                        raw: true
                    })
                    if (existFoundInfo) {
                        console.log(`充值记录已经存在`)
                        return true
                    }
                    await funds_listDAO.create(foundsInfo);

                    const Op = Sequelize.Op;

                    let curUserInfo = await user_infoDAO.findOne({
                        attributes: ['id', "mobile", 'name', 'merchantStatus', 'tradersStatus', 'merchantMarginValue', 'tradersMarginValue', 'area_code'],
                        where: {
                            [Op.or]: [{ merchantStatus: 1 }, { tradersStatus: 1 }],
                            id: addressInfo.userid
                        },
                        raw: true
                    })

                    if (curUserInfo) {

                        let freezeStatus = 3;
                        if (curUserInfo.tradersStatus == 1) {
                            console.log(`交易员状态，待冻结保证金`)

                            if (curUserInfo.tradersMarginValue > 0) {
                                let freezeResult = await aAService.marginFreeze({ id: curUserInfo.id }, { value: curUserInfo.tradersMarginValue, symbol: "USDT", source: "MARGIN_TRADER" })
                                if (freezeResult.code == 405602) {
                                    freezeStatus = 1;
                                    console.log(`冻结失败 - ${freezeResult.msg} - ${JSON.stringify(curUserInfo)} - ${JSON.stringify(freezeResult)}`)
                                }
                                else if (freezeResult.code != 0) {
                                    freezeStatus = 2;
                                    console.log(`冻结失败 - ${JSON.stringify(curUserInfo)} - ${JSON.stringify(freezeResult)}`)
                                }
                                else {
                                    console.log(`交易员状态，保证金 - 冻结成功`)
                                }
                            }
                            await user_infoDAO.update({
                                tradersStatus: freezeStatus
                            }, {
                                    where: { id: curUserInfo.id, tradersStatus: 1 },
                                    fields: ['tradersStatus']
                                })
                        }
                        else if (curUserInfo.merchantStatus == 1) {
                            console.log(`商户状态，待冻结保证金`)
                            if (curUserInfo.merchantMarginValue > 0) {
                                let freezeResult = await aAService.marginFreeze({ id: curUserInfo.id }, { value: curUserInfo.merchantMarginValue, symbol: "USDT", source: "MARGIN_MERCHANT" })
                                if (freezeResult.code == 405602) {
                                    freezeStatus = 1;
                                    console.log(`冻结失败 - ${freezeResult.msg} - ${JSON.stringify(curUserInfo)} - ${JSON.stringify(freezeResult)}`)
                                }
                                else if (freezeResult.code != 0) {
                                    freezeStatus = 2;
                                    console.log(`冻结失败 - ${JSON.stringify(curUserInfo)} - ${JSON.stringify(freezeResult)}`)
                                }
                                else {
                                    console.log(`商户状态，保证金 - 冻结成功`)
                                }
                            }
                            await user_infoDAO.update({
                                merchantStatus: freezeStatus
                            }, {
                                    where: { id: curUserInfo.id, merchantStatus: 1 },
                                    fields: ['merchantStatus']
                                })
                        }
                        else {

                        }
                    }

                    await commonService.sendSMS_Deposit(notifyUserInfo.mobile, usdtAmount, foundSymbol, notifyUserInfo.area_code)


                    console.log(`充值到账 - ${notifyUserInfo.mobile}, ${usdtAmount}, ${foundSymbol}`)

                    return true;
                }
                else {
                    depositInfo.status = 0
                    await walletDepositDAO.create(depositInfo)
                    return true;
                }

            } else if (data.side == "withdraw") {
                const usdtAmount = data.abs_amount
                const decimalLength = data.decimal

                let withdrawInfo = await walletWithdrawDAO.findOne({
                    where: {
                        address: data.address,
                        no_id: data.request_id,
                        symbol: data.coin,
                        value: usdtAmount
                    },
                    raw: true
                })
                if (withdrawInfo) {
                    await walletWithdrawDAO.update({
                        tx: data.txid,
                        status: 3
                    }, {
                            where: { no_id: data.request_id },
                            fields: ['status', 'tx']
                        });

                    console.log(`找到回调提现记录，已完成提现 - ${withdrawInfo.value}`)
                    return true
                }
                else {
                    return false
                }
            }

        } catch (e) {
            console.log(e)
            return false
        }
    }
};

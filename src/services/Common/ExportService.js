// import svgCaptcha from 'svg-captcha';
// import result from '../../tools/Result';
// import mongoUtil from '../../lib/mongoUtil';
// import mysqldb from '../../lib/mysql-db';
// var CryptoJS = require("crypto-js");
import result from '../../tools/Result';
import sequelize from '../../lib/sequelize';
import request from '../../tools/request';
// import xlsx from 'node-xlsx';
import uuid from 'uuid';
const { Parser } = require('json2csv');
let OSS = require('ali-oss');


const fs = require('fs');

const { webDBUtil, Sequelize } = sequelize;
const funds_listDAO = webDBUtil.import('../../models/Amount/funds_list');


// /**
//  * 公共服务系统
//  */
module.exports = class ExportService {

    /**
     * 获取图片验证码
     */
    async merchants_funds(session, { symbol, startTime, endTime, source }) {

        try {
            let sortby = 'id'
            let queryData = {
                where: { userid: session.parent_id || session.id, use: 0, value: { [Sequelize.Op.ne]: 0 } },
                raw: true,
                order: [
                    [sortby, 'DESC']
                ],
                attributes: [["orderNo", "orderNo"], ["value", "amount"],
                ["created_time", "createTime"], ["symbol", "symbol"], ["source", "type"], ["log_amount", "balance"]]
            };

            if (source) {
                queryData.where.source = source
            }

            if (symbol) {
                queryData.where.symbol = symbol
            }

            if (startTime && endTime) {
                queryData.where.created_time = {}
                queryData.where.created_time = { [Sequelize.Op.gte]: startTime, [Sequelize.Op.lte]: `${endTime} 23:59:59` }
            }
            else {
                if (startTime) {
                    queryData.where.created_time = {}
                    queryData.where.created_time = { [Sequelize.Op.gte]: startTime }
                }
                if (endTime) {
                    if (queryData.where.created_time) {
                        queryData.where.created_time = { [Sequelize.Op.lte]: endTime }
                    } else {
                        queryData.where.created_time = {}
                        queryData.where.created_time = { [Sequelize.Op.lte]: endTime }
                    }
                }
            }

            const _data = await funds_listDAO.findAll(queryData);
            var _headers = ["OrderNo", "amount", "createTime", "symbol", "type", "balance"]

            if (_data && _data.length > 0) {

                let csv = new Parser(_headers).parse(_data);

                let fileName = `/_export/merchant_funds_${startTime}_${endTime}_${uuid.v1().replace(/-/g, '')}.csv`

                await fs.writeFile(`.${fileName}`, csv, function (err) {
                    if (err) throw err;
                    console.log('file saved');
                });


                let client = new OSS({
                    region: 'oss-cn-hongkong',
                    //云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，部署在服务端使用RAM子账号或STS，部署在客户端使用STS。
                    accessKeyId: 'LTAIVWgFAI5Z6EKt',
                    accessKeySecret: 'mCaAQeFopSzarBonVsD0fAz8M88dGU',
                    bucket: '4uu'
                });

                // object表示上传到OSS的Object名称，localfile表示本地文件或者文件路径
                let r1 = await client.put(fileName, `.${fileName}`)

                console.log('put success: %j', r1.url);
                console.log(client.get('object'));
                // .then(function (r1) {
                //     console.log('put success: %j', r1);
                //     return client.get('object');
                // }).then(function (r2) {
                //     console.log('get success: %j', r2);
                // }).catch(function (err) {
                //     console.error('error: %j', err);
                // });

                // var headers = _headers
                //     .map((v, i) => Object.assign({}, { v: v, position: String.fromCharCode(65 + i) + 1 }))
                //     .reduce((prev, next) => Object.assign({}, prev, { [next.position]: { v: next.v } }), {});
                // var data = _data
                //     .map((v, i) => _headers.map((k, j) => Object.assign({}, { v: v[k], position: String.fromCharCode(65 + j) + (i + 2) })))
                //     .reduce((prev, next) => prev.concat(next))
                //     .reduce((prev, next) => Object.assign({}, prev, { [next.position]: { v: next.v } }), {});


                // // 合并 headers 和 data
                // var output = Object.assign({}, headers, data);
                // // 获取所有单元格的位置
                // var outputPos = Object.keys(output);

                // // 计算出范围
                // var ref = outputPos[0] + ':' + outputPos[outputPos.length - 1];

                // // 构建 workbook 对象
                // var wb = {
                //     SheetNames: ['mySheet'],
                //     Sheets: {
                //         'mySheet': Object.assign({}, output, { '!ref': ref })
                //     }
                // };

                // console.log(headers)
                // console.log(data)

                // 导出 Excel
                // xlsx.writeFile(wb, 'output.xlsx');
                // var buffer = xlsx.build([{ name: "mySheetName", data: data }]);
                return result.success("导出成功", r1.url);

            } else {
                return result.failed("暂无数据", 3546810);
            }
        } catch (error) {
            console.log(error)
            return result.failed(error);
        }
    }

};

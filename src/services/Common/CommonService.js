// import svgCaptcha from 'svg-captcha';
// import result from '../../tools/Result';
// import mongoUtil from '../../lib/mongoUtil';
// import mysqldb from '../../lib/mysql-db';
// var CryptoJS = require("crypto-js");
import { MicroService, ThirdPartyService } from '../../config.service';
import request from '../../tools/request';
import RedisDao from '../../lib/RedisDao';

const API_SYSTEM_SEND_MESSAGE_URL = `${MicroService.API_merchant_type}${MicroService.API_merchant_host}:${MicroService.API_merchant_port}/api/v1.1/message/send`

const API_SYSTEM_PUSH_MSG_APP_URL = `${MicroService.API_merchant_type}${MicroService.API_merchant_host}:${MicroService.API_merchant_port}/api/v1.1/message/jiguang/push`

const API_SYSTEM_TRADERS_GET_SET_OFF_LINE_URL = `${MicroService.API_traders_type}${MicroService.API_traders_host}:${MicroService.API_traders_port}/api/traders/isOnline`

const API_KS_SIGN_URL = `${ThirdPartyService.API_Ks_Sign_type}${ThirdPartyService.API_Ks_Sign_host}/ks_sign.php` //?rdm=0799687066&expired=100&current_time=1530762118

const API_KS_GET_BIZ_TOKEN_URL = `${ThirdPartyService.KuangShi_API_URL}/lite/v1/get_biz_token`
const API_KS_GET_RESULT_URL = `${ThirdPartyService.KuangShi_API_URL}lite/v1/get_result`

// /**
//  * 公共服务系统
//  */
module.exports = class CommonService {

    async ks_get_biz_token(params) {
        let url = `${API_KS_GET_BIZ_TOKEN_URL}`
        console.log(`旷视签名-get_biz_token`)
        console.log(url)
        console.log(API_KS_SIGN_URL)
        console.log(`用户信息`)
        console.log(params)

        let sign = await this.ks_gen_sign()
        console.log(`签名:${sign}`)

        let postData = {
            "sign": sign,
            "sign_version": "hmac_sha1",
            "return_url": params.return_url || `${ThirdPartyService.API_Ks_Return_url}`,
            "notify_url": `${ThirdPartyService.API_Ks_Notify_url}`,
            "biz_no": params.orderNo || '',
            "comparison_type": 1,
            "group": 0,//0为不需要用户上传，只需要提供证件号码与姓名；1位需要用户自己拍照上传身份证
            "idcard_name": params.idcard_name || '张三',
            "idcard_number": params.idcard_number || "331155198912120011",
            "idcard_threshold": 0.8,
            "idcard_retry_time": 5,
            "idcard_side": 0,
            "liveness_type": "video_number",
            "security_level": 3,
            "liveness_preferences": 2,
            "liveness_retry_count": 5,
            "uuid": '',
            // "image_ref1": '',//当comparison_type=0时，必选。
            // "image_ref2": ''//当comparison_type=0时，必选。
        }

        let { response, body } = await request.post(
            url, postData
        )
        // body.biz_token
        // body.request_id
        // body.time_used

        console.log(`返回结果`)
        console.log(body)

        return body
    }

    async ks_get_result(params) {

        let url = `${API_KS_GET_RESULT_URL}`
        console.log(`旷视签名-get_result`)
        console.log(url)
        console.log(API_KS_SIGN_URL)
        console.log(`用户信息`)
        console.log(params)

        let sign = await this.ks_gen_sign()
        console.log(`签名:${sign}`)

        let postData = {
            "sign": sign,
            "sign_version": "hmac_sha1",
            "biz_token": params.biz_token,
            "verbose": 1
        }

        let { response, body } = await request.get(
            `${url}?sign=${sign}&sign_version=hmac_sha1&biz_token=${params.biz_token}&verbose=1`
        )
        console.log(`返回结果`)
        // console.log(body)

        return body
    }

    async ks_gen_sign() {
        try {
            let random = `${Math.random().toString().slice(-10)}`
            let current_time = Date.parse(new Date()) / 1000;
            let expire_time = current_time + 300;

            let url = `${API_KS_SIGN_URL}?rdm=${random}&expired=300&current_time=${current_time}`

            let { response, body } = await request.get(url)
            if (response.statusCode !== 200) {
                return response
            } else {
                return body
            }

        } catch (e) {
            return false
        }
    }

    /**
     * 获取图片验证码
     */
    async sendSMS({ phone, code, country_code }, cachekey) {
        try {

            console.log(arguments)

            country_code = country_code || "+86"

            if (`${country_code}`.indexOf("+") < 0) {
                country_code = `+${country_code}`
            }
            let url = `${API_SYSTEM_SEND_MESSAGE_URL}?type=2&countryCode=${encodeURIComponent(country_code)}&mobile=${phone}&contents=${code}`

            console.log(url)
            console.log('send SMS')
            console.log(body)
            if (cachekey) {
                const hasLock = await RedisDao.get(`${cachekey}_lock`)
                if (hasLock) {
                    return "每分钟一个账号只能发送一次"
                }
            }
            else {
                await RedisDao.set(`${cachekey}_lock`, '', 60)
            }

            let { response, body } = await request.get(url)
            if (response.statusCode !== 200) {
                return false
            } else {
                return body
            }

        } catch (e) {
            return false
        }
    }

    async app_pushMsg(data) {
        try {

            let url = `${API_SYSTEM_PUSH_MSG_APP_URL}`

            let postData = {
                //推送消息
                "alert": data.alert,
                //推送目标
                "aliasArr": data.aliasArr,
                //场景码
                "code": data.code,
                //推送标题
                "title": data.title,
                "data": data.data
            }


            let { response, body } = await request.post(
                url, postData
            )

            return body
        } catch (err) {
            console.log(err)
            return "推送出现异常"
        }
    }

    async sendSMS_Deposit(phone, amount, symbol, country_code) {
        try {
            country_code = country_code || "+86"

            if (`${country_code}`.indexOf("+") < 0) {
                country_code = `+${country_code}`
            }
            let url = `${API_SYSTEM_SEND_MESSAGE_URL}?type=3&countryCode=${encodeURIComponent(country_code)}&mobile=${phone}&contents=${amount},${symbol}`

            let { response, body } = await request.get(url)
            if (response.statusCode !== 200) {
                console.log(`发送充值短信异常`)
                return false
            } else {
                console.log(`发送充值短信成功`)
                return body
            }

        } catch (e) {
            return false
        }
    }

    async setTradersOffLine(userId) {
        return this.setTradersOnLineStatus(userId, "OFF")
    }

    async setTradersOnLine(userId) {
        return this.setTradersOnLineStatus(userId, "ON")
    }

    async setTradersOnLineStatus(userId, status) {
        try {
            let url = `${API_SYSTEM_TRADERS_GET_SET_OFF_LINE_URL}?tradersNo=${userId}&isOnline=${status}`

            let { response, body } = await request.get(url)

            if (response.statusCode !== 200) {
                return result.failed(JSON.stringify(body))
            } else {
                return body
            }

        } catch (e) {
            return false
        }

    }

    //     /**
    //      * 获取图片验证码
    //      */
    //     async getImgValidate() {
    //         const { text, data } = svgCaptcha.create({
    //             inverse: false, // 翻转颜色
    //             size: 4, //随机字符串长度
    //             noise: 4, // 噪声线条数
    //             fontSize: 46,
    //             width: 100,
    //             height: 30,
    //             color: true //随机颜色
    //             // background: true
    //         });;
    //         return { result: result.success(null, data), text };
    //     }

    //     async generateNewKey() {
    //         var bitcoin = require("bitcoinjs-lib");
    //         let keyPair = bitcoin.ECPair.makeRandom();
    //         const { data } = {
    //             privateKey: keyPair.d,
    //             print16: keyPair.d.toHex(),
    //             print32: keyPair.d.toHex(32),
    //         }
    //         // 打印私钥:
    //         console.log(keyPair.d);
    //         // 以十六进制打印:
    //         console.log(keyPair.d.toHex());
    //         // 补齐32位:
    //         console.log(keyPair.d.toHex(32));

    //         return { result: result.success(null, data), text: "generateNewKey" };

    //     }

    //     async generateAddress() {
    //         var bitcoin = require("bitcoinjs-lib");
    //         let keyPair = bitcoin.ECPair.makeRandom();
    //         const { address } = bitcoin.payments.p2pkh({ pubkey: keyPair.publicKey })

    //         let createtime = Math.floor(Date.now() / 1000)
    //         let sign = CryptoJS.AES.encrypt(JSON.stringify(address + createtime), 'bitMall#hongyi@allblue!@#$%').toString();

    //         mysqldb.operateSingle("webDB","insert into address_list (address,symbol,createtime,sign) values(?,?,?,?)",[address,"BTC",createtime,sign])

    //         const data = {
    //             wif: keyPair.toWIF(),
    //             // addr: keyPair.getAddress(),
    //             // result: "ahahhahahaha",
    //             address,
    //             sign
    //             // privateKey : keyPair.__d,
    //             // print16:keyPair.__d.toHex(),
    //             // print32:keyPair.__d.toHex(32),
    //         }

    //         try {
    //             const table = 'addressinfo',dbName='wallet'
    //             const res = await mongoUtil.insert({table, data, dbName});
    //             // console.log(res.documents)
    //             if (res > 0) {
    //                 return result.success("生成成功",{});
    //             } else {
    //                 return result.failed();
    //             }
    //         } catch (error) {
    //             // console.log(error);
    //             return result.failed(error.toString());
    //         }
    //         // 打印私钥:
    //         // console.log(keyPair.d);
    //         // // 以十六进制打印:
    //         // console.log(keyPair.d.toHex());
    //         // // 补齐32位:
    //         // console.log(keyPair.d.toHex(32));

    //         return { result: result.success(null, data), text: "generateAddress" };
    //     }

    //     async signMsg() {
    //         const bitcoin = require('bitcoinjs-lib');
    //         console.log(bitcoin)
    //         let
    //             message = 'a secret message!',
    //             hash = bitcoin.crypto.sha256(message),
    //             wif = 'L16zYACWmkQXMUhYAzLA1dWEg95yv8VokSVFcadgpMFeTQoDzMPz',
    //             keyPair = bitcoin.ECPair.fromWIF(wif);
    //         console.log(keyPair)
    //         // 用私钥签名:
    //         let signature = keyPair.sign(hash).toDER(); // ECSignature对象
    //         // 打印签名:
    //         console.log(signature.toString('hex'));
    //         // 打印公钥以便验证签名:
    //         console.log(keyPair.getPublicKeyBuffer().toString('hex'));

    //         return { result: result.success(null, { message, hash, sigh: signature.toString('hex'), hex: keyPair.getPublicKeyBuffer().toString('hex') }), text: "signMsg" };
    //     }

};

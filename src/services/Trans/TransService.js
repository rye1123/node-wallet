import mysqldb from '../../lib/mysql-db';
import result from '../../tools/Result';
import { ENV } from '../../config';
var CryptoJS = require("crypto-js");
var asyncrequest = require("request");
const iv = CryptoJS.enc.Utf8.parse("f*f/0*$bi3$alEQl")
const hangfeiKey = CryptoJS.enc.Utf8.parse("f*f/0*$bi3$alEQl")
const isDev = ENV.isDev;

const allblueApiUrl = !isDev ? 'http://sky.bitmall.com' : 'http://192.168.50.106:8888';

function log(txt) {
    console.log(`${new Date().toLocaleString()}\t${txt}`);
}

module.exports = class TransService {

    //得到所有未转账订单的记录
    async transRecord({ sign }) {
        try {
            let createtime = Math.floor(Date.now() / 1000)
            // log(CryptoJS.AES.encrypt(JSON.stringify({id:1}), 'via!&*$bitMall%a*(2G#*allblue').toString())
            let de_sign_params = CryptoJS.AES.decrypt(sign, 'via!&*$bitMall%a*(2G#*allblue').toString(CryptoJS.enc.Utf8)

            if (createtime - de_sign_params < 60) {
                log("1分钟内");
            }
            else {
                log("超出1分钟")
            }
            let res = await mysqldb.queryList("webDB", " select * from trans_record where status = 0 limit 10 ", [])
            let _resut;
            if (res && res.length > 0) {
                res = CryptoJS.AES.encrypt(JSON.stringify(res), 'via!&*$bitMall%a*(2G#*allblue').toString();
                _resut = result.success("查询成功", res);
            }
            else {
                if (res.length == 0) {
                    _resut = result.failed("暂无转币订单");
                }
                else {
                    _resut = result.failed();
                }
            }

            return _resut;
        }
        catch (ex) {
            return result.failed("请求不合法" + ex);
        }
    }

    //锁定转账订单
    async blockTrans({ sign }) {
        try {
            if (!sign) return result.paramsLack();
            let _params = JSON.parse(CryptoJS.AES.decrypt(sign, 'via!&*$bitMall%a*(2G#*allblue').toString(CryptoJS.enc.Utf8))

            let res = await mysqldb.operateSingle("webDB", "update trans_record set status = -1 where id = ? and status = 0", [_params.id])
            let _resut;
            // log()
            if (res && res.affectedRows > 0) {
                _resut = result.success("锁单成功");
            }
            else {
                _resut = result.failed();
            }

            return _resut;
        }
        catch (ex) {

        }
    }

    //完成转账，需提供tx
    async complateTrans({ sign }) {
        try {
            let _params = JSON.parse(CryptoJS.AES.decrypt(sign, 'via!&*$bitMall%a*(2G#*allblue').toString(CryptoJS.enc.Utf8))

            let res = await mysqldb.operateSingle("webDB", "update trans_record set status = 1,tx = ? where id = ? and status = -1", [_params.tx, _params.id])

            let orderInfo = await mysqldb.operateSingle("webDB", "select orderNo from trans_record where id = ? and status = 1", [_params.id])

            let _resut;
            if (res && res.affectedRows > 0) {
                _resut = result.success("转账成功");
                let allurl = `${allblueApiUrl}/allblueapi/coin/callback`
                let resultData = {
                    "code": "200",
                    "msg": "转账完成",
                    "orderNo": orderInfo[0].orderNo
                }
                log(allurl)
                log(JSON.stringify(resultData))
                asyncrequest({
                    url:allurl,
                    body: resultData,
                    method: "POST",
                    json:true,
                    headers: {
                        "content-type": 'application/json;charset=utf-8'
                    }
                },function(){
                    log(JSON.stringify(arguments))
                })
            }
            else {
                _resut = result.failed();
            }

            return _resut;
        }
        catch (ex) {

        }
    }

    //订单异常
    async errorTrans({ sign }) {
        try {
            let _params = JSON.parse(CryptoJS.AES.decrypt(sign, 'via!&*$bitMall%a*(2G#*allblue').toString(CryptoJS.enc.Utf8))

            let res = await mysqldb.operateSingle("webDB", "update trans_record set status = -2 where id = ? and status = -1", [_params.id])
            let _resut;
            if (res && res.affectedRows > 0) {
                _resut = result.success("反馈订单异常已记录");
            }
            else {
                _resut = result.failed();
            }

            return _resut;
        }
        catch (ex) {

        }
    }

    //记录订单异常
    async markErrorTrans({ sign }) {
        try {
            let _params = JSON.parse(CryptoJS.AES.decrypt(sign, 'via!&*$bitMall%a*(2G#*allblue').toString(CryptoJS.enc.Utf8))

            let res = await mysqldb.operateSingle("webDB", "update trans_record set status = -3,remark=? where id = ? and status = -1", [_params.mark, _params.id])
            let _resut;
            if (res && res.affectedRows > 0) {
                _resut = result.success("反馈订单异常已记录");
            }
            else {
                _resut = result.failed();
            }

            return _resut;
        }
        catch (ex) {

        }
    }

    //记录订单异常
    async writeBroadcast({ tx }) {
        try {
            let createtime = Math.floor(Date.now() / 1000)

            let res = await mysqldb.operateSingle("webDB", "insert into broadcast_record (`tx`,`createtime`,`status`) values (?,?,0) ", [tx, createtime])
            let _resut;
            if (res && res.affectedRows > 0) {
                _resut = result.success("广播信息已保存");
            }
            else {
                _resut = result.failed();
            }

            return _resut;
        }
        catch (ex) {

        }
    }

    //添加转账记录
    async toAddress({ to, count, memo, symbol, sign, orderNo }) {
        try {
            // if (!to || !count || !symbol || !sign || !orderNo) return result.paramsLack();

            let createtime = Math.floor(Date.now() / 1000)
            // CryptoJS.RC4
            // let _sign = CryptoJS.AES.encrypt(JSON.stringify([count, memo, orderNo, symbol, to]), 'f*f/0*$bi3$alEQl').toString();

            ///_sign需要修改成sign
            let de_sign_params = JSON.parse(CryptoJS.AES.decrypt(sign, hangfeiKey, {
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            }).toString(CryptoJS.enc.Utf8))

            let transOrderList = await mysqldb.queryList("webDB", " select id from trans_record where orderNo = ?  limit 1 ", orderNo)
            if (transOrderList.length == 0 && de_sign_params[0] == count && de_sign_params[1] == memo && de_sign_params[2] == orderNo && de_sign_params[3] == symbol && de_sign_params[4] == to) {
                let _resut = await mysqldb.operateSingle("webDB", "insert into trans_record (`to`,`memo`,`count`,`symbol`,`sign`,`createtime`,`status`,`orderNo`) values (?,?,?,?,?,?,?,?) ",
                    [to, memo, count, symbol, sign, createtime, 0, orderNo]);
                return result.success("转账发起成功", _resut);
            }
            else {
                return result.failed(`数据不合法或者记录已存在`);
            }
        }
        catch (ex) {
            log(ex)
        }
    }

    async deSign({ sign }) {
        let _result = {};

        log(CryptoJS.AES.encrypt(JSON.stringify(['0.0000001', '', 'BUY201902198', 'BTC', '1FZrQi873doCSaLDMfmxPuD7hgdaVirK4z']), hangfeiKey, {
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        }).toString())

        _result.org = sign
        _result.test = CryptoJS.AES.encrypt("test", iv, {
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        }).toString()
        _result.de_test = CryptoJS.AES.decrypt(_result.test, iv, {
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        }).toString(CryptoJS.enc.Utf8)
        let de_sign_params = CryptoJS.AES.decrypt(sign, iv, {
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        }).toString(CryptoJS.enc.Utf8)
        _result.result = de_sign_params
        return result.success("success", _result);
    }
}
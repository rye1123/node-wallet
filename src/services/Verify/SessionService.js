import services from '../../services/Security/GoogleService';

import Crypto from '../../tools/Secret';
import RedisDao from '../../lib/RedisDao';

// console.log(services)
const googleService = new services();

/**
 * 用户信息服务 AccountService
 * 本页面处理业务逻辑 接收参数与返回处理结果
 */
module.exports = class SessionService {

    // {
    //     "id": 2356,
    //     "enable": 1,
    //     "mobile": "13321115828",
    //     "email": "29498587@163.com",
    //     "name": "ljn",
    //     "authGoogleCode": 0,
    //     "authPhoneCode": 1,
    //     "authMailCode": 0,
    //     "authFundsPassword": 0,
    //     "merchantId": 111111
    // }
    async Verify(session, data, isStrong = false) {
        console.log(`验证 - ${process.env.SYS_ENV}`)
        if (process.env.SYS_ENV == "development") {
            console.log(`测试环境-验证通过`)
            return {
                success: true,
                msg: "测试环境"
            }
        }

        let result = {
            success: true,
            msg: ""
        }
        let verifyCount = 0;
        try {
            let emailCodeKey = `session_verify:mailCode:_${session.id}`
            let phoneCodeKey = `session_verify:phoneCode:_${session.id}`

            if (isStrong) {

                console.log(session)
                if (result.success) { //session.authGoogleCode &&
                    let googleResut = await googleService._ValidateBase(session.id, data.googleCode)
                    if (!googleResut) {
                        result.msg = "Google验证码错误"
                        result.success = false
                    }
                    else {
                        console.log(`Google验证码-验证成功-${data.googleCode}`)
                    }
                }
                if (result.success) { //session.authPhoneCode &&
                    if (await RedisDao.get(phoneCodeKey) != Crypto.encryptByDES(data.phoneCode)) {
                        result.msg = "手机验证码错误"
                        result.success = false
                    }
                    else {
                        console.log(`手机验证码-验证成功-${data.phoneCode}`)
                    }
                }
                if (result.success) { //session.authMailCode &&
                    if (await RedisDao.get(emailCodeKey) != Crypto.encryptByDES(data.emailCode)) {
                        result.msg = "邮箱验证码错误"
                        result.success = false
                    }
                    else {
                        console.log(`邮箱验证码-验证成功-${data.emailCode}`)
                    }
                }
                if (session.authFundsPassword) {

                }
            } else {
                if (typeof (data.phoneCode) == "string" && result.success) {
                    if (await RedisDao.get(phoneCodeKey) != Crypto.encryptByDES(data.phoneCode)) {
                        result.msg = "手机验证码错误"
                        result.success = false
                    }
                    else {
                        verifyCount++
                        console.log(`手机验证码-验证成功-${data.phoneCode}`)
                    }
                }
                if (typeof (data.emailCode) == "string" && result.success) {
                    if (await RedisDao.get(emailCodeKey) != Crypto.encryptByDES(data.emailCode)) {
                        result.msg = "邮箱验证码错误"
                        result.success = false
                    }
                    else {
                        verifyCount++
                        console.log(`邮箱验证码-验证成功-${data.emailCode}`)
                    }
                }
                if (typeof (data.googleCode) == "string" && result.success) {
                    let googleResut = await googleService._ValidateBase(session.id, data.googleCode)

                    if (!googleResut) {
                        result.msg = "Google验证码错误"
                        result.success = false
                    }
                    else {
                        verifyCount++
                        console.log(`Google验证码-验证成功-${data.googleCode}`)
                    }
                }
                if (verifyCount == 0) {
                    result.success = false
                    result.msg = "没有验证"
                }
            }
            if (result.success) {
                await RedisDao.del(emailCodeKey)
                await RedisDao.del(phoneCodeKey)
            }
            return result
        }
        catch (error) {
            console.log(error)
            return {
                success: false,
                msg: error
            }
        }
    }

    /**
     * 获取用户信息
     * @param {*} ctx
     */
    // async !withdraw(session, data, validata) {
    //     try {

    //         let amountInfo = await aAService.amountBase(session.id)

    //         console.log(data)

    //         if (amountInfo.available >= data.value) {

    //             await walletWithdrawDAO.update({
    //                 enable: false
    //             }, {
    //                     where: { userid: session.id },
    //                     fields: ['enable']
    //                 });


    //             //这个格式不要变，不然会比较难验证签名
    //             let record = {
    //                 userid: session.id,
    //                 address: data.address,
    //                 symbol: data.symbol,
    //                 value: data.value,
    //                 enable: true,
    //                 status: 0,
    //             }

    //             record.sign = await Sign.getRSASign(JSON.stringify(record), 'withdraw')
    //             await walletWithdrawDAO.create(record)


    //             let foudInfo = {
    //                 source: "WITHDRAW",
    //                 symbol: "USDT",
    //                 value: data.value,
    //                 createUserId: session.id,
    //                 orderNo: "WITHDRAW",
    //                 userid: session.id
    //             }

    //             console.log(await funds_listDAO.create(foudInfo));

    //             return result.success(null);
    //         } else {
    //             return result.failed(`余额不足，当前可用余额：${amountInfo.available}`, 50082);
    //         }

    //     } catch (error) {
    //         return result.failed(error);
    //     }
    // }

}

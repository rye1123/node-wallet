import result from '../../tools/Result';
import Utils from '../../tools/Utils';
import Sign from '../../tools/Sign';
import sequelize from '../../lib/sequelize';
/* eslint-disable */
const { webDBUtil, Sequelize } = sequelize;
const userMerchantSettingModel = webDBUtil.import('../../models/Users/user_merchant_setting');
const userMerchantStrategyModel = webDBUtil.import('../../models/Users/user_merchant_strategy_withdraw');
const userMerchantWhiteListAddressModel = webDBUtil.import('../../models/Users/user_merchant_withdraw_whitelist');
/* eslint-endble */

/**
 * 业务示例代码
 */
module.exports = class SettingService {


    //#region   地址白名单 操作


    //白名单地址列表
    async withdraw_white_address_list(session, { page = 1, per_page = 10 }) {
        try {
            let queryData = {
                where: { isDelete: 0, user_id: session.parent_id || session.id },
                raw: true,
                order: [
                    ["id", 'DESC']
                ],
                attributes: [
                    "id",
                    "address",
                    "symbol",
                    "enable",
                    "created_time"
                ]
            };

            if (!page) {
                page = 1
            }

            if (per_page && per_page > 100) {
                per_page = 100
            }
            else {
                per_page = 10
            }
            //分页
            queryData.offset = Number((page - 1) * per_page); //开始的数据索引
            queryData.limit = Number(per_page); //每页限制返回的数据条数

            const { rows, count } = await userMerchantWhiteListAddressModel.findAndCountAll(queryData)

            return result.pageData('获取成功', null, rows, count, page, per_page);
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    //添加白名单地址
    async withdraw_white_address_add(session, data) {
        try {

            let existsCount = await userMerchantWhiteListAddressModel.count({
                where: {
                    isDelete: 0, user_id: session.id, symbol: data.symbol, address: data.address
                },
                col: 'id'
            })

            console.log(` existsCount - ${existsCount}`)
            if (existsCount < 1) {
                let strategyInfo = {
                    user_id: session.id,
                    address: data.address,
                    symbol: data.symbol,
                    isDelete: 0,
                    enable: 1
                }

                strategyInfo.sign = await Sign.getRSASign(JSON.stringify(strategyInfo), 'withdraw_white_address')
                strategyInfo.status = 1

                console.log("添加提现地址白名单---------")
                console.log(strategyInfo)
                await userMerchantWhiteListAddressModel.create(strategyInfo)

                return result.success('白名单地址添加完成');
            }
            else {
                return result.failed("该地址已经存在", 668018);
            }
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }


    //编辑白名单地址
    async withdraw_white_address_edit(session, data) {
        try {

            let existsCount = await userMerchantWhiteListAddressModel.findOne({
                where: {
                    isDelete: 0, id: data.id, user_id: session.id
                },
                col: 'id'
            })

            if (existsCount) {
                let strategyInfo = {
                    user_id: existsCount.user_id,
                    address: data.address,
                    symbol: data.symbol,
                    isDelete: 0,
                    enable: 1
                }

                strategyInfo.sign = await Sign.getRSASign(JSON.stringify(strategyInfo), 'withdraw_white_address')

                console.log("更新地址白名单---------")
                console.log(strategyInfo)

                await userMerchantWhiteListAddressModel.update(strategyInfo, {
                    where: { id: existsCount.id, isDelete: 0 },
                    fields: ['address', 'symbol']
                });

                return result.success('白名单地址修改完成');
            }
            else {
                return success.failed("白名单地址未找到", 6201537)
            }
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    //删除白名单地址
    async withdraw_white_address_delete(session, data) {

        try {
            let existsCount = await userMerchantWhiteListAddressModel.count({
                where: {
                    isDelete: 0, id: data.id, user_id: session.id
                },
                col: 'id'
            })

            if (existsCount == 1) {

                await userMerchantWhiteListAddressModel.update({ isDelete: 1 }, {
                    where: { id: data.id, isDelete: 0 },
                    fields: ['isDelete']
                });

                return result.success('白名单地址已删除');
            }

        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    //启用白名单地址
    async withdraw_white_address_enable(session, data) {
        try {
            let existsCount = await userMerchantWhiteListAddressModel.count({
                where: {
                    isDelete: 0, id: data.id, user_id: session.id, enable: 0
                },
                col: 'id'
            })

            if (existsCount == 1) {

                await userMerchantWhiteListAddressModel.update({ enable: 1 }, {
                    where: { id: data.id, isDelete: 0, enable: 0 },
                    fields: ['enable']
                });
                return result.success('白名单地址已启用');
            }
            else {
                return success.failed("白名单地址未找到", 6101422)
            }

        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    //禁用白名单地址
    async withdraw_white_address_disable(session, data) {
        try {
            let existsCount = await userMerchantWhiteListAddressModel.count({
                where: {
                    isDelete: 0, id: data.id, user_id: session.id, enable: 1
                },
                col: 'id'
            })

            if (existsCount == 1) {

                await userMerchantWhiteListAddressModel.update({ enable: 0 }, {
                    where: { id: data.id, isDelete: 0, enable: 1 },
                    fields: ['enable']
                });
                return result.success('白名单地址已禁用');
            }
            else {
                return success.failed("白名单地址未找到", 6268417)
            }

        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }



    //#endregion

    //#region   策略配置 操作

    //策略列表
    async withdraw_strategy_list(session, { page, per_page }) {
        try {
            let queryData = {
                where: { isDelete: 0, user_id: session.parent_id || session.id },
                raw: true,
                order: [
                    ["id", 'DESC']
                ],
                attributes: [
                    "id",
                    "limit_per",
                    "limit_hour",
                    "limit_day",
                    "limit_approval",
                    "symbol",
                    "enable",
                    "created_time"
                ]
            };

            if (!page) {
                page = 1
            }

            if (per_page && per_page > 100) {
                per_page = 100
            }
            else {
                per_page = 10
            }
            //分页
            queryData.offset = Number((page - 1) * per_page); //开始的数据索引
            queryData.limit = Number(per_page); //每页限制返回的数据条数

            const { rows, count } = await userMerchantStrategyModel.findAndCountAll(queryData)

            return result.pageData('获取成功', null, rows, count, page, per_page);
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    //添加策略
    async withdraw_strategy_add(session, data) {
        try {

            let existsCount = await userMerchantStrategyModel.count({
                where: {
                    isDelete: 0, user_id: session.id, symbol: data.symbol
                },
                col: 'id'
            })

            console.log(` existsCount - ${existsCount}`)
            if (existsCount < 1) {
                let strategyInfo = {
                    user_id: session.id,
                    limit_per: data.limit_per || 0,
                    limit_hour: data.limit_hour || 0,
                    limit_day: data.limit_day || 0,
                    limit_approval: data.limit_approval || 0,
                    symbol: data.symbol,
                    isDelete: 0,
                    enable: 1
                }

                strategyInfo.sign = await Sign.getRSASign(JSON.stringify(strategyInfo), 'withdraw_strategy')
                strategyInfo.status = 1

                console.log("添加提现策略---------")
                console.log(strategyInfo)
                await userMerchantStrategyModel.create(strategyInfo)

                return result.success('策略信息创建完成');
            }
            else {
                return result.failed("每个币种仅可设置一个提币策略", 416018);
            }
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }


    //编辑策略
    async withdraw_strategy_edit(session, data) {
        try {

            let existsCount = await userMerchantStrategyModel.findOne({
                where: {
                    isDelete: 0, id: data.id, user_id: session.id, symbol: data.symbol
                },
                col: 'id'
            })

            if (existsCount.symbol != data.symbol) {
                return success.failed("策略币种不可修改", 6201536)
            }

            if (existsCount) {
                let strategyInfo = {
                    user_id: existsCount.user_id,
                    limit_per: data.limit_per || existsCount.limit_per,
                    limit_hour: data.limit_hour || existsCount.limit_hour,
                    limit_day: data.limit_day || existsCount.limit_day,
                    limit_approval: data.limit_approval || existsCount.limit_approval,
                    symbol: existsCount.symbol,
                    isDelete: 0,
                    enable: 1
                }

                strategyInfo.sign = await Sign.getRSASign(JSON.stringify(strategyInfo), 'withdraw_strategy')


                console.log("修改提现策略---------")
                console.log(strategyInfo)
                await userMerchantStrategyModel.update(strategyInfo, {
                    where: { id: existsCount.id, isDelete: 0 },
                    fields: ['limit_per', 'limit_hour', 'limit_day']
                });

                return result.success('策略信息修改完成');
            }
            else {
                return success.failed("策略未找到", 6201537)
            }
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    //删除策略
    async withdraw_strategy_delete(session, data) {

        try {
            let existsCount = await userMerchantStrategyModel.count({
                where: {
                    isDelete: 0, id: data.id, user_id: session.id
                },
                col: 'id'
            })

            if (existsCount == 1) {

                await userMerchantStrategyModel.update({ isDelete: 1 }, {
                    where: { id: data.id, isDelete: 0 },
                    fields: ['isDelete']
                });

                return result.success('策略已删除');
            }

        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    //启用策略
    async withdraw_strategy_enable(session, data) {
        try {
            let existsCount = await userMerchantStrategyModel.count({
                where: {
                    isDelete: 0, id: data.id, user_id: session.id, enable: 0
                },
                col: 'id'
            })

            if (existsCount == 1) {

                await userMerchantStrategyModel.update({ enable: 1 }, {
                    where: { id: data.id, isDelete: 0, enable: 0 },
                    fields: ['enable']
                });
                return result.success('策略已启用');
            }
            else {
                return success.failed("策略未找到", 6201537)
            }

        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    //禁用策略
    async withdraw_strategy_disable(session, data) {
        try {
            let existsCount = await userMerchantStrategyModel.count({
                where: {
                    isDelete: 0, id: data.id, user_id: session.id, enable: 1
                },
                col: 'id'
            })

            if (existsCount == 1) {

                await userMerchantStrategyModel.update({ enable: 0 }, {
                    where: { id: data.id, isDelete: 0, enable: 1 },
                    fields: ['enable']
                });
                return result.success('策略已禁用');
            }
            else {
                return success.failed("策略未找到", 6201537)
            }

        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    /**
     * 获取
     * @param {*} param0
     */
    async withdraw_callback_status(session) {
        try {
            let merchantSetting = await userMerchantSettingModel.findOne({
                where: {
                    user_id: session.parent_id || session.id
                }
            })

            if (merchantSetting) {
                return merchantSetting
            }
            return {};
        } catch (error) {
            console.log(error);
            return {};
        }
    }

    async update_withdraw_callback_url(session, { callback_url }) {
        try {
            let merchantSetting = await userMerchantSettingModel.findOne({
                where: {
                    user_id: session.id
                }
            })

            console.log(merchantSetting)

            if (!callback_url) {
                return false
            }

            if (merchantSetting) {
                await userMerchantSettingModel.update({
                    withdraw_callback_url: callback_url
                }, {
                        where: { user_id: session.id },
                        fields: ['withdraw_callback_url']
                    })
            }
            else {
                await userMerchantSettingModel.create({
                    user_id: session.id,
                    withdraw_callback_url: callback_url
                })
            }
            return true
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    //#endregion

};

import result from '../../tools/Result';
import Utils from '../../tools/Utils';
import sequelize from '../../lib/sequelize';
import middleware from '../../middleware';
import RedisDao from '../../lib/RedisDao';
import Crypto from '../../tools/Secret';
import GoogleService from '../../services/Security/GoogleService';
import CommonService from '../../services/Common/CommonService';
// import Sign from '../../tools/Sign';
import uuid from 'uuid';

/* eslint-disable */
const { webDBUtil, Sequelize } = sequelize;
const AdminBaseModel = webDBUtil.import('../../models/AdminPlatform/AdminBaseModel');
const RolesBaseModel = webDBUtil.import('../../models/AdminPlatform/RolesBaseModel');
const RolesAuthModel = webDBUtil.import('../../models/AdminPlatform/RolesAuthModel');
const PermissionModel = webDBUtil.import('../../models/AdminPlatform/PermissionModel');
const userInfoModel = webDBUtil.import('../../models/Users/user_info');
/* eslint-endble */
const { ValidateTools } = middleware.ValidateTools;
const validateTools = new ValidateTools();
var moment = require('moment');

const ggService = new GoogleService();
const commonService = new CommonService();

/**
 * 登陆系统
 */
module.exports = class LoginService {

    /**
     * 用户登录
     * @param {*} user
     */
    async login({ account, password }) {
        try {
            if (!account || !password) return result.paramsLack();

            // let expiry = 60 * 60 * 24 * 7;
            let expiry = 60 * 30;

            console.log('getMd5='+Utils.getMd5(password))

          //查询用户信息
            const userInfo = await userInfoModel.findOne({
                where: { mobile: account, password: Utils.getMd5(password) },
                raw: true
            });

            if (userInfo) {
                const { id, account, enable, dealerNo, mobile, email, name, is_delete, authGoogleCode, authPhoneCode, authMailCode, authFundsPassword, merchantId, isTraders, isMerchants } = userInfo;
                if (is_delete) return result.failed(`该用户已被注销!`);
                if (!enable) return result.failed(`该用户已被禁用!`);
                const sessionInfo = { id, account, enable, dealerNo, mobile, email, name, authGoogleCode, authPhoneCode, authMailCode, authFundsPassword, isTraders, isMerchants, merchantId }
                const token = validateTools.getJWT(sessionInfo, expiry);

                RedisDao.set(`session:jwt:web:${id}`, JSON.stringify(sessionInfo), expiry)
                RedisDao.set(`session:jwt:web:token:${id}`, token, expiry)
                //超级管理员
                return result.success(null, { token, token_type: "Bearer", expires_in: expiry });
            } else {
                return result.failed(`用户名或密码错误`);
            }
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    async logout({ id }) {
        try {
            RedisDao.del(`session:jwt:web:${id}`)
            RedisDao.del(`session:jwt:web:token:${id}`)
            return result.success("注销成功", 1);
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    async loginV2_step_1({ country_code, account, password }) {
        try {
            console.log('getMd5='+Utils.getMd5(password))

            if (!account || !password) return result.paramsLack();

            let queryWhere = {
                area_code: "+86",
                [Sequelize.Op.or]: [
                    { mobile: account }
                    , { email: account }
                ], password: Utils.getMd5(password),
                isDelete: 0
            }

            if (country_code && country_code != "+86") {
                queryWhere = {
                    area_code: country_code,
                    mobile: account,
                    password: Utils.getMd5(password),
                    isDelete: 0
                }
            }

            //查询用户信息
            let userInfo = await userInfoModel.findOne({
                where: queryWhere,
                raw: true,
                attributes: ["id",
                    "parent_id",
                    "mobile",
                    "email",
                    "avatar",
                    "name",
                    "area",
                    "area_code",
                    "source",
                    "created_time",
                    "updated_time",
                    "enable",
                    "isDelete",
                    "version",
                    "authGoogleCode",
                    "authPhoneCode",
                    "authMailCode",
                    "authFundsPassword",
                    "authPicCode",
                    "tradersNo",
                    "tradersName",
                    "tradersStatus",
                    "tradersMarginValue",
                    "country",
                    "identity_no"]
            });

            if (userInfo) {
                let idlength = userInfo.identity_no.length
                if (idlength.length > 8) {
                    userInfo.identity_no = `${userInfo.identity_no.substr(0, 4)}*****${userInfo.identity_no.substr(idlength - 4, 4)}`
                }
                const { id, mobile, enable, dealerNo, email, name, is_delete, authGoogleCode, authPhoneCode, authMailCode, authFundsPassword, merchantId, isTraders, isMerchants, country, identity_no } = userInfo;
                if (is_delete) return result.failed(`该用户已被注销!`);
                if (!enable) return result.failed(`该用户已被禁用!`);

                // const sessionInfo = { id, mobile, enable, dealerNo, email, name, authGoogleCode, authPhoneCode, authMailCode, authFundsPassword, merchantId, isTraders, isMerchants }


                let googleStatus = await ggService.status({ id: userInfo.id });
                let codeType = 'Google'
                let sendSms = false
                if (googleStatus.code == 0 && googleStatus.data == 0) {
                    let code = Math.random().toString().slice(-6)
                    codeType = 'Phone'
                    var cacheKey = `session_verify:phoneCode:_${userInfo.id}`
                    // if (!await RedisDao.exists(cacheKey)) {
                    await commonService.sendSMS({ phone: userInfo.mobile, code: code, country_code })
                    RedisDao.set(cacheKey, Crypto.encryptByDES(code), 600)
                    sendSms = true
                    // }
                }

                userInfo.codeType = codeType

                let cacheSessionKey = `${uuid.v4()}${uuid.v4()}`.replace(/-/g, '')
                RedisDao.set(`session:prelogin:${cacheSessionKey}`, JSON.stringify(userInfo), 60 * 10)

                return result.success("Token created.", { token: cacheSessionKey, codeType: codeType, sendSms });
            }
            else {
                return result.failed("用户不存在或密码错误", 40210);
            }

        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    async loginV2_step_end({ token }, isMobile = false) {
        let session = await RedisDao.get(`session:prelogin:${token}`)

        if (session) {
            session = JSON.parse(session)
            delete session.codeType

            session.wechat = "four-usdt01"
            let token = ""
            let expiry = 60 * 30;

            if (isMobile) {
                expiry = 60 * 60 * 24 * 7;
                token = validateTools.getJWT(session, expiry);
                RedisDao.set(`session:jwt:web:${session.id}`, JSON.stringify(session), expiry)
                RedisDao.set(`session:jwt:web:token:${session.id}`, token, expiry)

                console.log(`[交易员]设置上线 - ${JSON.stringify(await commonService.setTradersOnLine(session.id))}`)
            }
            else {
                token = validateTools.getJWT(session, expiry);
            }

            return result.success(null, { token, token_type: "Bearer", expires_in: expiry, userInfo: session });
        }
        else {
            return result.failed("会话已失效")
        }
    }

    /**
     * 用户登录
     * @param {*} user
     */
    async loginMobile({ account, password }) {
        try {
            if (!account || !password) return result.paramsLack();

            //查询用户信息
            const userInfo = await userInfoModel.findOne({
                where: { mobile: account, password: Utils.getMd5(password) },
                raw: true
            });

            if (userInfo) {
                const { id, mobile, enable, dealerNo, email, name, is_delete, authGoogleCode, authPhoneCode, authMailCode, authFundsPassword, merchantId, isTraders, isMerchants, country, identity_no } = userInfo;
                if (is_delete) return result.failed(`该用户已被注销!`);
                if (!enable) return result.failed(`该用户已被禁用!`);
                const sessionInfo = { id, mobile, enable, dealerNo, email, name, authGoogleCode, authPhoneCode, authMailCode, authFundsPassword, merchantId, isTraders, isMerchants }
                const token = validateTools.getJWT(sessionInfo, 3600 * 24 * 7);

                if (userInfo.identity_no && userInfo.identity_no.length > 6) {
                    userInfo.identity_no = userInfo.identity_no.substr(0, 2) + "*****" + userInfo.identity_no.substr(userInfo.identity_no.length - 2, 2)
                }

                userInfo.created_time = moment(userInfo.created_time).format('YYYY-MM-DD hh:mm:ss');
                userInfo.updated_time = moment(userInfo.updated_time).format('YYYY-MM-DD hh:mm:ss');

                userInfo.wechat = "four-usdt01"

                RedisDao.set(`session:jwt:web:${id}`, JSON.stringify(sessionInfo), 60 * 60 * 24 * 7)
                RedisDao.set(`session:jwt:web:token:${id}`, token, 60 * 60 * 24 * 7)

                if (userInfo.tradersStatus == 3) {
                    console.log(`[交易员]设置上线 - ${JSON.stringify(await commonService.setTradersOnLine(userInfo.id))}`)
                }
                //超级管理员
                return result.success(null, { token, token_type: "Bearer", expires_in: 3600 * 24 * 7, userInfo });
            } else {
                return result.failed(`用户名或密码错误`);
            }
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }

    /**
     * 用户登录
     * @param {*} user
     */
    async aminLogin({ account, password, code }, { imgValidateData }) {
        try {
            if (!account || !password || !code || !imgValidateData) return result.paramsLack();
            // console.log(code, imgValidateData);
            //校验验证码
            if (String(code).toLowerCase() !== String(imgValidateData).toLowerCase()) {
                return result.failed(`验证码错误!`);
            }

            //查询用户信息
            const userInfo = await AdminBaseModel.findOne({
                where: { account, password: Utils.getMd5(password) },
                raw: true
            });

            if (userInfo) {
                const { adminId, adminName, account, isAdmin, isDelete, status, roleId } = userInfo;
                if (isDelete) return result.failed(`该用户已被注销!`);
                if (status) return result.failed(`该用户已被禁用!`);
                const token = validateTools.getJWT({ adminId, adminName, account, isAdmin, roleId }, 3600 * 24);
                //超级管理员
                if (isAdmin) return result.success(null, { token, userInfo: { adminId, adminName, account, isAdmin, roleId, roleName: '超级管理员' } });
                //普通人员登录
                if (roleId) {
                    //获取角色信息
                    const { roleName } = await RolesBaseModel.findOne({ where: { roleId, isDelete: null } });
                    //关联中间表/权限表
                    RolesAuthModel.belongsTo(PermissionModel, { foreignKey: 'permissionId' });
                    //获取菜单权限
                    const list = await RolesAuthModel.findAll({
                        where: { roleId },
                        include: [{
                            where: { isDelete: null },
                            model: PermissionModel,
                            attributes: []
                        }],
                        attributes: [
                            Sequelize.col('permission.permissionId'),
                            Sequelize.col('permission.permissionName'),
                            Sequelize.col('permission.permissionType'),
                            Sequelize.col('permission.permissionValue'),
                            Sequelize.col('permission.parentId'),
                            Sequelize.col('permission.path'),
                            Sequelize.col('permission.component'),
                            Sequelize.col('permission.meta')
                        ],
                        order: [
                            [Sequelize.col('permission.permissionValue'), 'ASC']
                        ],
                        raw: true
                    });

                    //处理Meta
                    let menus = list.map(item => {
                        item.Meta = item.Meta ? JSON.parse(item.meta) : null;
                        return item;
                    });

                    return result.success(null, { token, userInfo: { adminId, adminName, account, isAdmin, roleId, roleName }, menus });
                }
                return result.failed(`你暂无权限登录系统,请联系管理员`);
            } else {
                return result.failed(`用户名或密码错误`);
            }
        } catch (error) {
            console.log(error);
            return result.failed();
        }
    }
};

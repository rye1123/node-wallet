// 微服务配置
export const MicroService = {
    API_merchant_type: 'http://',
    API_merchant_host: '47.110.139.56', //:10101/api/v1.1/order/merchant/page
    API_merchant_port: '10101', // API服务器监听的端口号
    API_traders_type: 'http://',
    API_traders_host: '192.168.1.134', //:10101/api/v1.1/order/merchant/page
    API_traders_port: '8081', // API服务器监听的端口号
    API_order_type: 'http://',
    API_order_host: '47.75.145.179', //:10101/api/v1.1/order/merchant/page
    API_order_port: '8072', // API服务器监听的端口号
};

export const ThirdPartyService = {
    KuangShi_API_URL: "https://openapi.faceid.com/",
    API_Ks_Sign_type: "http://",
    API_Ks_Sign_host: "localhost:7001",
    API_Ks_Return_url: "http://47.75.145.179:8072/buy/",
    API_Ks_Notify_url: "http://47.110.139.56:8072/v1/_ks/notify",
    API_Ks_FaceId_url: "https://openapi.faceid.com/lite/v1/do/"
}

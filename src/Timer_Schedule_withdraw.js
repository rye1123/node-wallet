
var request = require("async-request");
import Sign from './tools/Sign';
import sequelize from './lib/sequelize';
import { MicroService } from './config.service';
import { add, sub, fool } from "./lib/operation"
import AmountService from './services/Account/AmountService';
const amountService = new AmountService()
const schedule = require('node-schedule');
const isDev = !(process.env.BUILD_TYPE === 'prod');
const { webDBUtil, Sequelize } = sequelize;
const withdrawMerchantRecordDAO = webDBUtil.import('./models/Wallet/wlt_withdraw_record_merchants');
const userMerchantSettingDAO = webDBUtil.import('./models/Users/user_merchant_setting');
const walletWithdrawDAO = webDBUtil.import('./models/Wallet/wlt_withdraw_record');
const withdrawCallbackRecordDAO = webDBUtil.import('./models/Wallet/wlt_withdraw_merchants_callback_record');
const coboCoinBalanceDAO = webDBUtil.import('./models/Wallet/wlt_cobo_symbol_balance');
const https = require("https");
const util = require('util')
const url = require('url')
var requestPromise = require('request-promise');
const rpn = require('request-promise-native');
const coboWallet = require("./tools/coboWallet-client")

function log(txt) {
    console.log(`${new Date().toLocaleString()}\t${txt}`);
}


const API_WITHDRAW_CALLBACK_URL = `${MicroService.API_merchant_type}${MicroService.API_merchant_host}:${MicroService.API_merchant_port}/api/v1.1/withdraw/callback`


log(`当前运行环境 - ${isDev ? "开发" : "生产"} - ${process.env.BUILD_TYPE}`)

async function useRequestPromiseNative(_url, data) {
    // console.log(_url)
    let options = {
        method: 'POST',
        uri: _url,
        body: data,
        json: true // 这个看你的参数而定
    };
    try {
        let rpnbody = await rpn(options);
        // console.log("rpnbody", JSON.stringify(rpnbody));
        console.log(rpnbody.statusCode)
        return rpnbody.response.body
    }
    catch (err) {
        // console.log(err)
        return err.response.body || ""
    }
}

async function postData(_url, data) {
    return new Promise(function (resolve, reject) {
        try {
            var post_option = {};
            post_option.url = _url
            post_option.method = 'POST';
            // post_option.port = 443;
            var post_data = JSON.stringify(data)
            post_option.headers = {
                'Content-Type': 'application/json',
                'Content-Length': post_data.length,
            };

            log(JSON.stringify(post_option))

            var post_req = https.request(post_option, function (res) {
                res.on('data', function (buffer) {
                    resolve(buffer.toString());
                });
            })
            post_req.write(post_data);
            post_req.end();
        }
        catch (err) {
            reject(err)
        }
    })
}

const scheduleCronstyle = () => {

    var rule1 = new schedule.RecurrenceRule();
    let timerArr = [];
    for (var i = 1; i < 60; i++) {
        timerArr.push(i)
    }

    rule1.minute = timerArr;
    schedule.scheduleJob(rule1, async function () {
        startThread()
    })
}

const balanceSchedule = () => {
    console.log(`钱包余额查询服务(每10分钟执行一次)`)
    var rule1 = new schedule.RecurrenceRule();
    let timerArr = [];
    for (var i = 1; i < 60; i++) {
        if (i % 10 == 0) {
            timerArr.push(i)
        }
    }

    rule1.minute = timerArr;
    schedule.scheduleJob(rule1, async function () {
        startThread_Balance()
    })
}

async function startThread_Balance() {
    console.log(`处理余额更新 - ${new Date().toLocaleString()}`)
    let coboResult = await coboWallet.cobo_org_info()

    if (coboResult && coboResult.success) {
        coboResult.result.assets.map(async item => {
            let existsCount = await coboCoinBalanceDAO.count({
                where: {
                    coin: item.coin,
                    display_code: item.display_code,
                    decimal: item.decimal
                }
            })
            if (existsCount > 0) {
                await coboCoinBalanceDAO.update(
                    {
                        balance: item.balance,
                        abs_balance: item.abs_balance,
                        fee_coin: item.fee_coin,
                        require_memo: item.require_memo,
                        confirming_threshold: item.confirming_threshold,
                        dust_threshold: item.dust_threshold
                    },
                    {
                        where: {
                            coin: item.coin,
                            display_code: item.display_code,
                            decimal: item.decimal
                        },
                        fields: ["balance", "abs_balance", "fee_coin", "require_memo", "confirming_threshold", "dust_threshold"]
                    })
                console.log(`${item.coin} - 余额信息更新完成`)
            }
            else {
                await coboCoinBalanceDAO.create(item)
                console.log(`${item.coin} - 余额信息创建完成`)
            }
        })
    }
    // console.log(coboResult)
}

async function startThread() {
    console.log(`处理商户提现请求 - ${new Date().toLocaleString()}`)
    let _createtime = Math.floor(Date.now() / 1000) - (3600 * 3)

    let withdrawRecord = await withdrawMerchantRecordDAO.findAll({
        where: {
            enable: 1,
            is_delete: 0,
            status: 0
        }
    })

    await withdrawRecord.map(async item => {
        let merchantCfg = await userMerchantSettingDAO.findOne({
            where: {
                user_id: item.user_id,
                isDelete: 0
            }
        })

        if (merchantCfg && merchantCfg.withdraw_callback_url) {


            if (!(merchantCfg.withdraw_callback_url.indexOf("http://") == 0 || merchantCfg.withdraw_callback_url.indexOf("https://") == 0)) {
                log(`回调地址错误 - [${merchantCfg.withdraw_callback_url}]`)
            }
            else {

                let decimalNum = 8
                let fee = 5


                if (item.symbol == "ETH_USDT") {
                    fee = 3
                }


                let request_count = await withdrawCallbackRecordDAO.count({
                    where: {
                        order_id: item.id,
                        call_url: merchantCfg.withdraw_callback_url
                    }
                })

                if (request_count > 60) {

                    await withdrawMerchantRecordDAO.update(
                        { enable: 0, status: -1 },
                        {
                            where: {
                                id: item.id
                            },
                            fields: ["enable", "status"]
                        }
                    )

                    log(`该记录已经通知过60次，终止该通知 - ${item.id}`)
                }

                // let calldata = {
                //     "coin": item.symbol,
                //     "display_code": "USDT",
                //     "decimal": decimalNum,
                //     "address": item.address,
                //     "memo": "",
                //     "side": "withdraw",
                //     "amount": parseFloat(item.value),
                //     "request_id": item.request_id,
                //     "created_time": item.created_time,
                //     "fee_amount": fee
                // }

                let chain = ""
                let currency = item.symbol

                if (item.symbol == "BTC_USDT") {
                    currency = "USDT"
                    chain = "OMNI"
                }
                if (item.symbol == "ETH_USDT") {
                    currency = "USDT"
                    chain = "ERC20"
                    decimalNum = 6
                }

                let calldata = {
                    "callback_url": merchantCfg.withdraw_callback_url,
                    "address": item.address,
                    "chain": chain,
                    "count": parseFloat(item.value),
                    "created_time": item.created_time,
                    "currency": currency,
                    "decimal": 0,
                    "fee": fee,
                    "memo": "",
                    "merchant_withdraw_no": item.request_id,
                    "order_no": item.order_no,
                    "side": "withdraw"
                }

                log(`CallBack data body`)
                log(JSON.stringify(calldata))

                let feedata = ""

                // if (merchantCfg.withdraw_callback_url.indexOf("https://") == 0) {
                //     feedata = await useRequestPromiseNative(merchantCfg.withdraw_callback_url, calldata)
                // }
                // else {

                // feedata = await request(merchantCfg.withdraw_callback_url, {
                // API_WITHDRAW_CALLBACK_URL
                //"http://127.0.0.1:3001/v1/login/loginV2S1"
                var options = {
                    method: 'post',
                    uri: API_WITHDRAW_CALLBACK_URL,
                    headers: {
                        'User-Agent': 'Queue_schedule_withdraw'
                    },
                    body: calldata,
                    json: true
                };
                feedata = await requestPromise(options)
                // feedata = await request("http://localhost:3001/v1/login", {
                //     method: "POST",
                //     headers: {
                //         'content-type': 'application/json'
                //     },
                //     json: calldata
                // })
                // }

                let callbackData = {
                    order_id: item.id,
                    call_url: merchantCfg.withdraw_callback_url,
                }
                if (feedata) {
                    feedata = JSON.stringify(feedata)
                }
                console.log(feedata)

                if (feedata && feedata.indexOf("SUCCESS") > -1) {
                    console.log(`回调通过`)
                    await withdrawMerchantRecordDAO.update(
                        { status: 1 },
                        {
                            where: {
                                id: item.id
                            },
                            fields: ["status"]
                        }
                    )
                    callbackData.res_data = feedata
                    callbackData.status = 1

                    let withdrawIsExists = await walletWithdrawDAO.count({
                        where: {
                            no_id: item.order_no,
                            is_delete: 0,
                            enable: 1
                        }
                    })

                    let amountInfo = await amountService.amountBase(item.user_id, item.created_time)

                    if (!withdrawIsExists) {
                        let real_withdraw_record = {
                            no_id: item.order_no,
                            userid: item.user_id,
                            address: item.address,
                            symbol: item.symbol,
                            value: `${fool(item.value, decimalNum)}`,
                            enable: 1,
                            status: 0
                        }
                        console.log(real_withdraw_record)

                        real_withdraw_record.sign = await Sign.getRSASign(JSON.stringify(real_withdraw_record), 'withdraw')
                        if (item.value <= 20000) {
                            //所有的
                            real_withdraw_record.status = 11
                        }
                        await walletWithdrawDAO.create(real_withdraw_record)
                    }
                    else {
                        console.log("提现记录已存在")
                    }
                }
                else {
                    // console.log(`feedata`)
                    // console.log(feedata)
                    callbackData.res_data = feedata.body
                    callbackData.status = -1
                }
                await withdrawCallbackRecordDAO.create(callbackData)
            }
        }
        else {
            log(`回调地址错误或不存在`)
        }

    })


    console.log(withdrawRecord.length)
}

startThread()

scheduleCronstyle();

startThread_Balance()

balanceSchedule()

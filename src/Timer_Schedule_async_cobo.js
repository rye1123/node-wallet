var request = require("async-request");
// import Sign from './tools/Sign';
import sequelize from "./lib/sequelize";
import { cobo_transaction_history } from "./tools/coboWallet-client";
const schedule = require("node-schedule");
const isDev = !(process.env.BUILD_TYPE === "prod");

const { webDBUtil } = sequelize;

const coboSendRecordDAO = webDBUtil.import('./models/Wallet/wlt_cobo_send_record');

function log(txt) {
    console.log(`${new Date().toLocaleString()}\t${txt}`);
}

function transformTime(timestamp = +new Date()) {
    if (timestamp) {
        var time = new Date(timestamp);
        var y = time.getFullYear();
        var M = time.getMonth() + 1;
        var d = time.getDate();
        var h = time.getHours();
        var m = time.getMinutes();
        var s = time.getSeconds();
        return y + '-' + addZero(M) + '-' + addZero(d) + ' ' + addZero(h) + ':' + addZero(m) + ':' + addZero(s);
    } else {
        return '';
    }
}

function addZero(m) {
    return m < 10 ? '0' + m : m;
}

log(`当前运行环境 - ${isDev ? "开发" : "生产"} - ${process.env.BUILD_TYPE}`)

const scheduleCronAsyncCobo = () => {

    var rule1 = new schedule.RecurrenceRule();
    let timerArr = [];
    for (var i = 1; i < 60; i++) {
        timerArr.push(i)
    }

    rule1.minute = timerArr;
    schedule.scheduleJob(rule1, async function () {
        startAsyncCobo()
    })
}

async function startAsyncCobo() {
    let maxCoboId_Script = " select cobo_id as max_cobo_id from wlt_cobo_send_record order by id desc limit 1 "

    let maxCoboId = await webDBUtil.query(maxCoboId_Script, { raw: true, type: sequelize.QueryTypes.SELECT })

    console.log(maxCoboId)

    if (maxCoboId.length > 0 && maxCoboId[0].max_cobo_id) {
        maxCoboId = maxCoboId[0].max_cobo_id
    }
    else {
        maxCoboId = 0
    }
    log(`maxCoboId - ${maxCoboId}`)

    let coboHistory = await cobo_transaction_history(maxCoboId)

    if (coboHistory.success == true && coboHistory.result.length > 0) {
        coboHistory.result.map(async _item => {
            let item = _item
            await coboSendRecordDAO.create({
                cobo_id: item.id,
                request_id: item.request_id,
                tx_id: item.txid,
                from_address: item.source_address,
                to_address: item.address,
                side: item.side,
                coin: item.coin,
                amount: item.abs_amount,
                cobo_fee: item.abs_cobo_fee,
                tx_fee_coin: item.fee_coin,
                tx_fee: item.fee_amount,
                status: item.status,
                time: transformTime(item.created_time)
            })
            log(item.id)
            console.log(item)
        })
    }

    // console.log(coboHistory)

}


scheduleCronAsyncCobo()


startAsyncCobo()

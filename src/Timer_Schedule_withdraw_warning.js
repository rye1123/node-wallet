
var request = require("async-request");
import Sign from './tools/Sign';
import sequelize from './lib/sequelize';
import { MicroService } from './config.service';
import { add, sub } from "./lib/operation"
import AmountService from './services/Account/AmountService';
var sendemail = require('sendemail')
var emailHelper = sendemail.email;
const amountService = new AmountService()
const schedule = require('node-schedule');
const isDev = !(process.env.BUILD_TYPE === 'prod');
const { webDBUtil, Sequelize } = sequelize;
const withdrawMerchantRecordDAO = webDBUtil.import('./models/Wallet/wlt_withdraw_record_merchants');
const userMerchantSettingDAO = webDBUtil.import('./models/Users/user_merchant_setting');
const walletWithdrawDAO = webDBUtil.import('./models/Wallet/wlt_withdraw_record');
const withdrawCallbackRecordDAO = webDBUtil.import('./models/Wallet/wlt_withdraw_merchants_callback_record');
const https = require("https");
const util = require('util')
const url = require('url')
var requestPromise = require('request-promise');
const rpn = require('request-promise-native');

function log(txt) {
    console.log(`${new Date().toLocaleString()}\t${txt}`);
}

log(`当前运行环境 - ${isDev ? "开发" : "生产"} - ${process.env.BUILD_TYPE}`)


const scheduleCronstyle = () => {

    var rule1 = new schedule.RecurrenceRule();
    let timerArr = [];
    for (var i = 1; i < 60; i++) {
        i % 9 == 0 && timerArr.push(i)
    }

    rule1.minute = timerArr;
    schedule.scheduleJob(rule1, async function () {
        startThread()
    })
}

async function startThread() {
    let _createtime = Math.floor(Date.now() / 1000) - (3600 * 3)


    let queryScript = "select * from wlt_withdraw_record where is_internal = 0 and status > 1 and (tx is null or tx = '') and created_time < DATE_SUB(NOW(),INTERVAL 15 MINUTE);"
    let withdraw_record_list = await webDBUtil.query(queryScript, { raw: true, type: sequelize.QueryTypes.SELECT })


    if (withdraw_record_list.length > 0) {

        let notify_title = `提币异常通知 - ${process.env.SYS_ENV}`
        let notify_subject = `提现申请`
        let notify_mail_code = `数量 - ${withdraw_record_list.length}`


        // if (process.env.SYS_ENV && process.env.SYS_ENV != "development") {
        var person = {
            name: notify_title,
            email: '519303436@qq.com', // person.email can also accept an array of emails
            subject: notify_subject,
            mail_code: notify_mail_code
        }

        emailHelper('mail_vali_code', person, function (error, result) {
            console.log(' - - - - - - - - - - - - - - - - - - - - -> email sent: ');
            console.log(result);
            console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
            console.log(error)
        })

        var person1 = {
            name: notify_title,
            email: '267300745@qq.com', // person.email can also accept an array of emails
            subject: notify_subject,
            mail_code: notify_mail_code
        }

        emailHelper('mail_vali_code', person1, function (error, result) {
            console.log(' - - - - - - - - - - - - - - - - - - - - -> email sent: ');
            console.log(result);
            console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
            console.log(error)
        })

        var person2 = {
            name: notify_title,
            email: '735182700@qq.com', // person.email can also accept an array of emails
            subject: notify_subject,
            mail_code: notify_mail_code
        }

        emailHelper('mail_vali_code', person2, function (error, result) {
            console.log(' - - - - - - - - - - - - - - - - - - - - -> email sent: ');
            console.log(result);
            console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
            console.log(error)
        })

        var person3 = {
            name: notify_title,
            email: '17721304491@163.com', // person.email can also accept an array of emails
            subject: notify_subject,
            mail_code: notify_mail_code
        }

        emailHelper('mail_vali_code', person3, function (error, result) {
            console.log(' - - - - - - - - - - - - - - - - - - - - -> email sent: ');
            console.log(result);
            console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
            console.log(error)
        })
        // }
        // else {
        //     console.log(`非生产环境，不发送提币异常通知邮件`)
        // }

        console.log(`${new Date().toLocaleString()} - 15分钟没有到账的提现记录 - ${withdraw_record_list.length}`)
    }
    else {
        console.log(`${new Date().toLocaleString()} - 无异常记录 - ${withdraw_record_list.length}`)
    }


}

startThread()

scheduleCronstyle();

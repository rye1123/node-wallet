/**
 * 验证码路由处理
 */
var url = require('url');

/**
 * Returns boolean indicating whether the requestUrl matches against the paths configured.
 *
 * @param requestedUrl - url requested by user
 * @param opts - unless configuration
 * @returns {boolean}
 */
function matchesPath(requestedUrl, opts) {
    var paths = !opts.path || Array.isArray(opts.path) ?
        opts.path : [opts.path];

    if (paths) {
        return paths.some(function (p) {
            return (typeof p === 'string' && p === requestedUrl.pathname) ||
                (p instanceof RegExp && !!p.exec(requestedUrl.pathname));
        });
    }

    return false;
}

module.exports = function (options) {


    const middleware = async function valicode(ctx, next) {

        return next()
    }
    middleware.needvali = (opts) => {
        var requestedUrl = url.parse((opts.useOriginalUrl ? ctx.originalUrl : ctx.url) || '', true);

        var paths = !opts.path || Array.isArray(opts.path) ?
            opts.path : [opts.path];
        if (paths) {
            return paths.some(function (p) {
                return (typeof p === 'string' && p === requestedUrl.pathname) ||
                    (p instanceof RegExp && !!p.exec(requestedUrl.pathname));
            });
        }

        return false;
    };
    return middleware;
}
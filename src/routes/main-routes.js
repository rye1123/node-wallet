//主路由文件
import KoaRouter from 'koa-router';
import controllers from '../controllers/index.js';

//所有的API接口都以apiv1开头
const router = new KoaRouter({ prefix: '/v1' });
const {
    Users,
    Login,
    Account,
    Verify,
    Security,
    Wallet,
    Merchant,
    _Merchant,
    _PlatInfo,
    _Traders,
    _Manage,
    Admin,
    Files,
    Common,
    WebSite,
    _Api,
    Team,
    Test
} = controllers;

const routers = [{
    url: `/test`,
    routes: Test.TestController
}, {
    url: `/user`,
    routes: Users.UserController
},
{
    url: `/login`,
    routes: Login.LoginController
},
{
    url: `/security`,
    routes: Security.GoogleController
},
{
    url: `/security`,
    routes: Security.ApiController
},
{
    url: `/wallet/usdt`,
    routes: Wallet.USDTController
},
{
    url: `/account`,
    routes: Security.EmailController
},
{
    url: `/wallet`,
    routes: Wallet.AmountController
},
{
    url: `/wallet/withdraw`,
    routes: Wallet.WithdrawController
},
{
    url: `/account`,
    routes: Account.AmountController
},
{
    url: `/merchants/api_setting`,
    routes: Merchant.SettingController
},
{
    url: `/merchants/setting`,
    routes: _Merchant.SettingController
},
{
    url: `/account`,
    routes: Account.AccountController
},
{
    url: `/merchants`,
    routes: Merchant.OrderController
},
{
    url: `/merchants`,
    routes: Merchant.StatisticsController
},
{
    url: `/traders`,
    routes: _Traders.PayMethodController
},
{
    url: `/_sys/_merchants`,
    routes: _Merchant.AmountController
},
{
    url: `/_sys/_merchants`,
    routes: _Merchant.AccountController
},
{
    url: `/_sys/_platinfo`,
    routes: _PlatInfo.AccessController
},
{
    url: `/_sys/_platinfo`,
    routes: _PlatInfo.BalanceController
},
{
    url: `/_sys/_manage`,
    routes: _Manage.MerchantController
},
{
    url: `/_sys/_manage/hedging_account`,
    routes: _Manage.HedgingController
},
{
    url: `/_sys/_manage`,
    routes: _Manage.TraderController
},
{
    url: `/_sys/_platform`,
    routes: _Manage.ProfitController
},
{
    url: `/_sys/_manage/wallet`,
    routes: _Manage.WalletController
},
{
    url: `/_api/wallet`,
    routes: _Api.WalletController
},
{
    url: `/_api/withdraw_info`,
    routes: _Api.WithdrawInfoController
},
{
    url: `/verify`,
    routes: Verify.SessionController
},
{
    url: `/platform`,
    routes: Admin.PlatformController
},
{
    url: `/permission`,
    routes: Admin.PermissionController
},
{
    url: `/files`,
    routes: Files.FileController
},
{
    url: `/common`,
    routes: Common.CommonController
},
{
    url: `/cb`,
    routes: Common.CoboController
}, {
    url: '/_ks',
    routes: Common.KsController
}, {
    url: `/export`,
    routes: Common.ExportController
},
{
    url: `/web`,
    routes: WebSite.WebController
},
{
    url: `/web`,
    routes: WebSite.OrderController
},
{
    url: `/team_merchant`,
    routes: Team.ChildAccountController
},
{
    url: `/mc`,
    routes: Team.AuditorController
}
];

//挂载路由
routers.forEach(item => {
    router.use(item.url, item.routes.routes(), item.routes.allowedMethods());
});
export default router;

import sequelize from './lib/sequelize_queue';
import RedisDao from './lib/RedisDao';
import result from './tools/Result';
import Sign from './tools/Sign';
const { webDBUtil, Sequelize } = sequelize;

import AmountService from './services/Account/AmountService';
const amountService = new AmountService()
const lock_order_listDAO = webDBUtil.import('./models/Amount/lock_order_list');
const convertInfoDAO = webDBUtil.import('./models/Wallet/wlt_convert_info');
const funds_listDAO = webDBUtil.import('./models/Amount/funds_list');

import { queueWorkerConfig } from "./lib/queueUtil"
var Queue = require('bee-queue');

var queueOrderComplete = new Queue('order_complete', queueWorkerConfig);


queueOrderComplete.on('ready', async function () {
    await queueOrderComplete.process(async function (job, done) {
        let msg;
        try {
            job.reportProgress(10);

            console.log('processing complete Order job ' + job.id);

            let session = job.data.session;
            let data = job.data.data;
            let order_no = data.order_no;
            let founds = data.founds;

            //首先应当判断当前会话的用户有没有操作全新session.id
            //是交易员，或者订单所属用户，或者是后台管理员
            //**********此处暂时省略，有时间更新需要增加该判断********** */

            let queryData = {
                where: {
                    orderNo: order_no,
                    status: 0,
                    enable: true
                    // userid: session.id
                    // is_delete: 0,
                    // enable: 1
                },
                raw: true,
            };

            const orderFreezeInfo = await lock_order_listDAO.findOne(queryData);

            if (!orderFreezeInfo) {
                msg = result.failed('订单未找到', 50038);
                await RedisDao.set(`order:complete_order_result_${job.id}`, JSON.stringify(msg), 6000)
                return done(null, msg);
            }

            let convert_info_exist = await convertInfoDAO.findOne({
                where: {
                    order_no: order_no,
                    user_id: orderFreezeInfo.userid
                }
            })

            let autoSource = orderFreezeInfo.source;

            //对冲订单资金流水处理，如果后续交易员需要出平台手续费，该内容需要修改
            //因为以下对冲将所有的币都对冲掉了。交易员只需要获取USDT
            if (convert_info_exist) {
                console.log(`对冲订单`)
                let hedgingAmountInfo = await amountService.amountBase(100, convert_info_exist.symbol)

                if (hedgingAmountInfo.available <= convert_info_exist.amount) {
                    msg = result.failed(`对冲资金不足，需对冲${convert_info_exist.amount} ${convert_info_exist.symbol}，目前仅可对冲${hedgingAmountInfo.available} ${convert_info_exist.symbol}`, 500197);
                    await RedisDao.set(`order:complete_order_result_${job.id}`, JSON.stringify(msg), 6000)
                    return done(null, msg);
                }

                let merchantFee = founds.filter(item => {
                    return item.source == "MERCHANTS_FEE"
                })[0].value

                let merchantProfit = founds.filter(item => {
                    return item.source == "PROFIT"
                })[0].value

                let buyAmount = founds.filter(item => {
                    return item.source == "BUY"
                })[0].value

                // if (orderFreezeInfo.symbol != 'USDT') {
                //     founds.push({
                //         source: 'MERCHANTS_FEE',
                //         symbol: orderFreezeInfo.symbol,
                //         value: `-${sub(orderFreezeInfo.value, sub(orderFreezeInfo.value, merchantFee))}`,
                //         to_user_id: orderFreezeInfo.userid
                //     })
                // }

                founds.push({
                    source: autoSource,
                    symbol: convert_info_exist.symbol,
                    value: `-${buyAmount}`,
                    use: -1,
                    to_user_id: orderFreezeInfo.userid
                })

                //如果冻结的是USDT，并且转换的币非USDT，则该记录为商户购买非USDT的币种
                if (orderFreezeInfo.symbol == "USDT" && convert_info_exist.symbol != "USDT") {
                    console.log(`交易员出售非USDT币种订单`)
                    founds.push({
                        source: `HEDGING_GET`,
                        symbol: convert_info_exist.symbol,
                        value: `${sub(convert_info_exist.amount, merchantFee)}`,
                        use: -1,
                        to_user_id: orderFreezeInfo.userid
                    })

                    founds.push({
                        source: `HEDGING_CONVERT`,
                        symbol: convert_info_exist.symbol,
                        value: `-${sub(convert_info_exist.amount, merchantFee)}`,
                        to_user_id: 100
                    })
                }
                else {
                    console.log(`商户出售非USDT币种订单`)
                    founds.push({
                        source: `HEDGING_GET`,
                        symbol: convert_info_exist.symbol,
                        value: `${sub(convert_info_exist.amount, sub(convert_info_exist.amount, buyAmount))}`,
                        use: -1,
                        to_user_id: orderFreezeInfo.userid
                    })

                    founds.push({
                        source: `HEDGING_CONVERT`,
                        symbol: convert_info_exist.symbol,
                        value: `-${sub(convert_info_exist.amount, sub(convert_info_exist.amount, buyAmount))}`,
                        to_user_id: 100
                    })
                }

                founds.push({
                    source: `HEDGING_BUY`,
                    symbol: orderFreezeInfo.symbol,
                    value: `${sub(orderFreezeInfo.value, merchantFee)}`,
                    to_user_id: 100
                })

                founds.push({
                    source: `HEDGING_SELL`,
                    symbol: orderFreezeInfo.symbol,
                    value: `-${orderFreezeInfo.value}`,
                    to_user_id: orderFreezeInfo.userid
                })

            }
            else {
                let orderItem = {
                    source: autoSource,
                    symbol: orderFreezeInfo.symbol,
                    value: `-${Math.abs(orderFreezeInfo.value)}`,
                    to_user_id: orderFreezeInfo.userid
                }

                founds.push(orderItem)
            }

            founds.reverse()

            let foundsList = founds.map(async item => {
                let amountInfo = await amountService.amountBase(item.to_user_id, item.symbol)

                //2019-09-10 16:21:49 增加使用了use字段
                let foudInfo = {
                    source: item.source,
                    symbol: item.symbol || "USDT",
                    value: item.value,
                    createUserId: session.id,
                    orderNo: order_no,
                    userid: item.to_user_id,
                    use: item.use || 0,
                    log_amount: `${add(amountInfo.sum_amount, item.value)}`
                }
                // console.log(foudInfo)
                return foudInfo
            })

            for (var i = 0; i < foundsList.length; i++) {
                foundsList[i] = await foundsList[i]
                foundsList[i].sign = await Sign.getRSASign(JSON.stringify(foundsList[i]), 'found_list')
            }

            /*

            //如果是买单，冻结的就是卖家的钱，那orderFreezeInfo.userid 就是卖家的id，那卖家的账单记录就应该是减去
            if (orderFreezeInfo.source == "BUY") {
                //org这里定为买家，所以买家的source就是BUY
                foundInfoOrg.source = "BUY"
                    //卖家的source就是sell
                foundInfoNew.source = "SELL"
                    //把冻结的金额给买家org
                foundInfoOrg.value = orderFreezeInfo.value
                    //账单也是标记为传进来的用户id，严谨一些，这里需要从订单记录里去找传进来的用户id，记得有时间更新一下！
                foundInfoOrg.userid = to_user_id

                //记住，这里是减去，给卖家减去
                foundInfoNew.userid = orderFreezeInfo.userid
                    //卖家减去
                foundInfoNew.value = "-" + orderFreezeInfo.value
            }
            //严谨一些，多加一个SELL的判断，已防止误操作其他类别的订单。这里就是反向操作了
            else if (orderFreezeInfo.source == "SELL") {
                foundInfoOrg.value = "-" + orderFreezeInfo.value
                foundInfoOrg.userid = orderFreezeInfo.userid

                foundInfoNew.userid = to_user_id
                foundInfoNew.value = orderFreezeInfo.value
            } else {
                return result.failed("订单不属于该业务", 50039);
            }
            */

            let createResult = await funds_listDAO.bulkCreate(foundsList.filter(item => { return item.value != 0 }));

            // foundInfoOrg.sign = await Sign.getRSASign(JSON.stringify(foundInfoOrg), 'found_list')
            // foundInfoNew.sign = await Sign.getRSASign(JSON.stringify(foundInfoNew), 'found_list')
            // await funds_listDAO.create(foundInfoOrg)
            // await funds_listDAO.create(foundInfoNew)
            await lock_order_listDAO.update({ enable: false, status: 1 }, {
                where: { enable: true, orderNo: order_no },
                fields: ['enable', 'status']
            });


            msg = result.success("订单完成");
            await RedisDao.set(`order:complete_order_result_${job.id}`, JSON.stringify(msg), 6000)
            return done(null, msg);
        } catch (error) {
            console.log(error)
            await RedisDao.set(`order:complete_order_result_${job.id}`, JSON.stringify(error), 6000)
            return done(null, error);
        }
    });

    console.log('processing complete order jobs...');
});

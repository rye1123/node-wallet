import sequelize from './lib/sequelize_queue';
import RedisDao from './lib/RedisDao';
import result from './tools/Result';
import Sign from './tools/Sign';
import Utils from './tools/Utils';
import uuid from "uuid"
const { webDBUtil, Sequelize } = sequelize;
import { add, sub } from "./lib/operation"

import AmountService from './services/Account/AmountService';
const amountService = new AmountService()
const userMerchantSettingDAO = webDBUtil.import('./models/Users/user_merchant_setting');

import { queueWorkerConfig } from "./lib/queueUtil"
var Queue = require('bee-queue');

var queue = new Queue('lock', queueWorkerConfig);


const withdrawMerchantRecordDAO = webDBUtil.import('./models/Wallet/wlt_withdraw_record_merchants');
const userMerchantStrategySettingDAO = webDBUtil.import('./models/Users/user_merchant_strategy_withdraw');
const withdrawWhiteListDAO = webDBUtil.import('./models/Users/user_merchant_withdraw_whitelist');
const apiInfoDAO = webDBUtil.import('./models/Security/merchant_api_info');

const funds_listDAO = webDBUtil.import('./models/Amount/funds_list');
var queue_withdraw_merchants = new Queue('withdraw_merchants', queueWorkerConfig)

queue_withdraw_merchants.on('ready', async function () {
    await queue_withdraw_merchants.process(async function (job, done) {
        console.log("start withdraw_merchants_trans")
        let msg;
        try {
            console.log('processing job ' + job.id);
            console.log(job.data)
            let data = job.data;

            if (data.amount < 20) {
                msg = result.failed(`最低提现数量为20`, 4032311)
                RedisDao.set(`withdraw_merchants_trans:lock_result_${job.id}`, JSON.stringify(msg), 6000)
                return done(null, msg);
            }

            let withdraw_fee = 5;
            //判断地址格式是否正确
            if (data.symbol == "USDT" || data.symbol == "BTC_USDT") {

                if (data.chain == "OMNI" || data.symbol == "BTC_USDT") {
                    if (!Utils.verifyAddress("BTC_USDT",data.address)) {
                        msg = result.failed(`${data.chain} - 地址格式错误`, 4031422)
                        RedisDao.set(`withdraw_merchants_trans:lock_result_${job.id}`, JSON.stringify(msg), 6000)
                        return done(null, msg);
                    }
                    data.symbol = "BTC_USDT"
                }

                if (data.chain == "ERC20" || data.symbol == "ETH_USDT") {
                    if (!Utils.verifyAddress("ETH_USDT",data.address)) {
                        msg = result.failed(`${data.chain} - 地址格式错误`, 4031421)
                        RedisDao.set(`withdraw_merchants_trans:lock_result_${job.id}`, JSON.stringify(msg), 6000)
                        return done(null, msg);
                    }
                    else {
                        //erc20，手续费为3
                        withdraw_fee = 3
                    }
                    data.symbol = "ETH_USDT"
                }
            }

            //获取API信息
            let apiInfo = await apiInfoDAO.findOne({
                where: {
                    api_key: data.api_key,
                    enable: 1,
                    wallet_withdraw: 1,
                    is_delete: 0
                }
            })

            if (apiInfo.bind_ip.indexOf('0.0.0.0') < 0 && apiInfo.bind_ip.indexOf(data.request_ip) < 0) {
                msg = result.failed("Ip address Error", 400411)
                RedisDao.set(`withdraw_merchants_trans:lock_result_${job.id}`, JSON.stringify(msg), 6000)
                return done(null, msg);
            }

            //没有找到提币API
            if (!apiInfo) {
                msg = result.failed("Api错误", 4003811)
                RedisDao.set(`withdraw_merchants_trans:lock_result_${job.id}`, JSON.stringify(msg), 6000)
                return done(null, msg);
            }


            let merchantCfg = await userMerchantSettingDAO.findOne({
                where: {
                    user_id: apiInfo.user_id,
                    isDelete: 0
                }
            })

            if (!merchantCfg) {
                msg = result.failed(`商户配置异常`, 4031445)
                RedisDao.set(`withdraw_merchants_trans:lock_result_${job.id}`, JSON.stringify(msg), 6000)
                return done(null, msg);
            }

            if (!(merchantCfg.withdraw_callback_url.indexOf("http://") == 0 || merchantCfg.withdraw_callback_url.indexOf("https://") == 0)) {
                msg = result.failed(`没有设置提现回调地址`, 4031446)
                RedisDao.set(`withdraw_merchants_trans:lock_result_${job.id}`, JSON.stringify(msg), 6000)
                return done(null, msg);
            }


            let whiteListCount = await withdrawWhiteListDAO.count({
                where: {
                    user_id: apiInfo.user_id,
                    isDelete: 0,
                    enable: 1
                }
            })

            console.log(` 白名单数量 - ${whiteListCount}`)

            if (whiteListCount > 0) {
                let whiteListAddressCount = await withdrawWhiteListDAO.count({
                    where: {
                        user_id: apiInfo.user_id,
                        address: data.address,
                        enable: 1,
                        isDelete: 0,
                        symbol: data.symbol
                    }
                })
                console.log(` 匹配白名单数量 - ${whiteListAddressCount}`)
                if (whiteListAddressCount < 1) {
                    msg = result.failed(`提现地址非法`, 4031441)
                    RedisDao.set(`withdraw_merchants_trans:lock_result_${job.id}`, JSON.stringify(msg), 6000)
                    return done(null, msg);
                }
            }

            let withdrawRecord = await withdrawMerchantRecordDAO.findOne({
                where: {
                    request_id: data.request_id,
                    user_id: apiInfo.user_id,
                    is_delete: 0,
                    enable: 1
                }
            })

            if (withdrawRecord) {
                msg = result.failed("提现已申请，请勿重复提交", 4003810)
                RedisDao.set(`withdraw_merchants_trans:lock_result_${job.id}`, JSON.stringify(msg), 6000)
                return done(null, msg);
            }


            //获取余额信息
            let amountInfo = await amountService.amountBase(apiInfo.user_id)
            console.log(`可用余额：${amountInfo.available}`)
            if (amountInfo.available < data.amount) {
                msg = result.failed("余额不足", 4003812)
                RedisDao.set(`withdraw_merchants_trans:lock_result_${job.id}`, JSON.stringify(msg), 6000)
                return done(null, msg);
            }

            //获取策略信息
            let merchantStrategyCfg = await userMerchantStrategySettingDAO.findOne({
                where: {
                    user_id: apiInfo.user_id,
                    enable: 1,
                    symbol: data.symbol,
                    isDelete: 0
                }
            })

            // console.log("策略")
            // console.log(merchantStrategyCfg)

            if (merchantStrategyCfg) {
                var curData = new Date();


                let withdrawInfo = {
                    user_id: apiInfo.user_id,
                    request_id: data.request_id,
                    address: data.address,
                    value: sub(data.amount, withdraw_fee),
                    fee: withdraw_fee,
                    symbol: data.symbol,
                    logBalance: `${amountInfo.sum_amount - data.amount}`,
                    order_no: uuid.v4().replace(/-/g, ''),
                    year: curData.getFullYear(),
                    month: curData.getMonth() + 1,
                    day: curData.getDate(),
                    hour: curData.getHours()
                }
                data.order_no = withdrawInfo.order_no

                withdrawInfo.sign = await Sign.getRSASign(JSON.stringify(withdrawInfo), 'withdraw_merchants')

                console.log(`merchantStrategyCfg.limit_per < withdrawInfo.value`)
                console.log(`${merchantStrategyCfg.limit_per} < ${withdrawInfo.value} = ${parseFloat(merchantStrategyCfg.limit_per) < parseFloat(withdrawInfo.value)}`)
                if (parseFloat(merchantStrategyCfg.limit_per) < parseFloat(withdrawInfo.value)) {
                    msg = result.failed("每笔提现金额超出设置", 4003835)
                    RedisDao.set(`withdraw_merchants_trans:lock_result_${job.id}`, JSON.stringify(msg), 6000)
                    return done(null, msg);
                }

                let day_limit_script = 'select IFNULL(sum(value),0) as sum_amount from wlt_withdraw_record_merchants where symbol= ? and user_id = ? and `year` = ? and `month` = ? and `day` = ? '
                let hour_limit_script = ` ${day_limit_script}  and hour = ? `


                let day_limit_sum = await webDBUtil.query(day_limit_script, { raw: true, replacements: [data.symbol, apiInfo.user_id, withdrawInfo.year, withdrawInfo.month, withdrawInfo.day], type: sequelize.QueryTypes.SELECT })
                day_limit_sum = day_limit_sum[0]

                console.log(add(day_limit_sum.sum_amount, withdrawInfo.value))
                console.log(merchantStrategyCfg.limit_day)
                console.log(add(day_limit_sum.sum_amount, withdrawInfo.value) < merchantStrategyCfg.limit_day)
                if (add(day_limit_sum.sum_amount, withdrawInfo.value) <= merchantStrategyCfg.limit_day) {
                    console.log(` 满足日限额 - ${day_limit_sum.sum_amount}/${merchantStrategyCfg.limit_day}`)


                    let hour_limit_sum = await webDBUtil.query(hour_limit_script, { raw: true, replacements: [data.symbol, apiInfo.user_id, withdrawInfo.year, withdrawInfo.month, withdrawInfo.day, withdrawInfo.hour], type: sequelize.QueryTypes.SELECT })
                    hour_limit_sum = hour_limit_sum[0]

                    if (add(hour_limit_sum.sum_amount, withdrawInfo.value) <= merchantStrategyCfg.limit_hour) {
                        console.log(` 满足小时限额 - ${hour_limit_sum.sum_amount}/${merchantStrategyCfg.limit_hour}`)
                        withdrawInfo.strategy_limit_per = merchantStrategyCfg.limit_per
                        withdrawInfo.strategy_limit_hour = merchantStrategyCfg.limit_hour
                        withdrawInfo.strategy_limit_day = merchantStrategyCfg.limit_day
                        withdrawInfo.strategy_limit_approval = merchantStrategyCfg.limit_approval


                        withdrawInfo.strategy_sign = await Sign.getRSASign(JSON.stringify(withdrawInfo), 'withdraw_merchants_strategy')


                        //如果需要审核，就将该提现记录改为不可用。
                        if (withdrawInfo.strategy_limit_approval > 0) {
                            withdrawInfo.enable = 0
                        }

                        console.log("商户api调用提币_策略")
                        console.log(withdrawInfo)

                        await fundsRecord(data, withdrawInfo, apiInfo, amountInfo, withdraw_fee)

                        await withdrawMerchantRecordDAO.create(withdrawInfo)
                        msg = result.success("申请提现成功", { order_no: withdrawInfo.order_no });
                        RedisDao.set(`withdraw_merchants_trans:lock_result_${job.id}`, JSON.stringify(msg), 6000)
                        return done(null, msg);
                    }
                    else {
                        console.log(` 不满足小时限额 - ${hour_limit_sum.sum_amount}+${data.amount}=${add(hour_limit_sum.sum_amount, data.amount)}/${merchantStrategyCfg.limit_hour}`)
                        msg = result.failed("超出每小时限额设置", 4003837)
                        RedisDao.set(`withdraw_merchants_trans:lock_result_${job.id}`, JSON.stringify(msg), 6000)
                        return done(null, msg);
                    }
                }
                else {
                    console.log(` 不满足日限额 - ${day_limit_sum.sum_amount}+${data.amount}=${add(day_limit_sum.sum_amount, data.amount)}/${merchantStrategyCfg.limit_day}`)
                    msg = result.failed("超出每日限额设置", 4003836)
                    RedisDao.set(`withdraw_merchants_trans:lock_result_${job.id}`, JSON.stringify(msg), 6000)
                    return done(null, msg);
                }
            }
            else {
                console.log("没有找到策略")
                var curData = new Date();
                if (data.amount <= 20000) {
                    //可以正常提币
                    let withdrawInfo = {
                        user_id: apiInfo.user_id,
                        request_id: data.request_id,
                        address: data.address,
                        value: `${sub(data.amount, withdraw_fee)}`,
                        fee: withdraw_fee,
                        symbol: data.symbol,
                        logBalance: `${sub(amountInfo.sum_amount, data.amount)}`,
                        order_no: uuid.v4().replace(/-/g, ''),
                        year: curData.getFullYear(),
                        month: curData.getMonth() + 1,
                        day: curData.getDate(),
                        hour: curData.getHours()
                    }
                    data.order_no = withdrawInfo.order_no


                    withdrawInfo.sign = await Sign.getRSASign(JSON.stringify(withdrawInfo), 'withdraw_merchants')

                    console.log("商户api调用提币")
                    console.log(withdrawInfo)

                    await fundsRecord(data, withdrawInfo, apiInfo, amountInfo, withdraw_fee)

                    await withdrawMerchantRecordDAO.create(withdrawInfo)
                    msg = result.success("申请提现成功", { order_no: withdrawInfo.order_no });
                    RedisDao.set(`withdraw_merchants_trans:lock_result_${job.id}`, JSON.stringify(msg), 6000)
                    return done(null, msg);
                    // console.log(createData)
                }
                else {
                    //没有找到策略以后，超过2w提币的话，不允许提币
                }

            }
        } catch (error) {
            console.log(error)
            RedisDao.set(`withdraw_merchants_trans:lock_result_${job.id}`, JSON.stringify(error), 6000)
            return done(null, error);
        }
    })
})

async function fundsRecord(data, withdrawInfo, apiInfo, amountInfo, withdraw_fee) {

    if (data.symbol == "BTC_USDT" || data.symbol == "ETH_USDT") {
        data.symbol = "USDT"
    }

    //2019年8月1日 19:19:55  增加了log_amount
    let foudInfo = {
        source: "WITHDRAW",
        symbol: data.symbol,
        value: "-" + Math.abs(sub(data.amount, withdraw_fee)),
        createUserId: apiInfo.user_id,
        orderNo: withdrawInfo.order_no,
        userid: apiInfo.user_id,
        log_amount: `${sub(amountInfo.sum_amount, sub(data.amount, withdraw_fee))}`
    }
    amountInfo.sum_amount = sub(amountInfo.sum_amount, data.amount)

    //2019年8月1日 19:19:55  增加了log_amount
    let foudInfo_fee_out = {
        source: "WITHDRAW_FEE_OUT",
        symbol: data.symbol,
        value: -withdraw_fee,
        createUserId: apiInfo.user_id,
        orderNo: withdrawInfo.order_no,
        userid: apiInfo.user_id,
        log_amount: `${sub(amountInfo.sum_amount, withdraw_fee)}`
    }
    amountInfo.sum_amount = sub(amountInfo.sum_amount, withdraw_fee)

    let systemAmountInfo = await amountService.amountBase(1)
    //2019年8月1日 19:19:55  增加了log_amount
    let foudInfo_fee_in = {
        source: "WITHDRAW_FEE_IN",
        symbol: data.symbol,
        value: withdraw_fee,
        createUserId: apiInfo.user_id,
        orderNo: withdrawInfo.order_no,
        userid: 1,
        log_amount: `${add(systemAmountInfo.sum_amount, withdraw_fee)}`
    }
    amountInfo.sum_amount = add(systemAmountInfo.sum_amount, withdraw_fee)

    foudInfo.sign = await Sign.getRSASign(JSON.stringify(foudInfo), 'withdraw')
    foudInfo_fee_out.sign = await Sign.getRSASign(JSON.stringify(foudInfo_fee_out), 'withdraw')
    foudInfo_fee_in.sign = await Sign.getRSASign(JSON.stringify(foudInfo_fee_in), 'withdraw')

    // console.log([foudInfo, foudInfo_fee_out, foudInfo_fee_in])
    await funds_listDAO.bulkCreate([foudInfo, foudInfo_fee_out, foudInfo_fee_in]);

}

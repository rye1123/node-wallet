/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('access_request', {
        id: {
            type: DataTypes.INTEGER(32),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        type: {
            type: DataTypes.STRING(20),
            allowNull: false,
            defaultValue: ''
        },
        business: {
            type: DataTypes.STRING(2000),
            allowNull: false,
            defaultValue: 'partner'
        },
        productName: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        companyName: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        companyScale: {
            type: DataTypes.STRING(255),
            allowNull: false,
            defaultValue: ''
        },
        website: {
            type: DataTypes.STRING(255),
            allowNull: false,
            defaultValue: ''
        },
        address: {
            type: DataTypes.STRING(255),
            allowNull: false,
            defaultValue: ''
        },
        id_card_no: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: ''
        },
        remark: {
            type: DataTypes.STRING(255),
            allowNull: false,
            defaultValue: ''
        },
        email: {
            type: DataTypes.STRING(64),
            allowNull: false
        },
        country: {
            type: DataTypes.STRING(64),
            allowNull: false
        },
        name: {
            type: DataTypes.STRING(64),
            allowNull: true
        },
        area_code: {
            type: DataTypes.STRING(10),
            allowNull: false,
            defaultValue: '+86'
        },
        phone: {
            type: DataTypes.STRING(32),
            allowNull: true
        },
        chat_type: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: ''
        },
        chat_account: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: ''
        },
        companyRegFile: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        idFrontFile: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        idBackFile: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        ownerFrontFile: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        ownerBackFile: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        vedioFile: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        isDelete: {
            type: DataTypes.INTEGER(2),
            allowNull: true,
            defaultValue: '0'
        },
        status: {
            type: DataTypes.INTEGER(2),
            allowNull: true,
            defaultValue: '0'
        },
        createdTime: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        updatedTime: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        app_id: {
            type: DataTypes.STRING(20),
            allowNull: false,
            defaultValue: '4USDT'
        }
    }, {
            tableName: 'access_request',
            timestamps: false
        });
};

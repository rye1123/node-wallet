/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('wlt_cobo_send_record', {
        id: {
            type: DataTypes.INTEGER(32),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        cobo_id: {
            type: DataTypes.STRING(40),
            allowNull: false,
            defaultValue: ''
        },
        request_id: {
            type: DataTypes.STRING(40),
            allowNull: false,
            defaultValue: ''
        },
        tx_id: {
            type: DataTypes.STRING(70),
            allowNull: false,
            defaultValue: ''
        },
        from_address: {
            type: DataTypes.STRING(40),
            allowNull: false,
            defaultValue: ''
        },
        to_address: {
            type: DataTypes.STRING(40),
            allowNull: false,
            defaultValue: ''
        },
        side: {
            type: DataTypes.STRING(20),
            allowNull: false,
            defaultValue: 'send'
        },
        coin: {
            type: DataTypes.STRING(255),
            allowNull: false,
            defaultValue: ''
        },
        amount: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.00000000'
        },
        cobo_fee: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.00000000'
        },
        tx_fee_coin: {
            type: DataTypes.STRING(10),
            allowNull: false,
            defaultValue: ''
        },
        tx_fee: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.00000000'
        },
        status: {
            type: DataTypes.STRING(10),
            allowNull: false,
            defaultValue: ''
        },
        time: {
            type: DataTypes.DATE,
            allowNull: false
        },
        is_delete: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '0'
        }
    }, {
        tableName: 'wlt_cobo_send_record',
        timestamps: false
    });
};

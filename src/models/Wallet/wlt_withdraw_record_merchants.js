/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('wlt_withdraw_record_merchants', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        request_id: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: ''
        },
        address: {
            type: DataTypes.STRING(200),
            allowNull: false
        },
        value: {
            type: DataTypes.DECIMAL,
            allowNull: false
        },
        fee: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.0000000000000000'
        },
        tx: {
            type: DataTypes.STRING(255),
            allowNull: false,
            defaultValue: ''
        },
        created_time: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        symbol: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        enable: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '1'
        },
        sign: {
            type: DataTypes.STRING(260),
            allowNull: false
        },
        is_delete: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '0'
        },
        status: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '0'
        },
        remark: {
            type: DataTypes.STRING(255),
            allowNull: false,
            defaultValue: ''
        },
        logBalance: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.0000000000000000'
        },
        order_no: {
            type: DataTypes.STRING(34),
            allowNull: false,
            defaultValue: ''
        },
        strategy_limit_per: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.0000000000000000'
        },
        strategy_limit_hour: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.0000000000000000'
        },
        strategy_limit_day: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.0000000000000000'
        },
        strategy_limit_approval: {
            type: DataTypes.INTEGER(2),
            allowNull: false,
            defaultValue: '0'
        },
        strategy_sign: {
            type: DataTypes.STRING(255),
            allowNull: false,
            defaultValue: ''
        },
        year: {
            type: DataTypes.INTEGER(2),
            allowNull: false,
            defaultValue: '2019'
        },
        month: {
            type: DataTypes.INTEGER(2),
            allowNull: false,
            defaultValue: '8'
        },
        day: {
            type: DataTypes.INTEGER(2),
            allowNull: false,
            defaultValue: '10'
        },
        hour: {
            type: DataTypes.INTEGER(2),
            allowNull: false,
            defaultValue: '0'
        }
    }, {
            tableName: 'wlt_withdraw_record_merchants',
            timestamps: false
        });
};

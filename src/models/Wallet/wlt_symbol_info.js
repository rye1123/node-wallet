/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('wlt_symbol_info', {
        id: {
            type: DataTypes.INTEGER(32),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(32),
            allowNull: false,
            defaultValue: ''
        },
        symbol: {
            type: DataTypes.STRING(20),
            allowNull: false,
            defaultValue: ''
        },
        display_name: {
            type: DataTypes.STRING(20),
            allowNull: false,
            defaultValue: ''
        },
        created_time: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        updated_time: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: 'CURRENT_TIMESTAMP(1)'
        },
        enable: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '1'
        },
        is_delete: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '0'
        },
        app_id: {
            type: DataTypes.STRING(20),
            allowNull: false,
            defaultValue: '4USDT'
        },
        withdrawFee: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '5.00000000'
        },
        withdrawMin: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '100.00000000'
        },
        address_role: {
            type: DataTypes.STRING(255),
            allowNull: false,
            defaultValue: ''
        },
    }, {
            tableName: 'wlt_symbol_info',
            timestamps: false
        });
};

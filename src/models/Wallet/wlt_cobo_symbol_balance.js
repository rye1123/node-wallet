/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('wlt_cobo_symbol_balance', {
        id: {
            type: DataTypes.INTEGER(32),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        coin: {
            type: DataTypes.STRING(32),
            allowNull: false,
            defaultValue: ''
        },
        display_code: {
            type: DataTypes.STRING(32),
            allowNull: false,
            defaultValue: ''
        },
        description: {
            type: DataTypes.STRING(32),
            allowNull: false,
            defaultValue: ''
        },
        decimal: {
            type: DataTypes.INTEGER(2),
            allowNull: false,
            defaultValue: '8'
        },
        can_deposit: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '1'
        },
        can_withdraw: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '1'
        },
        require_memo: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '0'
        },
        balance: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.0000000000000000'
        },
        abs_balance: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.0000000000000000'
        },
        fee_coin: {
            type: DataTypes.STRING(32),
            allowNull: false,
            defaultValue: ''
        },
        abs_estimate_fee: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.0000000000000000'
        },
        confirming_threshold: {
            type: DataTypes.INTEGER(5),
            allowNull: false,
            defaultValue: '10'
        },
        dust_threshold: {
            type: DataTypes.INTEGER(5),
            allowNull: false,
            defaultValue: '0'
        },
        token_address: {
          type: DataTypes.STRING(80),
          allowNull: true,
          defaultValue: ''
        },
        is_delete: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '0'
        }
    }, {
        tableName: 'wlt_cobo_symbol_balance',
        timestamps: false
    });
};

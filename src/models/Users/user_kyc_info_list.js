/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('user_kyc_info_list', {
      id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      user_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false
      },
      order_no: {
        type: DataTypes.STRING(60),
        allowNull: false,
        defaultValue: ''
      },
      abs_amount: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        defaultValue: '0.00'
      },
      kyc_name: {
        type: DataTypes.STRING(32),
        allowNull: false
      },
      kyc_id_no: {
        type: DataTypes.STRING(32),
        allowNull: false,
        defaultValue: ''
      },
      phone_no: {
        type: DataTypes.STRING(20),
        allowNull: false
      },
      result: {
        type: DataTypes.INTEGER(1),
        allowNull: false,
        defaultValue: '1'
      },
      source: {
        type: DataTypes.STRING(200),
        allowNull: false
      },
      enable: {
        type: DataTypes.INTEGER(1),
        allowNull: false,
        defaultValue: '1'
      },
      is_delete: {
        type: DataTypes.INTEGER(1),
        allowNull: false,
        defaultValue: '0'
      }
    }, {
      tableName: 'user_kyc_info_list',
      timestamps: false
    });
  };

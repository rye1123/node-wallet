/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('user_info', {
        id: {
            type: DataTypes.BIGINT,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        mobile: {
            type: DataTypes.CHAR(20),
            allowNull: false,
            defaultValue: '',
            unique: true
        },
        password: {
            type: DataTypes.STRING(60),
            allowNull: false,
            defaultValue: ''
        },
        email: {
            type: DataTypes.STRING(64),
            allowNull: false,
            defaultValue: ''
        },
        avatar: {
            type: DataTypes.STRING(200),
            allowNull: false,
            defaultValue: ''
        },
        name: {
            type: DataTypes.STRING(64),
            allowNull: false,
            defaultValue: ''
        },
        parent_id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            defaultValue: '0'
        },
        licenseNumber: {
            type: DataTypes.STRING(40),
            allowNull: false,
            defaultValue: ''
        },
        identifier: {
            type: DataTypes.STRING(64),
            allowNull: false,
            defaultValue: ''
        },
        area: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: ''
        },
        area_code: {
            type: DataTypes.STRING(16),
            allowNull: false,
            defaultValue: '+86'
        },
        source: {
            type: DataTypes.STRING(16),
            allowNull: false,
            defaultValue: ''
        },
        created_time: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        updated_time: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        isDelete: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '0'
        },
        isTraders: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '0'
        },
        isMerchants: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '0'
        },
        enable: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '1'
        },
        version: {
            type: DataTypes.STRING(20),
            allowNull: false,
            defaultValue: ''
        },
        authGoogleCode: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '0'
        },
        authPhoneCode: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '1'
        },
        authMailCode: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '1'
        },
        authFundsPassword: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '0'
        },
        authPicCode: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '1'
        },
        merchantName: {
            type: DataTypes.STRING(100),
            allowNull: false,
            defaultValue: ''
        },
        merchantStatus: {
            type: DataTypes.INTEGER(255),
            allowNull: false,
            defaultValue: '0'
        },
        merchantId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            defaultValue: '0'
        },
        merchantProfit: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.0000'
        },
        merchantBuyProfit: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.0000'
        },
        merchantSellProfit: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.0000'
        },
        merchantAppendFee: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.00000000'
        },
        merchantBuyAppendFee: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.00000000'
        },
        merchantSellAppendFee: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.00000000'
        },
        merchantPlatformBuyProfit: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.0050'
        },
        merchantPlatformSellProfit: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.0050'
        },
        merchantMarginValue: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '8000.0000000000000000'
        },
        tradersNo: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: '0'
        },
        tradersName: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: ''
        },
        tradersStatus: {
            type: DataTypes.INTEGER(5),
            allowNull: false,
            defaultValue: '0'
        },
        tradersMarginValue: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '8000.0000000000000000'
        },
        country: {
            type: DataTypes.STRING(50),
            allowNull: true,
            defaultValue: '中国'
        },
        identity_no: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: ''
        },
        merchant_max_cancel_order_day: {
            type: DataTypes.INTEGER(255),
            allowNull: false,
            defaultValue: '3'
        },
        merchant_max_order_concurrent: {
            type: DataTypes.INTEGER(255),
            allowNull: false,
            defaultValue: '1'
        },
        kyc_level: {
            type: DataTypes.INTEGER(5),
            allowNull: false,
            defaultValue: '0'
        },
        access_id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            defaultValue: '0'
        },
        app_id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            defaultValue: '1'
        },
        is_ispring_address: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '1'
        }
    }, {
        tableName: 'user_info',
        timestamps: false
    });
};

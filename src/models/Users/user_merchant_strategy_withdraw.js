/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('user_merchant_strategy_withdraw', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            defaultValue: '0'
        },
        limit_per: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.0000000000000000'
        },
        limit_hour: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.0000000000000000'
        },
        limit_day: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.0000000000000000'
        },
        limit_approval: {
            type: DataTypes.INTEGER(2),
            allowNull: false
        },
        symbol: {
            type: DataTypes.STRING(20),
            allowNull: false,
            defaultValue: 'USDT'
        },
        sign: {
            type: DataTypes.STRING(256),
            allowNull: false,
            defaultValue: ''
        },
        isDelete: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '0'
        },
        enable: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '1'
        },
        created_time: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        updated_time: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        }
    }, {
            tableName: 'user_merchant_strategy_withdraw',
            timestamps: false
        });
};

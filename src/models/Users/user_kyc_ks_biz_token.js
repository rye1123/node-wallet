/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('user_kyc_ks_biz_token', {
      id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      user_id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        defaultValue: '0'
      },
      request_id: {
        type: DataTypes.STRING(200),
        allowNull: false,
        defaultValue: ''
      },
      biz_token: {
        type: DataTypes.STRING(200),
        allowNull: false,
        defaultValue: '0'
      },
      time_used: {
        type: DataTypes.INTEGER(3),
        allowNull: false,
        defaultValue: '0'
      },
      isDelete: {
        type: DataTypes.INTEGER(1),
        allowNull: false,
        defaultValue: '0'
      },
      status: {
        type: DataTypes.INTEGER(4),
        allowNull: false,
        defaultValue: '0'
      },
      sign: {
        type: DataTypes.STRING(256),
        allowNull: false,
        defaultValue: ''
      },
      remark: {
        type: DataTypes.STRING(200),
        allowNull: false,
        defaultValue: ''
      },
      created_time: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
      },
      updated_time: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
      }
    }, {
      tableName: 'user_kyc_ks_biz_token',
      timestamps: false
    });
  };

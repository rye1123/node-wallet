/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('user_kyc_ks_vedio_chk_record', {
      id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      biz_token: {
        type: DataTypes.STRING(200),
        allowNull: false,
        defaultValue: '0'
      },
      isDelete: {
        type: DataTypes.INTEGER(1),
        allowNull: false,
        defaultValue: '0'
      },
      msg: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      created_time: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
      },
      updated_time: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
      }
    }, {
      tableName: 'user_kyc_ks_vedio_chk_record',
      timestamps: false
    });
  };

import KoaRouter from 'koa-router';
import services from '../../services';
import result from '../../tools/Result';

const router = new KoaRouter();
const amountService = new services.Account.AmountService();
const merchantService = new services._Manage.MerchantService();

/**
 * LoginController
 * 用户信息类
 */
class AmountController {

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async amountNoAuth(ctx) {
        try {
            ctx.body = result.success("", await amountService.amountBase(ctx.request.body.userid));
        }
        catch (ex) {
            ctx.body = result.failed('获取失败', 50230, ex)
        }
    }

    async orderFreeze(ctx) {
        ctx.body = await amountService.orderFreeze({ id: ctx.request.header.userid }, ctx.request.body)
    }

    async orderUnFreeze(ctx) {
        ctx.body = await amountService.orderUnfreeze({ id: ctx.request.header.userid }, ctx.request.body)
    }

    async transferAmount(ctx) {
        ctx.body = await amountService.transfer_amount({ id: ctx.request.header.userid }, ctx.request.body)
    }

    async merchantsBalance(ctx) {
        ctx.body = await merchantService.merchantsBalance({ id: ctx.request.header.userid }, ctx.request.query)
    }


    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async history(ctx) {
        // ctx.request.query
        // ctx.request.header.userid
        ctx.body = await amountService.history({ id: ctx.request.header.merchantid }, ctx.request.query);
    }

    /**
     * 获取商户额度信息
     * @param {*} ctx
     */
    async merchantAmount(ctx) {
        ctx.body = await amountService.merchantAmount(ctx.request.header.userid)
    }

    /**
     * 获取商户额度信息
     * @param {*} ctx
     */
    async merchantProfit(ctx) {
        ctx.body = await amountService.merchantProfit(ctx.request.header.userid, ctx.request.query)
    }
}

const {
    amountNoAuth,
    orderFreeze,
    orderUnFreeze,
    transferAmount,
    merchantAmount,
    merchantProfit,
    history,
    merchantsBalance
} = new AmountController();

/* eslint-disable */
const routers = [{
    url: `/merchants_balance`,
    method: 'get',
    acc: merchantsBalance
}, {
    url: `/amount/history`,
    method: 'get',
    acc: history
}, {
    url: `/_amount`,
    method: 'post',
    acc: amountNoAuth
}, {
    url: `/order/freeze`,
    method: 'post',
    acc: orderFreeze
}, {
    url: `/order/unfreeze`,
    method: 'post',
    acc: orderUnFreeze
}, {
    url: `/order/transfer_amount`,
    method: 'post',
    acc: transferAmount
}, {
    url: `/profit`,
    method: 'get',
    acc: merchantProfit
}, {
    url: `/amount`,
    method: 'get',
    acc: merchantAmount
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

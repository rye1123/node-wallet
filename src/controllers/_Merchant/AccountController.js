import KoaRouter from 'koa-router';
import services from '../../services';
import result from '../../tools/Result';

const router = new KoaRouter();
const accountService = new services.Account.AccountService();

/**
 * LoginController
 * 用户信息类
 */
class AccountController {

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    // async clientSecret(ctx) {
    //     ctx.body = await accountService.clientSecret(ctx.request.header.userid);
    // }

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async clientSecretByApiKey(ctx) {
        ctx.body = await accountService.clientSecretByApiKey(ctx.request.header.userid);
    }

}

const {
    clientSecretByApiKey
} = new AccountController();

/* eslint-disable */
const routers = [{
    url: `/client_secret`,
    method: 'get',
    acc: clientSecretByApiKey
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

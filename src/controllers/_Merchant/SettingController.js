import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
import result from '../../tools/Result';

const router = new KoaRouter();

const accountService = new services.Account.AccountService();
const merchantSettingService = new services.Account.MerchantSettingService();
const verifyService = new services.Verify.SessionService();

const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

/**
 * LoginController
 * 用户信息类
 */
class SettingController {

    async withdraw_strategy_list(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await merchantSettingService.withdraw_strategy_list(validate.data);
        } else {
            ctx.body = result.authorities();
        }
    }

    async withdraw_strategy_add(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    "phoneCode": ctx.request.body.validata.phoneCode,
                    "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await merchantSettingService.withdraw_strategy_add(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    async withdraw_strategy_delete(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    // "phoneCode": ctx.request.body.validata.phoneCode,
                    // "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await merchantSettingService.withdraw_strategy_delete(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    async withdraw_strategy_disable(ctx) {

        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    // "phoneCode": ctx.request.body.validata.phoneCode,
                    // "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await merchantSettingService.withdraw_strategy_disable(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    async withdraw_callback_status(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            try {
                ctx.body = result.success("success", await merchantSettingService.withdraw_callback_status(validate.data))
            }
            catch (error) {
                console.log(error)
                ctx.body = result.failed("获取失败", 650120)
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    async update_withdraw_callback_url(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            try {
                let verifyResult = await verifyService.Verify(validate.data, ctx.request.body.validata, true)
                if (verifyResult.success) {
                    ctx.body = result.success("success", await merchantSettingService.update_withdraw_callback_url(validate.data, ctx.request.body.data))
                }
                else {
                    ctx.body = result.failed("验证失败")
                }
            }
            catch (error) {
                console.log(error)
                ctx.body = result.failed("获取设置", 650122)
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async updateBuyProfit(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await accountService.updateBuyProfit(validate.data, ctx.request.body.data);
        }
    }

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async updateSellProfit(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await accountService.updateSellProfit(validate.data, ctx.request.body.data);
        }
    }
}

const {
    updateBuyProfit,
    updateSellProfit,
    withdraw_callback_status,
    update_withdraw_callback_url,
    withdraw_strategy_list,
    withdraw_strategy_add,
    withdraw_strategy_delete,
    withdraw_strategy_disable
} = new SettingController();

/* eslint-disable */
const routers = [{
    url: `/withdraw_strategy_list`,
    method: 'get',
    acc: withdraw_strategy_list
}, {
    url: `/withdraw_strategy_add`,
    method: 'post',
    acc: withdraw_strategy_add
}, {
    url: `/withdraw_strategy_delete`,
    method: 'post',
    acc: withdraw_strategy_delete
}, {
    url: `/withdraw_strategy_disable`,
    method: 'post',
    acc: withdraw_strategy_disable
}, {
    url: `/update_buy_profit`,
    method: 'post',
    acc: updateBuyProfit
}, {
    url: `/update_sell_profit`,
    method: 'post',
    acc: updateSellProfit
}, {
    url: `/withdraw_callback_status`,
    method: 'get',
    acc: withdraw_callback_status
}, {
    url: `/update_withdraw_callback_url`,
    method: 'post',
    acc: update_withdraw_callback_url
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
const router = new KoaRouter();
const exportService = new services.Common.ExportService();

const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

/**
 * 公共服务系统
 */
class ExportController {

    /**
     * 获取图片验证码
     * @param {*} ctx
     */
    async merchants_funds(ctx) {
        try {
            const validate = validateTools.validateJWT(ctx.request.header.authorization);
            if (validate) {
                const result = await exportService.merchants_funds(validate.data, ctx.request.query);
                ctx.body = result;
            } else {
                ctx.body = result.authorities();
            }
        } catch (ex) {
            console.log(ex)
            ctx.body = "error";
        }
    }
}

const { merchants_funds } = new ExportController();

/* eslint-disable */
const routers = [{
    url: `/merchants_funds`,
    method: 'get',
    acc: merchants_funds
}];
/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;
import KoaRouter from 'koa-router';
import services from '../../services';
const router = new KoaRouter();
const coboService = new services.Common.CoboService();

/**
 * 公共服务系统
 */
class CoboController {

    /**
     * 获取图片验证码
     * @param {*} ctx
     */
    async callback(ctx) {
        // console.log(typeof(ctx.request.body))
        // console.log(ctx.request.body)
        try {
            console.log(ctx.request.body)
            const result = await coboService.callback(ctx.request.body);
            if (result) {
                ctx.body = "ok";
            }
            else {
                ctx.body = "error"
            }
        } catch (ex) {
            console.log(ex)
            ctx.body = "error";
        }
    }
}

const { callback } = new CoboController();

/* eslint-disable */
const routers = [{
    url: `/callback`,
    method: 'post',
    acc: callback
}];
/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;
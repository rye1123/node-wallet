import KoaRouter from 'koa-router';
import services from '../../services';
const router = new KoaRouter();
const CommonService = new services.Common.CommonService();

/**
 * 公共服务系统
 */
class CommonController {

    async ks_get_biz_token(ctx) {
        ctx.body = await CommonService.ks_get_biz_token(ctx);
    }

    /**
     * 获取图片验证码
     * @param {*} ctx
     */
    async getImgValidate(ctx) {
        // const { text, result } = await CommonService.getImgValidate(ctx);
        //设置验证码
        // ctx.cookies.set('imgValidateData', text, { httpOnly: true, maxAge: 1000 * 60 * 5 });
        // ctx.body = result;
    }

    /**
     * 生成一個新的地址
     * @param {*} ctx
     */
    async generateAddress(ctx) {
        // const {result} = await CommonService.generateAddress(ctx);
        // ctx.body = result;
    }

    /**
     * 生成一個新的私钥
     * @param {*} ctx
     */
    async generateNewKey(ctx) {
        // const {result} = await CommonService.generateNewKey(ctx);
        // ctx.body = result;
    }

    /**
     * 对消息进行签名
     * @param {*} ctx
     */
    async signMsg(ctx) {
        // const {result} = await CommonService.signMsg(ctx);
        // ctx.body = result;
    }
}

const { getImgValidate,
    generateAddress,
    generateNewKey,
    signMsg,
    ks_get_biz_token
} = new CommonController();

/* eslint-disable */
const routers = [{
    url: `/getImgValidate`,
    method: 'get',
    acc: getImgValidate
}, {
    url: `/ks_get_biz_token`,
    method: 'get',
    acc: ks_get_biz_token
}, {
    url: `/generateAddress`,
    method: 'get',
    acc: generateAddress
}, {
    url: `/generateNewKey`,
    method: 'get',
    acc: generateNewKey
}, {
    url: `/signMsg`,
    method: 'get',
    acc: signMsg
}];
/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

import KoaRouter from 'koa-router';
import services from '../../services';
const router = new KoaRouter();
// const coboService = new services.Common.CoboService();
import AmountService from '../../services/Account/AmountService';
const amountService = new AmountService()

/**
 * 公共服务系统
 */
class KsController {

    /**
     * 获取图片验证码
     * @param {*} ctx
     */
    async notify(ctx) {
        console.log(ctx)
        // console.log(typeof(ctx.request.body))
        // console.log(ctx.request.body)
        try {
            console.log(`旷视回调通知 `)

            let ksResData = JSON.parse(ctx.request.body.data)
            // { "biz_token": "1570803987,8dcb2d95-d3c2-4f05-a3e8-edc6eda89b72", "biz_no": "20191011485130436305", "result_code": 1000, "result_message": "SUCCESS", "action": [] }
            console.log(ksResData)
            await amountService.setKsResult(ksResData.biz_token, ksResData.result_code, ctx.request.body.data)
            // const result = await coboService.callback(ctx.request.body);
            // if (result) {
            ctx.body = "ok";
            // }
            // else {
            //     ctx.body = "error"
            // }
        } catch (ex) {
            console.log(ex)
            ctx.body = "error";
        }
    }
}

const { notify } = new KsController();

/* eslint-disable */
const routers = [{
    url: `/notify`,
    method: 'post',
    acc: notify
}];
/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

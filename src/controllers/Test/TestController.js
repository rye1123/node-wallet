import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
import result from '../../tools/Result';
import RedisDao from '../../lib/RedisDao';
import Crypto from '../../tools/Secret';
const router = new KoaRouter();
const accountService = new services.Account.AccountService();
const verifyService = new services.Verify.SessionService();
const commonService = new services.Common.CommonService();
const traderService = new services._Manage.TraderService();



class AccountController {

    async app_pushMsg(ctx) {
        console.log(ctx.request.body)

        ctx.body = result.success(0, await commonService.app_pushMsg(ctx.request.body))

    }

    async sys_status(ctx) {
        let result = await traderService.traderBalance({ id: 1 }, { ids: '1' })
        result = result.data[0]._uid
        ctx.body = result
    }

}

const {
    app_pushMsg, sys_status
} = new AccountController();
const routers = [{
    url: `/app_push_msg`,
    method: 'post',
    acc: app_pushMsg
}, {
    url: `/sys_status`,
    method: 'get',
    acc: sys_status
}]

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

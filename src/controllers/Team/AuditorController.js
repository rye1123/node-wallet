import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
import result from '../../tools/Result';
const router = new KoaRouter();
const childAccountService = new services.Team.ChildAccountService();
const auditorService = new services.Team.AuditorService();
const verifyService = new services.Verify.SessionService();
const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

/**
 * LoginController
 * 用户信息类
 */
class AuditorController {

    async orderList(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await auditorService.orderList(validate.data, ctx.request.query);
        } else {
            ctx.body = result.authorities();
        }
    }

    async orderApproval(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    // "phoneCode": ctx.request.body.validata.phoneCode,
                    // "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await auditorService.orderApproval(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    async orderReject(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    // "phoneCode": ctx.request.body.validata.phoneCode,
                    // "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await auditorService.orderReject(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }
}

const {
    orderList, orderApproval, orderReject
} = new AuditorController();

/* eslint-disable */
const routers = [{
    url: `/approval_withdraw/list`,
    method: 'get',
    acc: orderList
}, {
    url: `/approval_withdraw/approval`,
    method: 'post',
    acc: orderApproval
}, {
    url: `/approval_withdraw/reject`,
    method: 'post',
    acc: orderReject
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

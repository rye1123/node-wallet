import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
import result from '../../tools/Result';
const router = new KoaRouter();
const childAccountService = new services.Team.ChildAccountService();
const accountService = new services.Account.AccountService();

const verifyService = new services.Verify.SessionService();
const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

/**
 * LoginController
 * 用户信息类
 */
class ChildAccountController {

    async list(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await childAccountService.list(validate.data, ctx.request.query);
        } else {
            ctx.body = result.authorities();
        }
    }

    async destroy(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    // "phoneCode": ctx.request.body.validata.phoneCode,
                    // "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await childAccountService.destroy(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 创建新的Google验证码
     * @param {*} ctx
     */
    async create(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    "phoneCode": ctx.request.body.validata.phoneCode,
                    "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await childAccountService.create(validate.data, ctx.request.body.data, ctx.request.body.config);
                await accountService.sendEmailRequest(validate.data, validate.data)
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 创建新的Google验证码
     * @param {*} ctx
     */
    async approvalCount(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await childAccountService.approvalCount(validate.data);
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 创建新的Google验证码
     * @param {*} ctx
     */
    async updateConfig(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    "phoneCode": ctx.request.body.validata.phoneCode,
                    "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await childAccountService.updateConfig(validate.data, ctx.request.body.data, ctx.request.body.config);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 创建新的Google验证码
     * @param {*} ctx
     */
    async freeze(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    // "phoneCode": ctx.request.body.validata.phoneCode,
                    // "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await childAccountService.freeze(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 创建新的Google验证码
     * @param {*} ctx
     */
    async unFreeze(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    // "phoneCode": ctx.request.body.validata.phoneCode,
                    // "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await childAccountService.unFreeze(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 创建新的Google验证码
     * @param {*} ctx
     */
    async reset_google(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    "phoneCode": ctx.request.body.validata.phoneCode,
                    "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await childAccountService.reset_google(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 创建新的Google验证码
     * @param {*} ctx
     */
    async reset_password(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    "phoneCode": ctx.request.body.validata.phoneCode,
                    "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await childAccountService.reset_password(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

}

const {
    create, list, destroy, freeze, unFreeze, reset_google, reset_password, updateConfig, approvalCount
} = new ChildAccountController();

/* eslint-disable */
const routers = [{
    url: `/child_account/create`,
    method: 'post',
    acc: create
}, {
    url: `/child_account/approval_member_count`,
    method: 'get',
    acc: approvalCount
}, {
    url: `/child_account/update_config`,
    method: 'post',
    acc: updateConfig
}, {
    url: `/child_account/list`,
    method: 'get',
    acc: list
}, {
    url: `/child_account/destroy`,
    method: 'post',
    acc: destroy
}, {
    url: `/child_account/freeze`,
    method: 'post',
    acc: freeze
}, {
    url: `/child_account/un_freeze`,
    method: 'post',
    acc: unFreeze
}, {
    url: `/child_account/reset_google`,
    method: 'post',
    acc: reset_google
}, {
    url: `/child_account/reset_password`,
    method: 'post',
    acc: reset_password
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

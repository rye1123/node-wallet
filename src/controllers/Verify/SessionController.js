import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
import result from '../../tools/Result';
import RedisDao from '../../lib/RedisDao';
import Crypto from '../../tools/Secret';
const router = new KoaRouter();
var sendemail = require('sendemail')
var emailHelper = sendemail.email;

// const amountService = new services.Verify.SessionService();
const commonService = new services.Common.CommonService();

const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

/**
 * LoginController
 * 用户信息类
 */
class SessionController {

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async sendPhoneCode(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let code = Math.random().toString().slice(-6)
            let cachekey = `session_verify:phoneCode:_${validate.data.id}`
            if (await commonService.sendSMS({ phone: validate.data.mobile,country_code: validate.data.area_code, code: code }, cachekey)) {
                RedisDao.set(cachekey, Crypto.encryptByDES(code), 600)
                ctx.body = result.success(null, { type: "phone_code", expires_in: 600 });
            } else {
                ctx.body = result.failed("发送失败", 50083)
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async VerifyPhoneCode(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        let parm_code = ctx.request.body.code
        if (!parm_code) return result.failed('参数错误')

        if (validate) {
            let phone_code = await RedisDao.get(`session_verify:phoneCode:_${validate.data.id}`)
            if (phone_code == Crypto.encryptByDES(parm_code)) {
                ctx.body = result.success(null, { result: true });
            } else {
                ctx.body = result.success(null, { result: false });
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async sendMailCode(ctx) {
        try {
            const validate = validateTools.validateJWT(ctx.request.header.authorization);
            if (validate) {
                let code = Math.random().toString().slice(-6)

                console.log(validate.data)

                RedisDao.set(`session_verify:mailCode:_${validate.data.id}`, Crypto.encryptByDES(code), 600)

                var person = {
                    name: validate.data.name,
                    email: validate.data.email, // person.email can also accept an array of emails
                    subject: `邮箱验证码 - 4USDT - [${code}]`,
                    mail_code: code
                }

                emailHelper('mail_vali_code', person, function (error, result) {
                    console.log(' - - - - - - - - - - - - - - - - - - - - -> email sent: ');
                    console.log(result);
                    console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
                    console.log(error)
                })

                ctx.body = result.success(null, { type: "mail_code", expires_in: 600 });
            } else {
                ctx.body = result.authorities();
            }
        } catch (ex) {
            console.log(ex)
            ctx.body = result.failed(ex);
        }
    }

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async VerifyMailCode(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        let parm_code = ctx.request.body.code
        if (!parm_code) return result.failed('参数错误')

        if (validate) {
            let mail_code = await RedisDao.get(`session_verify:mailCode:_${validate.data.id}`)
            console.log(`${mail_code} == ${parm_code}`)
            if (mail_code == Crypto.encryptByDES(parm_code)) {
                ctx.body = result.success(null, { result: true });
            } else {
                ctx.body = result.success(null, { result: false });
            }
        } else {
            ctx.body = result.authorities();
        }
    }

}

const {
    sendPhoneCode,
    sendMailCode,
    VerifyPhoneCode,
    VerifyMailCode
} = new SessionController();

/* eslint-disable */
const routers = [{
    url: `/send_phone_code`,
    method: 'get',
    acc: sendPhoneCode
}, {
    url: `/phone_code`,
    method: 'post',
    acc: VerifyPhoneCode
}, {
    url: `/send_mail_code`,
    method: 'get',
    acc: sendMailCode
}, {
    url: `/mail_code`,
    method: 'post',
    acc: VerifyMailCode
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

import KoaRouter from 'koa-router';
import services from '../../services';
import result from '../../tools/Result';
import middleware from '../../middleware';
import { MicroService } from '../../config.service';
import request from '../../tools/request';
const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();
const verifyService = new services.Verify.SessionService();

const router = new KoaRouter()

const API_SYSTEM_TRADERS_POST_ADD_TRADER_ACCOUNT_URL = `${MicroService.API_traders_type}${MicroService.API_traders_host}:${MicroService.API_traders_port}/api/addTradersAccount`


const API_SYSTEM_TRADERS_POST_EDIT_TRADER_ACCOUNT_URL = `${MicroService.API_traders_type}${MicroService.API_traders_host}:${MicroService.API_traders_port}/api/updateTradersAccount`

/**
 * LoginController
 * 用户信息类
 */
class PayMethodController {

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async addTradersAccount(ctx) {
        try {
            let url = `${API_SYSTEM_TRADERS_POST_ADD_TRADER_ACCOUNT_URL}`

            const validate = validateTools.validateJWT(ctx.request.header.authorization);
            if (validate) {

                console.log(ctx.request.body.data)

                let verifyResult = await verifyService.Verify(validate.data, ctx.request.body.validata, false)
                if (verifyResult.success) {
                    let { response, body } = await request.post(url, ctx.request.body.data)

                    console.log(response.statusCode)
                    console.log(body)
                    if (response.statusCode !== 200) {
                        ctx.body = result.failed(JSON.stringify(body))
                    } else {
                        ctx.body = body
                    }
                } else {
                    ctx.body = result.failed("验证失败")
                }

            } else {
                ctx.body = result.authorities();
            }
        } catch (e) {
            console.log(e)
            ctx.body = result.failed()
        }
    }
    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async updateTradersAccount(ctx) {
        try {
            let url = `${API_SYSTEM_TRADERS_POST_EDIT_TRADER_ACCOUNT_URL}`

            const validate = validateTools.validateJWT(ctx.request.header.authorization);
            if (validate) {

                console.log(ctx.request.body.data)

                let verifyResult = await verifyService.Verify(validate.data, ctx.request.body.validata, false)
                if (verifyResult.success) {
                    let { response, body } = await request.post(url, ctx.request.body.data)

                    console.log(response.statusCode)
                    console.log(body)
                    if (response.statusCode !== 200) {
                        ctx.body = result.failed(JSON.stringify(body))
                    } else {
                        ctx.body = body
                    }
                } else {
                    ctx.body = result.failed("验证失败")
                }

            } else {
                ctx.body = result.authorities();
            }
        } catch (e) {
            console.log(e)
            ctx.body = result.failed()
        }
    }

}

const {
    addTradersAccount, updateTradersAccount
} = new PayMethodController();

/* eslint-disable */
const routers = [{
    url: `/platform/traders_account/add`,
    method: 'post',
    acc: addTradersAccount
}, {
    url: `/platform/traders_account/update`,
    method: 'post',
    acc: updateTradersAccount
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

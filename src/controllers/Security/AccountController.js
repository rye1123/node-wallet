import KoaRouter from 'koa-router';
import services from '../../services';
const router = new KoaRouter();
// const ggService = new services.Security.AccountService();

/**
 * LoginController
 * 用户信息类
 */
class AccountController {

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async status(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            // ctx.body = await amountService.amount(validate.data, ctx.request.query);
        } else {
            ctx.body = result.authorities();
        }
    }
}

const {
    status
} = new AccountController();

/* eslint-disable */
const routers = [{
    url: `/account/status`,
    method: 'get',
    acc: status
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;
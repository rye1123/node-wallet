import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
const router = new KoaRouter();
const emailService = new services.Security.EmailService();

const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

/**
 * LoginController
 * 用户信息类
 */
class EmailController {


    async send_reg(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await emailService.send_reg(validate.data, ctx.request.body);
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 验证Google验证码
     * @param {*} ctx
     */
    async bind(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await emailService.bind(validate.data, ctx.request.body);
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 验证Google验证码
     * @param {*} ctx
     */
    async unbind(ctx) {
        ctx.body = await emailService.unbind({ jwt: ctx.request.header.authorization }, ctx.request.body);
    }
}

const {
    send_reg,
    bind,
    unbind
} = new EmailController();

/* eslint-disable */
const routers = [{
    url: `/email/send_bind`,
    method: 'post',
    acc: send_reg
}, {
    url: `/email/bind`,
    method: 'post',
    acc: bind
}, {
    url: `/email/unbind`,
    method: 'post',
    acc: unbind
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;
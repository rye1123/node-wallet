import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
import result from '../../tools/Result';
const router = new KoaRouter();
const apiService = new services.Security.ApiService();
const verifyService = new services.Verify.SessionService();
const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

/**
 * LoginController
 * 用户信息类
 */
class ApiController {

    async list(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await apiService.list(validate.data);
        } else {
            ctx.body = result.authorities();
        }
    }

    async destroy(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    // "phoneCode": ctx.request.body.validata.phoneCode,
                    // "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await apiService.destroy(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 创建新的Google验证码
     * @param {*} ctx
     */
    async create(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    "phoneCode": ctx.request.body.validata.phoneCode,
                    "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await apiService.create(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

}

const {
    create, list, destroy
} = new ApiController();

/* eslint-disable */
const routers = [{
    url: `/merchant_api/create`,
    method: 'post',
    acc: create
}, {
    url: `/merchant_api/list`,
    method: 'get',
    acc: list
}, {
    url: `/merchant_api/destroy`,
    method: 'post',
    acc: destroy
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

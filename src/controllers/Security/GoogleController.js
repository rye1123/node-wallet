import KoaRouter from 'koa-router';
import services from '../../services';
const router = new KoaRouter();
const ggService = new services.Security.GoogleService();
const verifyService = new services.Verify.SessionService();

import middleware from '../../middleware';
const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();
/**
 * LoginController
 * 用户信息类
 */
class GoogleController {

    /**
     * 创建新的Google验证码
     * @param {*} ctx
     */
    async create(ctx) {
        ctx.body = await ggService.create({ jwt: ctx.request.header.authorization });
    }

    /**
     * 验证Google验证码
     * @param {*} ctx
     */
    async bind(ctx) {
        ctx.body = await ggService.bind({ jwt: ctx.request.header.authorization }, ctx.request.body);
    }

    /**
     * 验证Google验证码
     * @param {*} ctx
     */
    async unbind(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        let verifyResult = await verifyService.Verify(validate.data, ctx.request.body.validata)
        if (verifyResult.success) {
            if (validate) {
                ctx.body = await ggService.unbind(validate.data);
            } else {
                ctx.body = result.authorities();
            }
        } else {
            ctx.body = result.failed("验证失败")
        }
    }

    /**
     * 验证Google验证码
     * @param {*} ctx
     */
    async validate(ctx) {
        ctx.body = await ggService.validate({ jwt: ctx.request.header.authorization }, ctx.request.body);
    }

    /**
     * 查看Google验证码状态
     * @param {*} ctx
     */
    async status(ctx) {
        console.log('status');
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await ggService.status(validate.data);
        } else {
            ctx.body = result.authorities();
        }
    }
}

const {
    create, validate, status, bind, unbind
} = new GoogleController();

/* eslint-disable */
const routers = [{
    url: `/google/create`,
    method: 'get',
    acc: create
}, {
    url: `/google/bind`,
    method: 'post',
    acc: bind
}, {
    url: `/google/unbind`,
    method: 'post',
    acc: unbind
}, {
    url: `/google/status`,
    method: 'get',
    acc: status
}, {
    url: `/google/validate`,
    method: 'post',
    acc: validate
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;
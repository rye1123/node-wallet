import KoaRouter from 'koa-router';
import services from '../../services';
import result from '../../tools/Result';
import RedisDao from '../../lib/RedisDao';
import Crypto from '../../tools/Secret';
var sendemail = require('sendemail')
var emailHelper = sendemail.email;

const router = new KoaRouter();
const accountService = new services.Account.AccountService();
const commonService = new services.Common.CommonService();
const access_requestService = new services._Manage.AccessService();

/**
 * LoginController
 * 用户信息类
 */
class AccountController {

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async regUser(ctx) {
        console.log(`------------------用户申请合作--------------------`)
        console.log(ctx.request.body)

        console.log(await access_requestService.regInfo(ctx.request.body))

        var person = {
            name: '申请合作',
            email: 'wei@4usdt.com', // person.email can also accept an array of emails
            subject: `申请合作`,
            mail_code: JSON.stringify(ctx.request.body)
        }

        emailHelper('mail_vali_code', person, function (error, result) {
            console.log(' - - - - - - - - - - - - - - - - - - - - -> email sent: ');
            console.log(result);
            console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
            console.log(error)
        })

        var person2 = {
            name: '申请合作',
            email: 'ray@bitmall.com', // person.email can also accept an array of emails
            subject: `申请合作`,
            mail_code: JSON.stringify(ctx.request.body)
        }

        emailHelper('mail_vali_code', person2, function (error, result) {
            console.log(' - - - - - - - - - - - - - - - - - - - - -> email sent: ');
            console.log(result);
            console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
            console.log(error)
        })

        ctx.body = result.success("您的请求已经被记录");
    }

    async activeMerchantInfo(ctx) {
        let merchantInfo = await RedisDao.get(`merchant_active:${ctx.request.body.token}`)

        console.log(merchantInfo)

        if (merchantInfo) {
            merchantInfo = JSON.parse(Crypto.decryptByDES(merchantInfo))
            merchantInfo.mobile = merchantInfo.mobile.substr(0, 3) + "****" + merchantInfo.mobile.substr(7, 5)
            ctx.body = result.success("获取成功", merchantInfo);
        }
        else {
            ctx.body = result.failed("没有找到待激活用户信息或者该信息已失效")
        }
    }

    async sendActMerchantPhoneCode(ctx) {
        let merchantInfo = await RedisDao.get(`merchant_active:${ctx.request.body.token}`)

        if (merchantInfo) {
            merchantInfo = JSON.parse(Crypto.decryptByDES(merchantInfo))

            let code = Math.random().toString().slice(-6)
            let cachekey = `session_verify:phoneCode:_${merchantInfo.id}`
            if (await commonService.sendSMS({ phone: merchantInfo.mobile,country_code: merchantInfo.area_code, code: code }, cachekey)) {
                await RedisDao.set(cachekey, Crypto.encryptByDES(code), 600)
                ctx.body = result.success(null, { type: "phone_code", expires_in: 600 });
            } else {
                ctx.body = result.failed("发送失败", 50083)
            }
        }
        else {
            ctx.body = result.failed("没有找到待激活用户信息或者该信息已失效")
        }
    }

    async actMerchant(ctx) {
        let merchantInfo = await RedisDao.get(`merchant_active:${ctx.request.body.token}`)

        if (merchantInfo) {
            merchantInfo = JSON.parse(Crypto.decryptByDES(merchantInfo))

            let redisCode = await RedisDao.get(`session_verify:phoneCode:_${merchantInfo.id}`)
            if (redisCode && Crypto.decryptByDES(redisCode) == ctx.request.body.code) {
                ctx.body = await accountService.setNewPassword({ account: merchantInfo.mobile, newpassword: ctx.request.body.password })
                await RedisDao.del(`merchant_active:${ctx.request.body.token}`)
                await RedisDao.del(`session_verify:phoneCode:_${merchantInfo.id}`)
            }
            else {
                ctx.body = result.failed("验证失败，请重新发送验证码")
            }
        }
        else {
            ctx.body = result.failed("没有找到待激活用户信息或者该信息已失效")
        }

    }
}

const {
    regUser, activeMerchantInfo, sendActMerchantPhoneCode, actMerchant
} = new AccountController();

/* eslint-disable */
const routers = [{
    url: `/reginfo`,
    method: 'post',
    acc: regUser
}, {
    url: `/active_merchant_info`,
    method: 'post',
    acc: activeMerchantInfo
}, {
    url: `/active_mc_send_code`,
    method: 'post',
    acc: sendActMerchantPhoneCode
}, {
    url: `/active_mc`,
    method: 'post',
    acc: actMerchant
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

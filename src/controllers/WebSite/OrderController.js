import KoaRouter from 'koa-router';
import services from '../../services';
import result from '../../tools/Result';
import RedisDao from '../../lib/RedisDao';
import Crypto from '../../tools/Secret';
import { MicroService } from '../../config.service';
import request from '../../tools/request';
var sendemail = require('sendemail')
import sequelize from '../../lib/sequelize';
var emailHelper = sendemail.email;

const router = new KoaRouter();
const accountService = new services.Account.AccountService();
const commonService = new services.Common.CommonService();
const access_requestService = new services._Manage.AccessService();

import AmountService from '../../services/Account/AmountService';
const amountService = new AmountService()

const API_ORDER_DETAIL_URL = `${MicroService.API_order_type}${MicroService.API_order_host}:${MicroService.API_order_port}/api/v1/_html/order/detail`

/**
 * LoginController
 * 用户信息类
 */
class OrderController {

    //订单详情
    async orderDetail(ctx) {

        ctx.body = await amountService.orderDetail(ctx.request.query)

    }

    //订单详情
    async orderDetailPost(ctx) {

        console.log(`post`)
        console.log(ctx.request.body)

        ctx.body = await amountService.orderDetail(ctx.request.body)

    }

}

const {
    orderDetail, orderDetailPost
} = new OrderController();

/* eslint-disable */
const routers = [{
    url: `/order/detail`,
    method: 'get',
    acc: orderDetail
}, {
    url: `/order/detail`,
    method: 'post',
    acc: orderDetailPost
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

import KoaRouter from 'koa-router';
import services from '../../services';
import result from '../../tools/Result';
import RedisDao from '../../lib/RedisDao';
import Crypto from '../../tools/Secret';
var sendemail = require('sendemail')
var emailHelper = sendemail.email;

const router = new KoaRouter();
const amountService = new services.Wallet.AmountService();
const aAamountService = new services.Account.AmountService();
const usdtService = new services.Wallet.USDTService();
const verifyService = new services.Verify.SessionService();
const commonService = new services.Common.CommonService();
const accountService = new services.Account.AccountService();

/**
 * LoginController
 * 用户信息类
 */
class AmountController {

    /**
     * 冻结保证金
     * @param {*} ctx
     */
    async marginFreeze(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let userId = ctx.request.header.userid
            ctx.body = await amountService.marginFreeze({ id: userId }, ctx.request.body);
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 解冻保证金
     * @param {*} ctx
     */
    async marginUnfreeze(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let userId = ctx.request.header.userid
            ctx.body = await amountService.marginUnfreeze({ id: userId }, ctx.request.body);
        } else {
            ctx.body = result.authorities();
        }
    }
}

const {
    marginFreeze,
    marginUnfreeze
} = new AmountController();

/* eslint-disable */
const routers = [{
    url: `/margin/freeze`,
    method: 'post',
    acc: marginFreeze
}, {
    url: `/margin/un_freeze`,
    method: 'post',
    acc: marginUnfreeze
}
];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;


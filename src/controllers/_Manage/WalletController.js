import KoaRouter from 'koa-router';
import services from '../../services';
import result from '../../tools/Result';

const router = new KoaRouter();
const walletService = new services._Manage.WalletService();

/**
 * LoginController
 * 用户信息类
 */
class WalletController {

    async platform_balance_info(ctx) {
        ctx.body = await walletService.platform_balance_info({ id: ctx.request.header.userid }, ctx.request.query)
    }

    async cobo_take_fee_info(ctx) {
        ctx.body = await walletService.cobo_take_fee_info({ id: ctx.request.header.userid })
    }

    async withdrawList(ctx) {
        ctx.body = await walletService.withdrawList({ id: ctx.request.header.userid }, ctx.request.query)
    }

    async approval(ctx) {
        ctx.body = await walletService.approval({ id: ctx.request.header.userid }, ctx.request.body)
    }

    async reject(ctx) {
        ctx.body = await walletService.reject({ id: ctx.request.header.userid }, ctx.request.body)
    }

}

const {
    approval, reject, withdrawList, platform_balance_info, cobo_take_fee_info
} = new WalletController();

/* eslint-disable */
const routers = [{
    url: `/platform_balance_info`,
    method: 'get',
    acc: platform_balance_info
}, {
    url: `/cobo_take_fee_info`,
    method: 'get',
    acc: cobo_take_fee_info
}, {
    url: `/withdraw_list`,
    method: 'get',
    acc: withdrawList
}, {
    url: `/withdraw_approval`,
    method: 'post',
    acc: approval
}, {
    url: `/withdraw_reject`,
    method: 'post',
    acc: reject
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

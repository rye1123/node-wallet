import KoaRouter from 'koa-router';
import services from '../../services';
import result from '../../tools/Result';

const router = new KoaRouter();
const amountService = new services.Account.AmountService();
const traderService = new services._Manage.TraderService();
const accountService = new services.Account.AccountService();

/**
 * LoginController
 * 用户信息类
 */
class TraderController {

    async accountMarginFreeze(ctx){
        ctx.body = await amountService.marginFreeze({ id: ctx.request.body.userid }, ctx.request.body);
        // ctx.body = await traderService.traderList({ id: ctx.request.header.userid }, ctx.request.query)
    }

    async accountMarginUnFreeze(ctx){
        ctx.body = await amountService.marginUnAllFreeze({ id: ctx.request.body.userid }, ctx.request.body);
        // ctx.body = await traderService.traderList({ id: ctx.request.header.userid }, ctx.request.query)
    }

    async traderList(ctx) {
        ctx.body = await traderService.traderList({ id: ctx.request.header.userid }, ctx.request.query)
    }

    async traderBalance(ctx) {
        ctx.body = await traderService.traderBalance({ id: ctx.request.header.userid }, ctx.request.query)
    }

    async traderDetail(ctx) {
        ctx.body = await traderService.traderDetail({ id: ctx.request.header.userid }, ctx.request.query)
    }

    async traderAdd(ctx) {
        console.log(`${JSON.stringify(ctx.request.header)} - ${ctx.request.header.userid} - ${JSON.stringify(ctx.request.body)}`)
        ctx.body = await traderService.traderAdd({ id: ctx.request.header.userid }, ctx.request.body)
    }

    async traderEdit(ctx) {
        console.log(`${JSON.stringify(ctx.request.header)} - ${ctx.request.header.userid} - ${JSON.stringify(ctx.request.body)}`)
        ctx.body = await traderService.traderEdit({ id: ctx.request.header.userid }, ctx.request.body)
    }

    async traderDisable(ctx) {
        ctx.body = await traderService.traderDisable({ id: ctx.request.header.userid }, ctx.request.body)
    }

    async sendEmailRequest(ctx) {
        ctx.body = await accountService.sendEmailRequest({ id: ctx.request.header.userid }, ctx.request.body);
    }

    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async wallet_history(ctx) {
        // ctx.request.query
        // ctx.request.header.userid
        ctx.body = await amountService.history({ id: 0 }, ctx.request.query);
    }

}

const {
    traderList, traderDetail, traderAdd, traderEdit, traderDisable, sendEmailRequest, traderBalance, wallet_history,accountMarginFreeze,accountMarginUnFreeze
} = new TraderController();

/* eslint-disable */
const routers = [{
    url: `/account/margin/freeze`,
    method: 'post',
    acc: accountMarginFreeze
},{
    url: `/account/margin/unfreeze`,
    method: 'post',
    acc: accountMarginUnFreeze
},{
    url: `/traders_wallet_history`,
    method: 'get',
    acc: wallet_history
}, {
    url: `/traders_balance`,
    method: 'get',
    acc: traderBalance
}, {
    url: `/trader_list`,
    method: 'get',
    acc: traderList
}, {
    url: `/trader_detail`,
    method: 'get',
    acc: traderDetail
}, {
    url: `/trader_add`,
    method: 'post',
    acc: traderAdd
}, {
    url: `/trader_edit`,
    method: 'post',
    acc: traderEdit
}, {
    url: `/trader_disable`,
    method: 'post',
    acc: traderDisable
}, {
    url: `/trader_send_request`,
    method: 'post',
    acc: sendEmailRequest
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

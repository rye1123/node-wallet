import KoaRouter from 'koa-router';
import services from '../../services';
import result from '../../tools/Result';
import RedisDao from '../../lib/RedisDao';
import Crypto from '../../tools/Secret';
var sendemail = require('sendemail')
var emailHelper = sendemail.email;

const router = new KoaRouter();
const amountService = new services.Wallet.AmountService();
const aAamountService = new services.Account.AmountService();
const usdtService = new services.Wallet.USDTService();
const verifyService = new services.Verify.SessionService();
const commonService = new services.Common.CommonService();
const accountService = new services.Account.AccountService();

/**
 * LoginController
 * 用户信息类
 */
class HedgingController {

    async balance_info(ctx) {
        let userid = ctx.request.header.userid
        if (userid == 100 || userid == 1) {
            ctx.body = result.success("success", await aAamountService.amountBaseList(userid));
        }
        else {
            ctx.body = result.failed('用户无权限')
        }
    }

    async sendPhoneCode(ctx) {
        let userid = ctx.request.header.userid
        if (userid == 100 || userid == 1) {
            let code = Math.random().toString().slice(-6)
            let cachekey = `session_verify:phoneCode:_${userid}`
            let accountInfo = await accountService.accountInfo({ id: userid })

            if (await commonService.sendSMS({ phone: accountInfo.data.mobile, country_code: accountInfo.data.area_code, code: code }, cachekey)) {
                RedisDao.set(cachekey, Crypto.encryptByDES(code), 600)
                ctx.body = result.success(null, { type: "phone_code", expires_in: 600 });
            } else {
                ctx.body = result.failed("发送失败", 50083)
            }
        }
        else {
            ctx.body = result.failed('用户无权限')
        }
    }

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async sendMailCode(ctx) {
        try {
            let userid = ctx.request.header.userid
            if (userid == 100 || userid == 1) {
                let code = Math.random().toString().slice(-6)
                let accountInfo = await accountService.accountInfo({ id: userid })

                RedisDao.set(`session_verify:mailCode:_${userid}`, Crypto.encryptByDES(code), 600)

                var person = {
                    name: accountInfo.data.name,
                    email: accountInfo.data.email, // person.email can also accept an array of emails
                    subject: `邮箱验证码 - 4USDT - [${code}]`,
                    mail_code: code
                }

                emailHelper('mail_vali_code', person, function (error, result) {
                    console.log(' - - - - - - - - - - - - - - - - - - - - -> email sent: ');
                    console.log(result);
                    console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
                    console.log(error)
                })

                ctx.body = result.success(null, { type: "mail_code", expires_in: 600 });
            }
            else {
                ctx.body = result.failed('用户无权限')
            }
        } catch (ex) {
            console.log(ex)
            ctx.body = result.failed(ex);
        }
    }

    async deposit_address(ctx) {
        let userid = ctx.request.header.userid
        let symbol = ctx.request.query.symbol || 'ETH_USDT'
        if (userid == 100 || userid == 1) {
            ctx.body = await usdtService.getAddress({ id: userid }, { symbol: symbol });
        }
        else {
            ctx.body = result.failed('用户无权限')
        }
    }

    async hedging_withdraw(ctx) {
        let userid = ctx.request.header.userid
        let verifyResult = await verifyService.Verify({ id: userid }, ctx.request.body.validata, true)
        if (verifyResult.success) {
            if (userid == 100 || userid == 1) {
                ctx.body = await amountService.withdraw(
                    { id: userid },
                    ctx.request.body.data);
            }
            else {
                ctx.body = result.failed('用户无权限')
            }
        } else {
            ctx.body = result.failed("验证失败")
        }
    }


    async balance_history(ctx) {
        let userid = ctx.request.header.userid
        if (userid == 100 || userid == 1) {
            ctx.body = await aAamountService.history({ id: userid }, ctx.request.query);
        }
        else {
            ctx.body = result.failed('用户无权限')
        }
    }
}

const {
    balance_info, deposit_address, hedging_withdraw, balance_history, sendPhoneCode, sendMailCode
} = new HedgingController();

/* eslint-disable */
const routers = [{
    url: `/balance_info`,
    method: 'get',
    acc: balance_info
}, {
    url: `/send_mail_code`,
    method: 'get',
    acc: sendMailCode
}, {
    url: `/send_phone_code`,
    method: 'get',
    acc: sendPhoneCode
}, {
    url: `/deposit_address`,
    method: 'get',
    acc: deposit_address
}, {
    url: `/withdraw`,
    method: 'post',
    acc: hedging_withdraw
}, {
    url: `/balance_history`,
    method: 'get',
    acc: balance_history
}
];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

import KoaRouter from 'koa-router';
import services from '../../services';
import result from '../../tools/Result';

const router = new KoaRouter();
const accountService = new services.Account.AccountService();
const merchantService = new services._Manage.MerchantService();

/**
 * LoginController
 * 用户信息类
 */
class MerchantController {

    async merchantChildAccountList(ctx) {
        ctx.body = await merchantService.merchantChildAccountList({ id: ctx.request.header.userid }, ctx.request.query)
    }

    async merchantList(ctx) {
        ctx.body = await merchantService.merchantList({ id: ctx.request.header.userid }, ctx.request.query)
    }

    async merchantDetail(ctx) {
        ctx.body = await merchantService.merchantDetail({ id: ctx.request.header.userid }, ctx.request.query)
    }

    async merchantAdd(ctx) {
        console.log(`${JSON.stringify(ctx.request.header)} - ${ctx.request.header.userid} - ${JSON.stringify(ctx.request.body)}`)
        ctx.body = await merchantService.merchantAdd({ id: ctx.request.header.userid }, ctx.request.body)
    }

    async merchantEdit(ctx) {
        console.log(`${JSON.stringify(ctx.request.header)} - ${ctx.request.header.userid} - ${JSON.stringify(ctx.request.body)}`)
        ctx.body = await merchantService.merchantEdit({ id: ctx.request.header.userid }, ctx.request.body)
    }

    async merchantDisable(ctx) {
        ctx.body = await merchantService.merchantDisable({ id: ctx.request.header.userid }, ctx.request.body)
    }

    async merchantUpdateSellProfit(ctx) {
        ctx.body = await accountService.updateSellProfit({ id: ctx.request.header.userid }, ctx.request.body.data);
    }

    async merchantUpdateBuyProfit(ctx) {
        ctx.body = await accountService.updateBuyProfit({ id: ctx.request.header.userid }, ctx.request.body.data);
    }

    async merchantResetPassword(ctx) {
        ctx.body = await merchantService.merchantResetPassword({ id: ctx.request.header.userid }, ctx.request.body);
    }

    async merchantEnable(ctx) {
        ctx.body = await merchantService.merchantEnable({ id: ctx.request.header.userid }, ctx.request.body);
    }

    async merchantUnbindGoogleCode(ctx) {
        ctx.body = await merchantService.merchantUnbindGoogleCode({ id: ctx.request.header.userid }, ctx.request.body);
    }

    async merchantUpdateProfit(ctx) {
        ctx.body = await accountService.updateProfit({ id: ctx.request.header.userid }, ctx.request.body.data);
    }

    async sendEmailRequest(ctx) {
        ctx.body = await accountService.sendEmailRequest({ id: ctx.request.header.userid }, ctx.request.body);
    }

    async getMerchantKycSetting(ctx) {
        ctx.body = await merchantService.getMerchantKycSetting({ id: ctx.request.header.userid }, ctx.request.query);
    }

    async setMerchantKycSetting(ctx) {
        console.log(ctx.request.body)
        ctx.body = await merchantService.setMerchantKycSetting({ id: ctx.request.header.userid }, ctx.request.body);
    }

}

const {
    merchantList, merchantDetail, merchantAdd, merchantEdit, merchantDisable, merchantUpdateSellProfit, merchantUpdateBuyProfit,
    merchantUpdateProfit, sendEmailRequest, merchantsBalance,
    merchantChildAccountList, merchantResetPassword, merchantUnbindGoogleCode, merchantEnable,
    setMerchantKycSetting, getMerchantKycSetting
} = new MerchantController();

/* eslint-disable */
const routers = [{
    url: `/set_merchant_kyc_setting`,
    method: 'post',
    acc: setMerchantKycSetting
}, {
    url: `/get_merchant_kyc_setting`,
    method: 'get',
    acc: getMerchantKycSetting
}, {
    url: `/merchant_child_account_list`,
    method: 'get',
    acc: merchantChildAccountList
}, {
    url: `/merchant_enable`,
    method: 'post',
    acc: merchantEnable
}, {
    url: `/merchant_reset_pwd`,
    method: 'post',
    acc: merchantResetPassword
}, {
    url: `/merchant_unbind_google_code`,
    method: 'post',
    acc: merchantUnbindGoogleCode
}, {
    url: `/merchant_list`,
    method: 'get',
    acc: merchantList
}, {
    url: `/merchant_detail`,
    method: 'get',
    acc: merchantDetail
}, {
    url: `/merchant_add`,
    method: 'post',
    acc: merchantAdd
}, {
    url: `/merchant_edit`,
    method: 'post',
    acc: merchantEdit
}, {
    url: `/merchant_disable`,
    method: 'post',
    acc: merchantDisable
}, {
    url: `/merchant_update_sell_profit`,
    method: 'post',
    acc: merchantUpdateSellProfit
}, {
    url: `/merchant_update_buy_profit`,
    method: 'post',
    acc: merchantUpdateBuyProfit
}, {
    url: `/merchant_update_profit`,
    method: 'post',
    acc: merchantUpdateProfit
}, {
    url: `/merchant_send_request`,
    method: 'post',
    acc: sendEmailRequest
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

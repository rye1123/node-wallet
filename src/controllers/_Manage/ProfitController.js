import KoaRouter from 'koa-router';
import services from '../../services';
import result from '../../tools/Result';

const router = new KoaRouter();
const profitService = new services._Manage.ProfitService();

/**
 * LoginController
 * 用户信息类
 */
class ProfitController {

    async platform(ctx) {
        ctx.body = await profitService.platform_profit({ id: ctx.request.header.userid }, ctx.request.query)
    }

    async merchants_profit_day(ctx) {
        ctx.body = await profitService.merchants_profit_day({ id: ctx.request.header.userid }, ctx.request.query)
    }

    async traders_profit_day(ctx) {
        ctx.body = await profitService.traders_profit_day({ id: ctx.request.header.userid }, ctx.request.query)
    }

    async withdraw_fee_profit_day(ctx) {
        ctx.body = await profitService.withdraw_fee_profit_day({ id: ctx.request.header.userid }, ctx.request.query)
    }
}

const {
    platform, merchants_profit_day, traders_profit_day, withdraw_fee_profit_day
} = new ProfitController();

/* eslint-disable */
const routers = [{
    url: `/platform`,
    method: 'get',
    acc: platform
}, {
    url: `/merchants_profit_day`,
    method: 'get',
    acc: merchants_profit_day
}, {
    url: `/traders_profit_day`,
    method: 'get',
    acc: traders_profit_day
}, {
    url: `/withdraw_fee_profit_day`,
    method: 'get',
    acc: withdraw_fee_profit_day
}
];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

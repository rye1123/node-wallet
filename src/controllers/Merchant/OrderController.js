import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
import result from '../../tools/Result';
import RedisDao from '../../lib/RedisDao';
import { MicroService } from '../../config.service';
import request from '../../tools/request';

const router = new KoaRouter();
// const amountService = new services.Verify.SessionService();


const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

const API_MERCHANT_ORDER_LIST_URL = `${MicroService.API_merchant_type}${MicroService.API_merchant_host}:${MicroService.API_merchant_port}/api/v1.1/order/merchant/page`

const API_MERCHANT_PRICE_INFO_URL = `${MicroService.API_merchant_type}${MicroService.API_merchant_host}:${MicroService.API_merchant_port}/api/v1.1/price`

/**
 * LoginController
 * 用户信息类
 */
class OrderController {

    async merchant_price_info(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        // let request = require('async-request'), response;
        if (validate) {
            try {
                let symbol = ctx.request.query.symbol
                let reqUrl = `${API_MERCHANT_PRICE_INFO_URL}?symbol=${symbol || "USDT"}`.replace('ETH_', '').replace('BTC_', '')

                console.log(`调用价格接口 ${reqUrl}`)

                let { response, body } = await request.get(reqUrl)

                if (response.statusCode !== 200) {
                    return error(response, body)
                } else {
                    let data = JSON.parse(body)
                    if (data.code == 0) {
                        ctx.body = result.success("success", data.data);
                    }
                    else {
                        ctx.body = result.failed(data.msg, data.code);
                    }
                    // ctx.body = result.success(null, response.body.data);
                }
            } catch (e) {

                console.log(e)

            }
        }
        else {
            ctx.body = result.authorities();
        }

    }


    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async merchant_order(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        // let request = require('async-request'), response;
        if (validate) {
            try {
                // response.statusCode
                // response.headers
                // response.body
                // ctx.request.query

                if (ctx.request.query.endTime && ctx.request.query.endTime.indexOf(' ') < 0) {
                    ctx.request.query.endTime = `${ctx.request.query.endTime} 23:59:59`
                }

                // console.log(ctx.request.query.endTime)
                if (ctx.request.query.status == "ING") {
                    ctx.request.query.status = [`PAY_NO`, `PAY_FINISH`];
                }
                let reqData = {
                    "clientId": validate.data.parent_id || validate.data.id,
                    "page": parseInt(ctx.request.query.page),
                    "pageSize": parseInt(ctx.request.query.per_page),
                    "endTime": ctx.request.query.endTime,
                    "startTime": ctx.request.query.startTime,
                    "status": ctx.request.query.status,
                    "keyword": ctx.request.query.keyword || "",
                    "side": ctx.request.query.side,
                    "currency": ctx.request.query.symbol || ""
                }

                console.log(reqData)
                let { response, body } = await request.post(
                    API_MERCHANT_ORDER_LIST_URL, reqData
                )

                if (response.statusCode !== 200) {
                    return error(response, body)
                } else {
                    // ctx.body = result.success(null, response.body.data);
                    ctx.body = result.pageData(null, null, response.body.data,
                        response.body.meta.pagination.total,
                        response.body.meta.pagination.page,
                        response.body.meta.pagination.limit);
                }

                // success(response, body)


                // response = await request("POST", API_MERCHANT_ORDER_LIST_URL, {
                //     headers: {
                //         "Content-Type": "application/json"
                //     },
                //     // json: true,
                //     data: {
                //         "clientId": "111111",
                //         "page": 1,
                //         "pageSize": 10
                //     }
                // });
                // response = await request(API_MERCHANT_ORDER_LIST_URL, {
                //     method: 'POST',
                //     headers: {
                //         "Content-Type": "application/json"
                //     },
                //     // json: true,
                //     data: {
                //         "clientId": "111111",
                //         "page": 1,
                //         "pageSize": 10
                //     }
                // });
            } catch (e) {

            }

            // ctx.body = result.success(null, response);
        } else {
            ctx.body = result.authorities();
        }
    }

}

const {
    merchant_order, merchant_price_info
} = new OrderController();

/* eslint-disable */
const routers = [{
    url: `/price`,
    method: 'get',
    acc: merchant_price_info
}, {
    url: `/order`,
    method: 'get',
    acc: merchant_order
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

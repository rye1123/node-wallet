import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
import result from '../../tools/Result';
import RedisDao from '../../lib/RedisDao';
import uuid from "uuid";
import { MicroService } from '../../config.service';
import request from '../../tools/request';

const router = new KoaRouter();
// const amountService = new services.Verify.SessionService();


const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

const API_MERCHANT_ORDER_Statistics_CHART_URL = `${MicroService.API_merchant_type}${MicroService.API_merchant_host}:${MicroService.API_merchant_port}/api/v1.1/order/merchant/statistics/chart`
const API_MERCHANT_ORDER_Statistics_TODAY_URL = `${MicroService.API_merchant_type}${MicroService.API_merchant_host}:${MicroService.API_merchant_port}/api/v1.1/order/merchant/statistics/today`

const API_MERCHANT_ORDER_EXPORT_URL = `${MicroService.API_merchant_type}${MicroService.API_merchant_host}:${MicroService.API_merchant_port}/api/v1.1/order/merchant/export`


/**
 * LoginController
 * 用户信息类
 */
class StatisticsController {

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async merchant_index_chart(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            try {
                let reqUrl = `${API_MERCHANT_ORDER_Statistics_CHART_URL}?merchantId=${validate.data.parent_id || validate.data.id}`

                console.log(reqUrl)

                let { response, body } = await request.get(reqUrl)

                if (response.statusCode !== 200) {
                    return error(response, body)
                } else {
                    body = JSON.parse(body)
                    ctx.body = result.success("success", body.data);
                }
            } catch (e) {
                console.log(e)
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async merchant_index_today(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            try {

                let { response, body } = await request.get(
                    `${API_MERCHANT_ORDER_Statistics_TODAY_URL}?merchantId=${validate.data.parent_id || validate.data.id}`
                )

                if (response.statusCode !== 200) {
                    return error(response, body)
                } else {
                    body = JSON.parse(body)
                    ctx.body = result.success("success", body.data);
                }
            } catch (e) {
                console.log(e)
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    async merchant_order_export(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            try {
                let curUid = uuid.v4()
                RedisDao.set(curUid, validate.data.parent_id || validate.data.id, 60 * 60)
                // let curlUrl = `${API_MERCHANT_ORDER_EXPORT_URL}?token=${curUid}&startTime=${ctx.request.query.startTime}&endTime=${ctx.request.query.endTime}`
                // console.log(curlUrl)
                if (process.env.SYS_ENV == "development") {
                    console.log(`测试环境-导出订单数据`)
                    if (ctx.request.query.status == "ING") {
                        ctx.request.query.status = "PAY_NO,PAY_FINISH"
                    }
                    ctx.body = result.success("success", `http://47.75.145.179:8072/api/v1.1/order/merchant/export?token=${curUid}&startTime=${ctx.request.query.startTime}&endTime=${ctx.request.query.endTime}&side=${ctx.request.query.side}&status=${ctx.request.query.status || ''}&keyword=${ctx.request.query.keyword || ''}`);
                }
                else {
                    console.log(`生产环境-导出订单数据`)
                    if (ctx.request.query.status == "ING") {
                        ctx.request.query.status = "PAY_NO,PAY_FINISH"
                    }
                    ctx.body = result.success("success", `https://a0.4usdt.com/api/v1.1/order/merchant/export?token=${curUid}&startTime=${ctx.request.query.startTime}&endTime=${ctx.request.query.endTime}&side=${ctx.request.query.side}&status=${ctx.request.query.status || ''}&keyword=${ctx.request.query.keyword || ''}`);
                }
                // let { response, body } = await request.get(curlUrl)

                // if (response.statusCode !== 200) {
                //     return error(response, body)
                // } else {
                //     body = JSON.parse(body)
                //     ctx.body = result.success("success", curlUrl);
                // }
            } catch (e) {
                console.log(e)
            }
        } else {
            ctx.body = result.authorities();
        }
    }
}

const {
    merchant_index_chart, merchant_index_today, merchant_order_export
} = new StatisticsController();

/* eslint-disable */
const routers = [{
    url: `/order/merchant_index_chart`,
    method: 'get',
    acc: merchant_index_chart
}, {
    url: `/order/merchant_index_today`,
    method: 'get',
    acc: merchant_index_today
}, {
    url: `/order/merchant_order_export`,
    method: 'get',
    acc: merchant_order_export
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

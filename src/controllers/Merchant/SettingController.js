import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
import result from '../../tools/Result';

const router = new KoaRouter();

const accountService = new services.Account.AccountService();
const merchantSettingService = new services.Merchants.SettingService();
const verifyService = new services.Verify.SessionService();

const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

/**
 * LoginController
 * 用户信息类
 */
class SettingController {


    //#region  白名单



    async withdraw_white_address_list(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await merchantSettingService.withdraw_white_address_list(validate.data, ctx.request.query);
        } else {
            ctx.body = result.authorities();
        }
    }

    async withdraw_white_address_add(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    "phoneCode": ctx.request.body.validata.phoneCode,
                    "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await merchantSettingService.withdraw_white_address_add(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    async withdraw_white_address_edit(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    "phoneCode": ctx.request.body.validata.phoneCode,
                    "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await merchantSettingService.withdraw_white_address_edit(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    async withdraw_white_address_delete(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    // "phoneCode": ctx.request.body.validata.phoneCode,
                    // "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await merchantSettingService.withdraw_white_address_delete(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    async withdraw_white_address_disable(ctx) {

        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    // "phoneCode": ctx.request.body.validata.phoneCode,
                    // "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await merchantSettingService.withdraw_white_address_disable(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    async withdraw_white_address_enable(ctx) {

        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    // "phoneCode": ctx.request.body.validata.phoneCode,
                    // "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await merchantSettingService.withdraw_white_address_enable(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    //#endregion


    //#region  策略


    async withdraw_strategy_list(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await merchantSettingService.withdraw_strategy_list(validate.data, ctx.request.query);
        } else {
            ctx.body = result.authorities();
        }
    }

    async withdraw_strategy_add(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    "phoneCode": ctx.request.body.validata.phoneCode,
                    "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await merchantSettingService.withdraw_strategy_add(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    async withdraw_strategy_edit(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    "phoneCode": ctx.request.body.validata.phoneCode,
                    "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await merchantSettingService.withdraw_strategy_edit(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    async withdraw_strategy_delete(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    // "phoneCode": ctx.request.body.validata.phoneCode,
                    // "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await merchantSettingService.withdraw_strategy_delete(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    async withdraw_strategy_disable(ctx) {

        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    // "phoneCode": ctx.request.body.validata.phoneCode,
                    // "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await merchantSettingService.withdraw_strategy_disable(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    async withdraw_strategy_enable(ctx) {

        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            let verifyResult = await verifyService.Verify(validate.data,
                {
                    "googleCode": ctx.request.body.validata.googleCode,
                    // "phoneCode": ctx.request.body.validata.phoneCode,
                    // "emailCode": ctx.request.body.validata.emailCode
                }, false)
            if (verifyResult.success) {
                ctx.body = await merchantSettingService.withdraw_strategy_enable(validate.data, ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    //#endregion

    async withdraw_callback_status(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            try {
                ctx.body = result.success("success", await merchantSettingService.withdraw_callback_status(validate.data))
            }
            catch (error) {
                console.log(error)
                ctx.body = result.failed("获取失败", 650120)
            }
        } else {
            ctx.body = result.authorities();
        }
    }

    async update_withdraw_callback_url(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            try {
                let verifyResult = await verifyService.Verify(validate.data, ctx.request.body.validata, true)
                if (verifyResult.success) {
                    ctx.body = result.success("success", await merchantSettingService.update_withdraw_callback_url(validate.data, ctx.request.body.data))
                }
                else {
                    ctx.body = result.failed("验证失败")
                }
            }
            catch (error) {
                console.log(error)
                ctx.body = result.failed("获取设置", 650122)
            }
        } else {
            ctx.body = result.authorities();
        }
    }

}

const {
    //回调地址
    withdraw_callback_status,
    update_withdraw_callback_url,
    //策略
    withdraw_strategy_list,
    withdraw_strategy_add,
    withdraw_strategy_delete,
    withdraw_strategy_disable,
    withdraw_strategy_edit,
    //白名单
    withdraw_white_address_list,
    withdraw_white_address_add,
    withdraw_white_address_disable,
    withdraw_white_address_delete,
    withdraw_white_address_enable,
    withdraw_white_address_edit
} = new SettingController();

/* eslint-disable */
const routers = [
    //策略
    {
        url: `/withdraw_strategy_list`,
        method: 'get',
        acc: withdraw_strategy_list
    }, {
        url: `/withdraw_strategy_add`,
        method: 'post',
        acc: withdraw_strategy_add
    }, {
        url: `/withdraw_strategy_edit`,
        method: 'post',
        acc: withdraw_strategy_edit
    }, {
        url: `/withdraw_strategy_delete`,
        method: 'post',
        acc: withdraw_strategy_delete
    }, {
        url: `/withdraw_strategy_disable`,
        method: 'post',
        acc: withdraw_strategy_disable
    },
    //地址白名单
    {
        url: `/withdraw_white_address_list`,
        method: 'get',
        acc: withdraw_white_address_list
    }, {
        url: `/withdraw_white_address_add`,
        method: 'post',
        acc: withdraw_white_address_add
    }, {
        url: `/withdraw_white_address_edit`,
        method: 'post',
        acc: withdraw_white_address_edit
    }, {
        url: `/withdraw_white_address_disable`,
        method: 'post',
        acc: withdraw_white_address_disable
    }, {
        url: `/withdraw_white_address_delete`,
        method: 'post',
        acc: withdraw_white_address_delete
    }, {
        url: `/withdraw_white_address_enable`,
        method: 'post',
        acc: withdraw_white_address_enable
    },
    //回调地址
    {
        url: `/withdraw_callback_status`,
        method: 'get',
        acc: withdraw_callback_status
    }, {
        url: `/update_withdraw_callback_url`,
        method: 'post',
        acc: update_withdraw_callback_url
    }];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

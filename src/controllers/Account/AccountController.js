import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
import result from '../../tools/Result';
import RedisDao from '../../lib/RedisDao';
import Crypto from '../../tools/Secret';
const router = new KoaRouter();
const accountService = new services.Account.AccountService();
const verifyService = new services.Verify.SessionService();
const commonService = new services.Common.CommonService();

var sendemail = require('sendemail')
var emailHelper = sendemail.email;

const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

/**
 * LoginController
 * 用户信息类
 */
class AccountController {

    async roleConfig(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await accountService.roleConfig(validate.data);
        } else {
            ctx.body = result.authorities();
        }
    }

    async roleList(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await accountService.roleList(validate.data);
        } else {
            ctx.body = result.authorities();
        }
    }
    /**
     * 获取用户信息
     * @param {*} ctx
     */
    async accountInfo(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await accountService.accountInfo(validate.data);
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 获取用户信息
     * @param {*} ctx
     */
    async baseInfo(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await accountService.baseInfo(validate.data);
        } else {
            ctx.body = result.authorities();
        }
    }

    async updatePassword(ctx) {
        try {
            console.log("udpate pwd")
            const validate = validateTools.validateJWT(ctx.request.header.authorization);
            let verifyResult = await verifyService.Verify(validate.data, ctx.request.body.validata, false)
            console.log(verifyResult)
            if (verifyResult.success) {
                if (validate) {
                    ctx.body = await accountService.updatePassword(validate.data, ctx.request.body.data);
                } else {
                    ctx.body = result.authorities();
                }
            } else {
                ctx.body = result.failed("验证失败")
            }
        } catch (ex) {
            console.log(ex)
            ctx.body = result.failed(ex)
        }
    }




    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async sendRPPhoneCode(ctx) {
        let code = Math.random().toString().slice(-6)
        const account = ctx.request.body.account
        const accountModel = await accountService.accountInfoByMobile(account);
        let cachekey = `session_verify:phoneCode:_reset_pwd_${account}`
        if (await commonService.sendSMS({ phone: accountModel.mobile, country_code: accountModel.area_code, code: code }, cachekey)) {
            RedisDao.set(cachekey, Crypto.encryptByDES(code), 600)
            ctx.body = result.success(null, { type: "phone_code", expires_in: 600 });
        } else {
            ctx.body = result.failed("发送失败", 50083)
        }
    }

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async sendRPMailCode(ctx) {
        try {
            let code = Math.random().toString().slice(-6)

            const account = ctx.request.body.account
            const accountModel = await accountService.accountInfoByMobile(account);

            RedisDao.set(`session_verify:mailCode:_reset_pwd_${account}`, Crypto.encryptByDES(code), 600)

            var person = {
                name: accountModel.name,
                email: accountModel.email, // person.email can also accept an array of emails
                subject: `邮箱验证码 - 4USDT - [${code}]`,
                mail_code: code
            }

            emailHelper('mail_vali_code', person, function (error, result) {
                console.log(' - - - - - - - - - - - - - - - - - - - - -> email sent: ');
                console.log(result);
                console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
                console.log(error)
            })

            ctx.body = result.success(null, { type: "mail_code", expires_in: 600 });
        } catch (ex) {
            console.log(ex)
            ctx.body = result.failed(ex);
        }
    }

    async resetPassword(ctx) {
        try {
            console.log("reset pwd")
            const valiCode = ctx.request.body.validata
            let verifyResult = await verifyService.Verify({ id: `reset_pwd_${ctx.request.body.data.account}` }, valiCode, false)
            console.log(verifyResult)
            if (verifyResult.success) {
                console.log(ctx.request.body.data)
                ctx.body = await accountService.resetPassword(ctx.request.body.data);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } catch (ex) {
            console.log(ex)
            ctx.body = result.failed(ex)
        }
    }
}

const {
    accountInfo,
    baseInfo,
    updatePassword,
    resetPassword,
    sendRPMailCode,
    sendRPPhoneCode, roleConfig, roleList
} = new AccountController();

/* eslint-disable */
const routers = [{
    url: `/info`,
    method: 'get',
    acc: accountInfo
}, {
    url: `/role_list_info`,
    method: 'get',
    acc: roleList
}, {
    url: `/role_info`,
    method: 'get',
    acc: roleConfig
}, {
    url: `/base_info`,
    method: 'get',
    acc: baseInfo
}, {
    url: `/update_password`,
    method: 'post',
    acc: updatePassword
}, {
    url: `/reset_pwd/reset_password`,
    method: 'post',
    acc: resetPassword
}, {
    url: `/reset_pwd/send_mail_code`,
    method: 'post',
    acc: sendRPMailCode
}, {
    url: `/reset_pwd/send_phone_code`,
    method: 'post',
    acc: sendRPPhoneCode
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

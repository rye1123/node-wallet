import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
import result from '../../tools/Result';
const router = new KoaRouter();
const amountService = new services.Account.AmountService();

const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

/**
 * LoginController
 * 用户信息类
 */
class AmountController {

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async amount(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            console.log(ctx.request.query)
            ctx.body = await amountService.amount(validate.data, ctx.request.query);
        } else {
            ctx.body = result.authorities();
        }
    }

    async amountList(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = result.success("success", await amountService.amountBaseList(validate.data.parent_id || validate.data.id));
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 获取商户额度信息
     * @param {*} ctx
     */
    async merchantAmount(ctx) {
        ctx.body = await amountService.merchantAmount(ctx.request.header.merchantid)
    }

    /**
     * 获取商户费率信息
     * @param {*} ctx
     */
    async merchantProfit(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await amountService.merchantProfit(validate.data.parent_id || validate.data.id, ctx.request.query)
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 获取用户充值地址
     * @param {*} ctx
     */
    async address(ctx) {
        ctx.body = await amountService.address({ jwt: ctx.request.header.authorization }, ctx.request.query);
    }

    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async history(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await amountService.history(validate.data, ctx.request.query);
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 冻结保证金
     * @param {*} ctx
     */
    async marginFreeze(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await amountService.marginFreeze(validate.data, ctx.request.body);
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 解冻保证金
     * @param {*} ctx
     */
    async marginUnfreeze(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await amountService.marginUnfreeze(validate.data, ctx.request.body);
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 冻结订单金额
     * @param {*} ctx
     */
    async orderFreeze(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await amountService.orderFreeze(validate.data, ctx.request.body);
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 解冻订单金额，取消订单使用
     * @param {*} ctx
     */
    async orderUnfreeze(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await amountService.orderUnfreeze(validate.data, ctx.request.body);
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 订单完成转账（自动解冻并转账）
     * @param {*} ctx
     */
    async transfer_amount(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await amountService.transfer_amount(validate.data, ctx.request.body);
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 充值
     * @param {*} ctx
     */
    async deposit(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await amountService.deposit(validate.data, ctx.request.body);
        } else {
            ctx.body = result.authorities();
        }
    }
}

const {
    amount,
    address,
    history,
    marginFreeze,
    marginUnfreeze,
    orderFreeze,
    orderUnfreeze,
    transfer_amount,
    merchantProfit,
    merchantAmount,
    amountList,
    // withdraw,
    deposit
} = new AmountController();

/* eslint-disable */
const routers = [{
    url: `/merchant_profit`,
    method: 'get',
    acc: merchantProfit
}, {
    url: `/amount_list`,
    method: 'get',
    acc: amountList
}, {
    url: `/merchant_amount`,
    method: 'get',
    acc: merchantAmount
}, {
    url: `/amount`,
    method: 'get',
    acc: amount
}, {
    url: `/amount/address`,
    method: 'get',
    acc: address
}, {
    url: `/amount/history`,
    method: 'get',
    acc: history
}, {
    url: `/amount/margin/freeze`,
    method: 'post',
    acc: marginFreeze
}, {
    url: `/amount/margin/unfreeze`,
    method: 'post',
    acc: marginUnfreeze
}, {
    url: `/amount/order/freeze`,
    method: 'post',
    acc: orderFreeze
}, {
    url: `/amount/order/unfreeze`,
    method: 'post',
    acc: orderUnfreeze
}, {
    url: `/amount/order/transfer_amount`,
    method: 'post',
    acc: transfer_amount
},
//  {
//     url: `/amount/withdraw`,
//     method: 'post',
//     acc: withdraw
// },
{
    url: `/amount/deposit`,
    method: 'post',
    acc: deposit
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

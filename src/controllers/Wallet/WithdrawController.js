import KoaRouter from 'koa-router';
import services from '../../services';
import result from '../../tools/Result';
import middleware from '../../middleware';

const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

const router = new KoaRouter();
const withdrawService = new services.Wallet.WithdrawService();


class WithdrawController {

    async withdraw_record(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await withdrawService.list(validate.data, ctx.request.query);
        } else {
            ctx.body = result.authorities();
        }
    }

}

const {
    withdraw_record
} = new WithdrawController();

/* eslint-disable */
const routers = [{
    url: `/record`,
    method: 'get',
    acc: withdraw_record
}];
//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

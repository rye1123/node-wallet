import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
import result from '../../tools/Result';
var sendemail = require('sendemail')
var emailHelper = sendemail.email;
const router = new KoaRouter();
const amountService = new services.Wallet.AmountService();
const verifyService = new services.Verify.SessionService();

const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

/**
 * LoginController
 * 用户信息类
 */
class AmountController {

    async address_check(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);

        if (validate) {
            ctx.body = await amountService.address_check(ctx.request.body)
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 获取用户信息
     * @param {*} ctx
     */
    async withdraw(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        let verifyResult = await verifyService.Verify(validate.data, ctx.request.body.validata, true)
        if (verifyResult.success) {
            if (validate) {
                ctx.body = await amountService.withdraw(
                    validate.data,
                    ctx.request.body.data);

                if (process.env.SYS_ENV != "development") {
                    var person = {
                        name: `提现申请-${process.env.SYS_ENV}`,
                        email: '519303436@qq.com', // person.email can also accept an array of emails
                        subject: `提现申请`,
                        mail_code: JSON.stringify(ctx.request.body.data)
                    }

                    emailHelper('mail_vali_code', person, function (error, result) {
                        console.log(' - - - - - - - - - - - - - - - - - - - - -> email sent: ');
                        console.log(result);
                        console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
                        console.log(error)
                    })

                    var person1 = {
                        name: `提现申请-${process.env.SYS_ENV}`,
                        email: '267300745@qq.com', // person.email can also accept an array of emails
                        subject: `提现申请`,
                        mail_code: JSON.stringify(ctx.request.body.data)
                    }

                    emailHelper('mail_vali_code', person1, function (error, result) {
                        console.log(' - - - - - - - - - - - - - - - - - - - - -> email sent: ');
                        console.log(result);
                        console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
                        console.log(error)
                    })

                    var person2 = {
                        name: `提现申请-${process.env.SYS_ENV}`,
                        email: '735182700@qq.com', // person.email can also accept an array of emails
                        subject: `提现申请`,
                        mail_code: JSON.stringify(ctx.request.body.data)
                    }

                    emailHelper('mail_vali_code', person2, function (error, result) {
                        console.log(' - - - - - - - - - - - - - - - - - - - - -> email sent: ');
                        console.log(result);
                        console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
                        console.log(error)
                    })
                }
                else {
                    console.log(`非生产环境，不发送提现申请通知邮件`)
                }
            } else {
                ctx.body = result.authorities();
            }
        } else {
            ctx.body = result.failed("验证失败")
        }
    }
}

const {
    withdraw, address_check
} = new AmountController();

/* eslint-disable */
const routers = [{
    url: `/withdraw`,
    method: 'post',
    acc: withdraw
}, {
    url: `/address_check`,
    method: 'post',
    acc: address_check
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

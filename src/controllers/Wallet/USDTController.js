import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
import result from '../../tools/Result';
const router = new KoaRouter();
const usdtService = new services.Wallet.USDTService();

const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

/**
 * LoginController
 * 用户信息类
 */
class USDTController {

    /**
     * 获取用户信息
     * @param {*} ctx
     */
    async getNewAddress(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await usdtService.getNewAddress(validate.data, ctx.request.query);
        } else {
            ctx.body = result.authorities();
        }
    }

    async getERC2Address(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await usdtService.getAddress(validate.data, { symbol: 'ETH_USDT' });
        } else {
            ctx.body = result.authorities();
        }

    }

    /**
     * 获取用户信息
     * @param {*} ctx
     */
    async getTradersMarginAddress(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await usdtService.getTradersMarginAddress(validate.data, ctx.request.query);
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 获取用户信息
     * @param {*} ctx
     */
    async getAddress(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = await usdtService.getAddress(validate.data, ctx.request.query);
        } else {
            ctx.body = result.authorities();
        }
    }
}

const {
    getNewAddress, getERC2Address,
    getAddress, getTradersMarginAddress
} = new USDTController();

/* eslint-disable */
const routers = [{
    url: `/get_new_address`,
    method: 'get',
    acc: getNewAddress
}, {
    url: `/get_address`,
    method: 'get',
    acc: getAddress
}, {
    url: `/get_traders_init_address`,
    method: 'get',
    acc: getTradersMarginAddress
}, {
    url: `/get_address_erc2`,
    method: 'get',
    acc: getERC2Address
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;
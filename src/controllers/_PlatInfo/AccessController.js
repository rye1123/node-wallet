import KoaRouter from 'koa-router';
import services from '../../services';
import result from '../../tools/Result';

const router = new KoaRouter();
const access_requestService = new services._Manage.AccessService();

/**
 * LoginController
 * 用户信息类
 */
class AccessController {


    /**
     * 获取用户的账单流水
     * @param {*} ctx
     */
    async access_record(ctx) {
        ctx.body = await access_requestService.access_record({ id: ctx.request.header.userid },
            ctx.request.query);
    }

    async access_record_merchants(ctx) {
        ctx.body = await access_requestService.access_record_merchants({ id: ctx.request.header.userid },
            ctx.request.query);
    }

    async access_record_traders(ctx) {
        ctx.body = await access_requestService.access_record_traders({ id: ctx.request.header.userid },
            ctx.request.query);
    }

    async regMerchants(ctx) {
        ctx.body = await access_requestService.regMerchant({ id: ctx.request.header.userid },
            ctx.request.body.id)
    }

    async regTraders(ctx) {
        ctx.body = await access_requestService.regMerchant({ id: ctx.request.header.userid },
            ctx.request.body.id)
    }

}

const {
    access_record, access_record_merchants, access_record_traders
} = new AccessController();

/* eslint-disable */
const routers = [{
    url: `/access_record`,
    method: 'get',
    acc: access_record
}, {
    url: `/access_record_merchants`,
    method: 'get',
    acc: access_record_merchants
}, {
    url: `/access_record_traders`,
    method: 'get',
    acc: access_record_traders
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

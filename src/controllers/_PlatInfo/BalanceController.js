import KoaRouter from 'koa-router';
import services from '../../services';
import result from '../../tools/Result';

const router = new KoaRouter();
const balanceService = new services._Manage.BalanceService();

/**
 * LoginController
 * 用户信息类
 */
class BalanceController {


    /**
     * 平台余额信息
     * @param {*} ctx
     */
    async cobo_balanceInfo(ctx) {
        ctx.body = await balanceService.cobo_balanceInfo({ id: ctx.request.header.userid });
    }

}

const {
    cobo_balanceInfo
} = new BalanceController();

/* eslint-disable */
const routers = [{
    url: `/cobo_balance_info`,
    method: 'get',
    acc: cobo_balanceInfo
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
import result from '../../tools/Result';
var sendemail = require('sendemail')
var emailHelper = sendemail.email;
const router = new KoaRouter();
const amountService = new services.Wallet.AmountService();
const verifyService = new services.Verify.SessionService();

const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

/**
 * LoginController
 * 用户信息类
 */
class WalletController {

    /**
     * 获取用户信息
     * @param {*} ctx
     */
    async api_merchants_withdraw(ctx) {
        console.log("api_merchants_withdraw")
        ctx.body = await amountService.api_merchants_withdraw(
            ctx.request.body.data, ctx.request.body.request_ip);

        // var person = {
        //     name: '商户自动提现-申请',
        //     email: '519303436@qq.com', // person.email can also accept an array of emails
        //     subject: `提现申请`,
        //     mail_code: JSON.stringify(ctx.request.body.data)
        // }

        // emailHelper('mail_vali_code', person, function (error, result) {
        //     console.log(' - - - - - - - - - - - - - - - - - - - - -> email sent: ');
        //     console.log(result);
        //     console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
        //     console.log(error)
        // })
    }
}

const {
    api_merchants_withdraw
} = new WalletController();

/* eslint-disable */
const routers = [{
    url: `/withdraw`,
    method: 'post',
    acc: api_merchants_withdraw
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

import KoaRouter from 'koa-router';
import services from '../../services';
import middleware from '../../middleware';
import result from '../../tools/Result';
var sendemail = require('sendemail')
var emailHelper = sendemail.email;
const router = new KoaRouter();
const amountService = new services.Wallet.AmountService();
const verifyService = new services.Verify.SessionService();

const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

/**
 * LoginController
 * 用户信息类
 */
class WithdrawInfoController {

    /**
     * 获取用户信息
     * @param {*} ctx
     */
    async api_withdraw_info(ctx) {
        console.log("api_withdraw_info")
        ctx.body = await amountService.api_withdraw_info(
            ctx.request.body.data);

        // var person = {
        //     name: '商户自动提现-申请',
        //     email: '519303436@qq.com', // person.email can also accept an array of emails
        //     subject: `提现申请`,
        //     mail_code: JSON.stringify(ctx.request.body.data)
        // }

        // emailHelper('mail_vali_code', person, function (error, result) {
        //     console.log(' - - - - - - - - - - - - - - - - - - - - -> email sent: ');
        //     console.log(result);
        //     console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
        //     console.log(error)
        // })
    }
}

const {
    api_withdraw_info
} = new WithdrawInfoController();

/* eslint-disable */
const routers = [{
    url: `/get_single_info`,
    method: 'post',
    acc: api_withdraw_info
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;

import KoaRouter from 'koa-router';
import services from '../../services';
import RedisDao from '../../lib/RedisDao';
import middleware from '../../middleware';
const router = new KoaRouter();
const LoginService = new services.Login.LoginService();
const commonService = new services.Common.CommonService();
const verifyService = new services.Verify.SessionService();

import result from '../../tools/Result';

const {
    ValidateTools
} = middleware.ValidateTools;
const validateTools = new ValidateTools();

/**
 * LoginController
 * 用户信息类
 */
class LoginController {

    /**
     * 管理员登录 后台账号
     * @param {*} ctx
     */
    async aminLogin(ctx) {
        ctx.body = result.authorities()
        return
        const imgValidateData = ctx.cookies.get('imgValidateData');
        ctx.body = await LoginService.aminLogin(ctx.request.body, { imgValidateData });
    }

    /**
     * 用户登录 后台账号
     * @param {*} ctx
     */
    async login(ctx) {
        ctx.body = result.authorities()
        return
        ctx.body = await LoginService.login(ctx.request.body);
    }

    async loginV2_step_1(ctx) {
        // console.log(ctx.request.header)
        // console.log(ctx.request.body)
        ctx.body = await LoginService.loginV2_step_1(ctx.request.body);
    }

    async loginV2_step_end(ctx) {

        // console.log(await RedisDao.mget([``, `4usdt-CLIENT:SECRET:222294`, `4usdt-CLIENT:SECRET:222277`, `4usdt-CLIENT:SECRET:222295`]))

        try {
            let session = await RedisDao.get(`session:prelogin:${ctx.request.body.data.token}`)

            let valiCode = {}
            if (session) {
                session = JSON.parse(session)
            }
            else {
                ctx.body = result.failed("会话已失效")
                return
            }
            if (session.codeType == "Google") {
                valiCode.googleCode = ctx.request.body.validata.googleCode
            }
            else {
                valiCode.phoneCode = ctx.request.body.validata.phoneCode
            }

            let isMobile = false

            console.log(ctx.request.headers['user-agent'])

            let verifyResult = await verifyService.Verify(session, valiCode, false)
            if (ctx.request.headers['user-agent'].indexOf("Traders/") == 0) {
                console.log(`IOS 登录 - ${ctx.request.headers['user-agent']}`)
                isMobile = true
            }
            if (ctx.request.headers['user-agent'].indexOf("okhttp/") == 0) {
                console.log(`Android 登录 - ${ctx.request.headers['user-agent']}`)
                isMobile = true
            }
            if (verifyResult.success) {
                ctx.body = await LoginService.loginV2_step_end(ctx.request.body.data, isMobile);
            } else {
                ctx.body = result.failed("验证失败")
            }
        } catch (error) {
            console.log(error)
            ctx.body = result.failed("发生异常", 400230)
        }
    }

    async logout(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            if (validate.data.tradersStatus > 0) {
                console.log(`[交易员]设置下线 - ${JSON.stringify(await commonService.setTradersOffLine(validate.data.id))}`)
            }
            ctx.body = await LoginService.logout(validate.data);
        } else {
            ctx.body = result.authorities();
        }
    }

    async deToken(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        if (validate) {
            ctx.body = validate.data;
        } else {
            ctx.body = result.authorities();
        }
    }

    /**
     * 用户登录 后台账号
     * @param {*} ctx
     */
    async loginMobile(ctx) {
        ctx.body = await LoginService.loginMobile(ctx.request.body);
    }
    /**
     * 用户登录 后台账号
     * @param {*} ctx
     */
    async loginV2_app_store(ctx) {
        ctx.body = await LoginService.loginV2_app_store(ctx.request.body);
    }

    /**
     * 发送短信，暂不用
     * @param {*} ctx
     */
    async sendPhoneCode(ctx) {
        let parm_account = ctx.request.body.account
        if (!parm_account) return result.failed('参数错误')
        // if (validate) {
        RedisDao.set(`userLoginVerify.phoneCode:_${parm_account}`, Math.floor(Math.random() * 1000000), 600)
        ctx.body = result.success(null, { type: "phone_code", expires_in: 600 });
        // } else {
        //     ctx.body = result.authorities();
        // }
    }

    /**
     * 获取用户额度信息
     * @param {*} ctx
     */
    async VerifyPhoneCode(ctx) {
        const validate = validateTools.validateJWT(ctx.request.header.authorization);
        let parm_code = ctx.request.body.code
        if (!parm_code) return result.failed('参数错误')

        if (validate) {
            let phone_code = await RedisDao.get(`session_verify:phoneCode:_${validate.data.id}`)
            if (phone_code == parm_code) {
                ctx.body = result.success(null, { result: true });
            } else {
                ctx.body = result.success(null, { result: false });
            }
        } else {
            ctx.body = result.authorities();
        }
    }
}

const {
    login,
    loginMobile,
    VerifyPhoneCode,
    sendPhoneCode,
    deToken,
    logout,
    loginV2_step_1,
    loginV2_step_end
} = new LoginController();

/* eslint-disable */
const routers = [{
    url: `/access_token`,
    method: 'post',
    acc: login
}, {
    url: `/de_access_token`,
    method: 'get',
    acc: deToken
}, {
    url: `/access_token_mobile`,
    method: 'post',
    acc: loginMobile
}, {
    url: `/send_verify_code`,
    method: 'get',
    acc: sendPhoneCode
}, {
    url: `/verify_code`,
    method: 'post',
    acc: VerifyPhoneCode
}, {
    url: `/exit`,
    method: 'get',
    acc: logout
}, {
    url: `/loginV2S1`,
    method: 'post',
    acc: loginV2_step_1
}, {
    url: `/loginV2SE`,
    method: 'post',
    acc: loginV2_step_end
}];

/* eslint-enable */

//挂载路由
routers.forEach(item => {
    router[item.method](item.url, item.acc);
});

module.exports = router;
